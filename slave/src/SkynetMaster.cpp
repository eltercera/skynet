/*
 * SkynetMaster.cpp
 */

#include <SkynetMaster.h>
#include <Config.h>
#include <cstring>
#include <ctime>
#include <UniqueID.h>
#include <SkynetPacketProcesing.h>


SkynetMaster::SkynetMaster() {
	this->addr_in = new struct sockaddr_in;
	this->addr_in_sync = new struct sockaddr_in;
	this->status = 0;
	this->lastTimeStamp = -1;
	this->addr_inSize = 0;
	this->addr_inSize_sync = 0;
}


void SkynetMaster::setSockaddr(char *ipaddess, char *port) throw(InvalidServicePortException,InvalidServiceIPException) {

	Config::setSockaddrIn(ipaddess,port,this->addr_in);
	this->addr_inSize = sizeof(&this->addr_in);

}

void SkynetMaster::setSockaddr(struct sockaddr_in *addr_in){
	this->addr_in = addr_in;
}


socklen_t SkynetMaster::getSockaddrSize() {
	return this->addr_inSize;
}

struct sockaddr_in *SkynetMaster::getSockaddr() {

	return this->addr_in;

}

void SkynetMaster::setSockaddrSync(char *ipaddess, char *port) throw(InvalidServicePortException,InvalidServiceIPException) {

	Config::setSockaddrIn(ipaddess,port,this->addr_in_sync);
	this->addr_inSize_sync = sizeof(&this->addr_in_sync);

}

void SkynetMaster::setSockaddrSync(struct sockaddr_in *addr_in){
	this->addr_in_sync = addr_in;
	this->addr_inSize_sync = sizeof(&this->addr_in_sync);
}


struct sockaddr_in *SkynetMaster::getSockaddrSync() {
	return this->addr_in_sync;
}

socklen_t SkynetMaster::getSockaddrSizeSync() {
	return this->addr_inSize_sync;
}

void SkynetMaster::setUid(unsigned char *uid) {

	memcpy(this->uid,uid,16);

}

unsigned char *SkynetMaster::getUid(){

	return this->uid;

}

char *SkynetMaster::getUidString() {

	return UniqueID::getString(this->uid);

}

void SkynetMaster::setStatus(unsigned char status){

	this->status = status;

}

unsigned char SkynetMaster::getStatus() {

	return this->status;

}

void SkynetMaster::touch() {

	this->lastTimeStamp = std::time(NULL);

}

void SkynetMaster::touch(time_t lastTimeStamp) {

	this->lastTimeStamp = lastTimeStamp;

}

int SkynetMaster::time() {

	return std::time(NULL) - this->lastTimeStamp;

}

void SkynetMaster::setUid(char *uid){
	char hex [2];
	for (int i = 0; i < 16; i++){
		hex[0]=uid[i*2];
		hex[1]=uid[(i*2)+1];
		this->uid[i]=strtol(hex,NULL,16);
	}
}

SkynetMaster::~SkynetMaster() {

}

