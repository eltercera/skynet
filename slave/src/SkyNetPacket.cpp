/*
 * SkynetPacket.cpp
 */

#include <cstdio>
#include <cstring>
#include <ctime>
#include <SkyNetPacket.h>
#include <UniqueID.h>

SkyNetPacket::SkyNetPacket() {

	std::memset(this->packet,0,32);

}

SkyNetPacket::SkyNetPacket(char *datagram){

	if (sizeof(datagram) > 32)
		throw InvalidPacketSizeException();

	std::memcpy(this->packet,datagram,32);

}

void SkyNetPacket::setAcctionID(char acction_id) {

	this->packet[0] = acction_id;

}

void SkyNetPacket::setStatusID(char status_id) {

	this->packet[1] = status_id;

}

void SkyNetPacket::setServicePort(int port) {

	if (port > UDP_TOP_PORT || port < UDP_MIN_PORT)
		throw InvalidServicePortException();

	this->packet[2] = (port & 0xFF00) >> 8;
	this->packet[3] = (0xFF & port);

}

void SkyNetPacket::setServiceAddress(uint32_t service_address) {
	int i;
	int j;

	j = 4;
	for (i=24; i >= 0; i = i - 8) {
		this->packet[j] = (service_address & (0xFF << i)) >> i;
		j++;
	}

}

void SkyNetPacket::setTimeStamp(uint64_t timestamp) {
	int i;
	int j;

	j = 8;
	for (i=56; i >= 0; i = i - 8) {
		this->packet[j] = (timestamp & (0xFF << i)) >> i;
		j++;
	}

}

void SkyNetPacket::setTimeStamp() {

	this->setTimeStamp((uint64_t) std::time(NULL));

}

void SkyNetPacket::setUID(unsigned char digest[16]) {

	std::memcpy(&this->packet[16],digest,16);

}

char SkyNetPacket::getAcctionID() {

	return this->packet[0];

}

char SkyNetPacket::getStatusID() {

	return this->packet[1];

}

int SkyNetPacket::getServicePort() {

	return (this->packet[2] << 8) | this->packet[3];

}

unsigned char *SkyNetPacket::getUID() {

	return &this->packet[16];

}

uint32_t SkyNetPacket::getServiceAddr(){
	int i;
	int j;
	uint32_t add = 0;
	j = 4;
	for (i=24; i >= 0; i = i - 8) {
		add = add | (this->packet[j] << i);
		j++;
	}
	return add;
}

char *SkyNetPacket::getUIDString() {

	return UniqueID::getString(&this->packet[16]);

}

struct sockaddr_in *SkyNetPacket::getSockAddr(){

	struct sockaddr_in *address = new struct sockaddr_in;
	struct in_addr in_ad;

	address->sin_family = AF_INET;
	address->sin_port = htons(this->getServicePort());
	in_ad.s_addr = this->getServiceAddr();
	address->sin_addr = in_ad;

	return address;
}

uint64_t SkyNetPacket::getTimeStamp() {
	int i;
	int j;
	uint64_t add = 0;
	j = 8;
	for (i=56; i >= 0; i = i - 8) {
		add = add | (this->packet[j] << i);
		j++;
	}
	return add;
}

unsigned char *SkyNetPacket::getPacket() {

	return this->packet;

}

SkyNetPacket::~SkyNetPacket() {

	std::memset(this->packet,0,32);

}

