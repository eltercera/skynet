/*
 * UniqueID.cpp
 */


#include <UniqueID.h>
#include <cstdio>
#include <ctime>
#include <unistd.h>
#include <Config.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <openssl/md5.h>
#include <sstream>
#include <Masters.h>
#include <string.h>



unsigned char *UniqueID::uniqueID = new unsigned char[16];
unsigned char UniqueID::status;

char *UniqueID::getMac(int Nat){

	char buf[1024];
	char *ret;
	int sock;	//Sock para consulta
	int nInter;	//Numero de interfaces

	struct ifconf ifc;	//Consulta
	struct ifreq *ifr;	//Lista de interfaces
	struct ifreq *iface; //interfaz
	struct sockaddr_in *address; //direccion de interfaz
	struct sockaddr_in *addressCompare; //dirección a comparar

	iface = NULL;

	sock = socket(AF_INET, SOCK_DGRAM, 0);
	if (sock < 0) {
		SkynetLog::log(SkyNet_LOG_ERR,"Error al buscar la mac address: %s",strerror(errno));
		return NULL;
	}

	ifc.ifc_len = sizeof(buf);
	ifc.ifc_buf = buf;
	if(ioctl(sock, SIOCGIFCONF, &ifc) < 0) {
		SkynetLog::log(SkyNet_LOG_ERR,"Error al buscar interfaces activas: %s",strerror(errno));
		return NULL;
	}

	ifr = ifc.ifc_req;
	nInter = ifc.ifc_len / sizeof(struct ifreq);

	if (!Config::getNat())
		addressCompare = &Config::service;
	else
		addressCompare = &Config::masterUnicast;

	for(int i = 0; i < nInter; i++) {

		iface = &ifr[i];
		address=(struct sockaddr_in *) &iface->ifr_addr;

		if (address->sin_addr.s_addr == addressCompare->sin_addr.s_addr)
			break;
		iface = NULL;
	}

	if (iface == NULL) {
		iface = &ifr[1];
	}
	ret = new char [18];

	if(ioctl(sock, SIOCGIFHWADDR, iface) < 0) {
		SkynetLog::log(SkyNet_LOG_ERR,"Error al buscar la Mac de la iterfaz: %s",strerror(errno));
		throw new CanNotGetMACException();
	}

	std::sprintf(ret,"%02x:%02x:%02x:%02x:%02x:%02x",
			(unsigned char) iface->ifr_hwaddr.sa_data[0],
			(unsigned char) iface->ifr_hwaddr.sa_data[1],
			(unsigned char) iface->ifr_hwaddr.sa_data[2],
			(unsigned char) iface->ifr_hwaddr.sa_data[3],
			(unsigned char) iface->ifr_hwaddr.sa_data[4],
			(unsigned char) iface->ifr_hwaddr.sa_data[5]);

	return ret;
}

void UniqueID::generate(){

	ostringstream data;

	char *mac = UniqueID::getMac(Config::Nat);

	data << mac << "/" << time(NULL) << "/" << getpid();

	MD5_CTX ctx;
	MD5_Init(&ctx);
	MD5_Update(&ctx, ((std::string)data.str()).c_str(), data.str().length());
	MD5_Final(UniqueID::uniqueID, &ctx);
	SkynetLog::log(SkyNet_LOG_INFO,"UID Genreado para la instancia: %s",UniqueID::getString());
}

unsigned char *UniqueID::get(){

	return UniqueID::uniqueID;

}

char *UniqueID::getString() {

	return UniqueID::getString(UniqueID::uniqueID);

}

char *UniqueID::getString(unsigned char *uid) {
	int i;
	char *mdString;
	mdString = new char[33];

	for(i = 0; i < 16; i++)
		std::sprintf(&mdString[i*2], "%02x", uid[i]);

	return mdString;
}

unsigned char UniqueID::slaveStatus(){
	return UniqueID::status;
}
void UniqueID::setSlaveStatus(unsigned char sta){

	SkyNetPacket *pk = new SkyNetPacket();

	UniqueID::status = sta;

	pk->setAcctionID(AID_SLAVE_STATUS);
	pk->setServiceAddress(Config::masterUnicast.sin_addr.s_addr);
	pk->setServicePort(Config::masterUnicast.sin_port);
	pk->setStatusID(UniqueID::status);
	pk->setUID(UniqueID::get());
	Masters::sendToAll(pk);

}

