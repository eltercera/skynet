/*
 * main.cpp
 */


#include <csignal>
#include <SkyNetSlave.h>
#include <SkyNetException.h>
#include <SkyNetLocalIndex.h>
#include <Config.h>
#include <string.h>
#include <SkynetLog.h>
#include <SkyNetLocalFS.h>
#include <Masters.h>
#include <UniqueID.h>
#include <unistd.h>
#include <SkynetUdpSock.h>
#include <SkyNetSync.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <SkyNetSyncMsg.h>
#include <SkynetPacketProcesing.h>
#include <SkynetTCPSock.h>

int work = 1;

void endall(int signum){

	if(Masters::login(1)){
		SkynetLog::log(SkyNet_LOG_DEBUG,"Unregister Satisfactorio.");
	}

	SkynetLog::log(SkyNet_LOG_DEBUG,"Matando a los servidores.");
	SkynetUdpSock::stopSkynetUdpSock();
	SkynetLog::log(SkyNet_LOG_DEBUG,"Udp Sock Murio.");
	SkyNetSync::stopSkynetSyncSock();
	SkynetLog::log(SkyNet_LOG_DEBUG,"Sync Sock murio.");

	SkynetUdpSock::stopSkynetKeepAlive();
	SkynetLog::log(SkyNet_LOG_DEBUG,"KeepAlive murio.");
	SkynetPacketProcesing::stopProcesing();
	SkynetLog::log(SkyNet_LOG_DEBUG,"Procesador de paquetes murio.");


	Masters::end();
	SkyNetLocalIndex::end();
	SkynetLog::close();
	unlink(".skynetpid");

	work = 0;
	exit(EXIT_SUCCESS);
}


int checkParameter3(char *a){
	return (strcmp(a,HOST_CONFIG_TYPE_SERVICE) == 0
			|| strcmp(a,HOST_CONFIG_TYPE_MASTER) == 0
			|| strcmp(a,HOST_CONFIG_TYPE_MASTER_S) == 0
			|| strcmp(a,HOST_CONFIG_TYPE_SYNC) == 0);
}




void server(){
	signal(SIGTERM,endall);

	SkynetLog::init(SkyNet_LOG_DEBUG);

	Masters::init();

	try{
		SkynetLog::log(SkyNet_LOG_DEBUG,"Inicializando Indice.");
		SkyNetLocalIndex::init();
	} catch (exception *e) {
		SkynetLog::log(SkyNet_LOG_CRIT,"Se produjo un error al iniciar la base de datos.\nVea el log para mas informacion.");
		exit(EXIT_FAILURE);
	}

	try{
		SkynetLog::log(SkyNet_LOG_DEBUG,"Inicializando Configuración");
		Config::init();
	} catch (exception *e) {
		SkynetLog::log(SkyNet_LOG_CRIT,"Se produjo un error al iniciar La Configuració.\nVea el log para mas informacion.");
		exit(EXIT_FAILURE);
	}

	try{
		SkynetLog::log(SkyNet_LOG_DEBUG,"Generando UID");
		UniqueID::generate();
	} catch (CanNotGetMACException *e) {
		SkynetLog::log(SkyNet_LOG_CRIT,"Se produjo un error al Generar el UID de la instancia.\nVea el log para mas informacion.");
		exit(EXIT_FAILURE);
	}

	try{
		SkynetLog::log(SkyNet_LOG_DEBUG,"Checqueando Archivos.");
		SkyNetLocalIndex::init_files_status();
		SkyNetLocalFS::checkLocalDir();
	} catch (exception *e) {
		SkynetLog::log(SkyNet_LOG_CRIT,"Se produjo un error al iniciar La Configuració.\nVea el log para mas informacion.");
		exit(EXIT_FAILURE);
	}

	if (Masters::login(0)) {

		SkynetLog::log(SkyNet_LOG_DEBUG,"Iniciando procesamiento de paquetes.");
		try{
			SkynetPacketProcesing::startProcesing();
		} catch (ProcesingAlreadyStartedException *e) {
			SkynetLog::log(SkyNet_LOG_CRIT,"Ya existe un procesador de paquetes iniciado.");
		}
		try {
			SkynetLog::log(SkyNet_LOG_DEBUG,"Iniciando UDP Sock.");
			SkynetUdpSock::init();
			SkynetUdpSock::startSkynetUdpSock();
		} catch (SockUdpException *e) {
			SkynetLog::log(SkyNet_LOG_CRIT,"Se produjo un error al inicializar el sock UDP.");
			endall(0);
		} catch (ProcesingAlreadyStartedException *e) {
			SkynetLog::log(SkyNet_LOG_CRIT,"Ya esiste un Servidor iniciado.");
		}
		SkynetLog::log(SkyNet_LOG_DEBUG,"Iniciando Keep Alive.");
		try{
			SkynetUdpSock::startSkynetKeepAlive();
		} catch (ProcesingAlreadyStartedException *e) {
			SkynetLog::log(SkyNet_LOG_CRIT,"Ya existe un Keep Alive iniciado.");
		}

		SkynetLog::log(SkyNet_LOG_DEBUG,"Iniciando Sync Sock.");
		try {
			SkyNetSync::startSkynetSyncSock();
		} catch (SockUdpException *e) {
			SkynetLog::log(SkyNet_LOG_CRIT,"Se produjo un error al inicializar el sock Sync.");
			endall(0);
		} catch (ProcesingAlreadyStartedException *e) {
			SkynetLog::log(SkyNet_LOG_CRIT,"Ya esiste un Servidor iniciado.");
		}

		SkynetLog::log(SkyNet_LOG_DEBUG,"Iniciando Client Sock.");
		try {
			SkynetTCPSock::startSkynetTCPSock();
		} catch (SockUdpException *e) {
			SkynetLog::log(SkyNet_LOG_CRIT,"Se produjo un error al inicializar el sock Sync.");
			endall(0);
		} catch (ProcesingAlreadyStartedException *e) {
			SkynetLog::log(SkyNet_LOG_CRIT,"Ya esiste un Servidor iniciado.");
		}


	} else {
		SkynetLog::log(SkyNet_LOG_CRIT,"No se logro realizar el registro.");
		endall(0);
	}

	SkynetLog::log(SkyNet_LOG_DEBUG,"Ya se ha iniciado todo.");
	work = 1;
	UniqueID::setSlaveStatus(SID_SLAVE_ENABLE);
	while (work){
		sleep(1);
		SkyNetSync::clean();
	}
	endall(0);
}

int main (int argc, char** argv) {
	int ret = EXIT_SUCCESS;

	if (argc < 2){
		cerr << "Es necesario el parametro de accion:\n"
				"   Skynet_slave server\n"
				"   Skynet_slave config\n";
		return EXIT_FAILURE;
	}


	SkynetLog::init(SkyNet_LOG_DEBUG);

	if (strcmp(argv[1],"config") == 0){
		if (argc > 2){
			if(strcmp(argv[2],"set") == 0 && argc == 6){
				if (checkParameter3(argv[3])){
					try{
						SkyNetLocalIndex::init();
						Config::setHostInfo(argv[4],argv[5],argv[3]);
						SkyNetLocalIndex::end();
					} catch (exception *e) {
						cerr << "Se a producido un error, Verifique el log del sistema.\n";
						ret = EXIT_FAILURE;
					}
				} else {
					cerr << "Parametro " << argv[3] << " desconocido.\n";
					ret = EXIT_FAILURE;
				}
			} else if(strcmp(argv[2],"del") == 0 && argc == 4) {
				if (checkParameter3(argv[3])){
					try{
						SkyNetLocalIndex::init();
						Config::delHostInfo(argv[3]);
						SkyNetLocalIndex::end();
					} catch (exception *e) {
						cerr << "Se a producido un error, Verifique el log del sistema.\n";
						ret = EXIT_FAILURE;
					}
				} else {
					cerr << "Parametro " << argv[3] << " desconocido.\n";
					ret = EXIT_FAILURE;
				}
			} else if(strcmp(argv[2],"print") == 0) {

				sqlResult res;
				sqlRow *row;

				SkyNetLocalIndex::init();
				res = Config::getConfig();
				printf("Tipo\tHost:Puerto\n");
				while (!res.empty()){
					row = res.back();
					printf("%s\t%s:%s\n",row->at(0),row->at(1),row->at(2));
					res.pop_back();
				}

				SkyNetLocalIndex::end();
			} else {
				cerr << "Formato de parametros invalido.\n"
						"   Skynet_slave config set <service|master|sync|masterserver> <host> <port>\n"
						"   Skynet_slave config del <service|master|sync|masterserver>\n"
						"   Skynet_slave config print\n";
				ret = EXIT_FAILURE;
			}
		} else {
			cerr << "Faltan  Parametros.\n"
					"   Skynet_slave config set <service|master|sync|masterserver> <host> <port>\n"
					"   Skynet_slave config del <service|master|sync|masterserver>\n"
					"   Skynet_slave config print\n";
			ret = EXIT_FAILURE;
		}
	} else if (strcmp(argv[1],"server") == 0){

		if (strcmp(argv[2],"start") == 0 && argc == 3){
			pid_t pid;

			if (access (".skynetpid", F_OK) == 0){
				cerr << "Existe un archivo .skynetpid puede que otro demonio este corriendo.\n";
				ret = EXIT_FAILURE;
			} else {
				pid = fork();
				if (pid < 0) {
					cerr << "No se logro iniciar el demonio.\n";
					ret = EXIT_FAILURE;
				}

				if (pid > 0) {
					exit(EXIT_SUCCESS);
				}

				umask(0);
				pid_t sid;
				sid = setsid();
				if (sid < 0) {
					cerr << "Patito Feo\n";
					exit(EXIT_FAILURE);
				}
				cerr << "Iniciado demonio en PID: " << sid << "\n";
				char bash [100];
				memset(bash,0,100);
				sprintf(bash,"echo '%d' > %s",sid ,".skynetpid");
				system(bash);

				//close(STDIN_FILENO);
				//close(STDOUT_FILENO);
				//close(STDERR_FILENO);

				server();
			}
		} else if (strcmp(argv[2],"stop") == 0 && argc == 3) {
			if (access (".skynetpid", F_OK) == -1){
				cerr << "No existe un archivo .skynetpid, lo mas seguro es que no se encuentre corriendo un servicor.\n";
				ret = EXIT_FAILURE;
			} else {
				char bash [100];
				memset(bash,0,100);
				sprintf(bash,"kill $(cat .skynetpid)");
				if(!system(bash)){
					cout << "Enviada señal de Finalizacion del servidor.\n";
				} else {
					cout << "No se logro enviar sefal de Finalizacion del servidor.\n";
				}
			}
		} else {
			cerr << "Param.\n"
					"   Skynet_slave <start|stop>\n";
		}



	} else {
		cerr << "Parametro " << argv[1] << " desconocido.\n";
		ret = EXIT_FAILURE;
	}
	SkynetLog::close();
	return ret;
}

