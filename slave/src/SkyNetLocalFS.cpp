/*
 * SkyNetLocalFS.cpp
 *
 *  Created on: Dec 26, 2015
 *      Author: rrodriguez
 */

#include <SkyNetLocalFS.h>

#include <dirent.h>
#include <string.h>
#include <list>
#include <SkyNetLocalIndex.h>

const char *df_path =  ".";

void SkyNetLocalFS::checkLocalDir(){
	struct dirent *dirEntry;
	list <const char *> paths;
	char *path;
	const char *current_p;
	DIR * dp;
	SkyNetFile *file;


	paths.push_front(df_path);


	while (!paths.empty()){
		current_p = paths.front();
		paths.pop_front();

		dp = opendir(current_p);
		if (dp == NULL){
			SkynetLog::log(SkyNet_LOG_ERR,"No se pudo abrir el directorio '%s': %s",current_p,strerror(errno));
			continue;
		}
		dirEntry=readdir(dp);
		while (dirEntry != NULL){
			if (dirEntry->d_name[0] == '.'){
				dirEntry=readdir(dp);
				continue;
			}

			path = new char[strlen(dirEntry->d_name)+strlen(current_p)+10];
			sprintf(path,"%s/%s",current_p,dirEntry->d_name);

			switch(dirEntry->d_type){
				case DT_DIR:
					paths.push_back(path);
					break;

				case DT_REG:
					try{
						file = new SkyNetFile(path);
					} catch (PathIsNullException *e) {
						SkynetLog::log(SkyNet_LOG_DEBUG,"Archivo %s se encontraba en indice pero no existe en FS.",file->getName());
						SkyNetLocalIndex::deleteFile(file);
						delete(file);
						continue;
					}
					SkynetLog::log(SkyNet_LOG_DEBUG,"Chekeando archivo '%s'",path);
					delete SkyNetLocalIndex::checkFile(file);
					break;

				default:
					SkynetLog::log(SkyNet_LOG_DEBUG,"Omitiendo archivo: '%s'",path);
					break;
			}
			dirEntry=readdir(dp);
		}
		closedir(dp);
	}

}
