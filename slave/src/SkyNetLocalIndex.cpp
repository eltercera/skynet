/*
 * SkyNetLocalIndex.cpp
 *
 *  Created on: Dec 23, 2015
 *      Author: rrodriguez
 */

#include "SkyNetLocalIndex.h"
#include "SkyNetException.h"
#include "string.h"
#include <SkynetSyncLocalAgent.h>




sqlite3 *SkyNetLocalIndex::db = NULL;

const char *sql_table_struct_file = "\
CREATE TABLE `FILE` (\
	`name_hash`	TEXT NOT NULL,\
	`file_name`	TEXT NOT NULL,\
	`file_chksum`	TEXT NOT NULL,\
	`file_timestamp`	TEXT NOT NULL,\
	`file_size`	TEXT NOT NULL,\
	`file_status`	INTEGER NOT NULL,\
	PRIMARY KEY(name_hash)\
)";

const char *sql_table_struct_config = "\
CREATE TABLE `HOSTS_CONFIG` (\
	`type`	TEXT NOT NULL,\
	`host`	TEXT NOT NULL,\
	`port`	TEXT\
)";

const char *sql_chk_tables_cfg = "select name FROM sqlite_master WHERE type='table' AND name='HOSTS_CONFIG'";

const char *sql_chk_tables = "select name FROM sqlite_master WHERE type='table' AND name='FILE'";

const char *sql_file_exist = "select name_hash from `FILE` where name_hash = '%s'";

const char *sql_get_file_info = ""
		"select name_hash,file_name,file_chksum,file_timestamp,file_size,file_status "
		"from `FILE` "
		"where name_hash = '%s'";
const char *sql_get_files_info = ""
		"select name_hash,file_name,file_chksum,file_timestamp,file_size,file_status "
		"from `FILE`";

const char *sql_insert_file = ""
			"insert into `FILE` (name_hash,file_name,file_chksum,file_timestamp,file_size,file_status) "
			"values ('%s','%s','%s','%ld','%ld',%i)";

const char *sql_update_file = ""
		"update `FILE` set file_chksum = '%s', file_timestamp = '%ld', file_size = '%ld', file_status = %i "
		"where name_hash = '%s'";

const char *sql_update_file_name = ""
		"update `FILE` set name_hash = '%s', fiel_name = '%s'"
		"where name_hash = '%s'";

const char *sql_delete_file = "delete from `FILE` where name_hash = '%s'";

const char *sql_init_status = "update FILE set file_status = 0";

void SkyNetLocalIndex::init(){
	if (SQLITE_OK != sqlite3_open_v2("./.local.db",&SkyNetLocalIndex::db,SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE,NULL)){
		SkynetLog::log(SkyNet_LOG_CRIT,"Error al abrir .local.db: %s",sqlite3_errmsg(SkyNetLocalIndex::db));
		throw new CanNotOpenIndexDBException();
	}
	SkynetLog::log(SkyNet_LOG_DEBUG,".local.db Abierto.");
	if (sqlite3_db_readonly(SkyNetLocalIndex::db,NULL)){
		SkynetLog::log(SkyNet_LOG_CRIT,"Error al abrir .local.db: No se logra abrir con permisos de escritura");
		SkyNetLocalIndex::end();
		throw new CanNotOpenIndexDBException();
	}
	SkynetLog::log(SkyNet_LOG_DEBUG,"Escritura verificada en .local.db");

	SkyNetLocalIndex::checkStructure();
}



void SkyNetLocalIndex::end(){
	if (SkyNetLocalIndex::db){

		if (SQLITE_OK == sqlite3_close(SkyNetLocalIndex::db)){
			SkynetLog::log(SkyNet_LOG_DEBUG,"Cerrada la conexión al .local.db");
		} else {
			SkynetLog::log(SkyNet_LOG_ERR,"Error al cerrar la conexión al .local.db: %s",sqlite3_errmsg(SkyNetLocalIndex::db));
		}
	}
}

void SkyNetLocalIndex::init_files_status(){

	if (!SkyNetLocalIndex::exceQuery((char *)sql_init_status,NULL)){
		SkynetLog::log(SkyNet_LOG_DEBUG,"Inicialización de estados de archivos completa.");
	} else {
		SkynetLog::log(SkyNet_LOG_WARNING,"Recividos lineas en consulta de inicializacion de estatus.");
	}
}


int SkyNetLocalIndex::fileExist(SkyNetFile *file){
	char sql [100];

	sprintf(sql,sql_file_exist,file->getHashName());

	return SkyNetLocalIndex::exceQuery(sql,NULL);
}

void SkyNetLocalIndex::saveFile(SkyNetFile *file){
	char sql [500];

	if (file == NULL)
		return;

	SkynetLog::log(SkyNet_LOG_DEBUG,"Guardando Archivo %s en indice.",file->getName());
	if (!SkyNetLocalIndex::fileExist(file)){
		SkynetLog::log(SkyNet_LOG_DEBUG,"Archivo %s no existe en indice.",file->getName());
		sprintf(sql,sql_insert_file ,file->getHashName(),file->getName(),file->getCheckSum(),file->getTimeStamp(),file->getSize(),file->getStatus());
	} else {
		SkynetLog::log(SkyNet_LOG_DEBUG,"Archivo %s existe en indice.",file->getName());
		sprintf(sql,sql_update_file ,file->getCheckSum(),file->getTimeStamp(),file->getSize(),file->getStatus(),file->getHashName());
	}

	try{
		SkyNetLocalIndex::exceQuery(sql,NULL);
	}  catch (CanNotPrepareSqlDBException *e) {
		SkynetLog::log(SkyNet_LOG_DEBUG,"No se pudo guardar archivo en %s del indice.",file->getName());
	} catch (CanNotExecQueryDBException *e) {
		SkynetLog::log(SkyNet_LOG_DEBUG,"No se pudo guardar archivo en %s del indice.",file->getName());
	}

	SkynetLog::log(SkyNet_LOG_DEBUG,"Archivo %s Guardado.",file->getName());
}


int SkyNetLocalIndex::exceQuery(char *sql,sqlResult *result){

	sqlite3_stmt *stmt;
	int res;
	int col;
	int rows;


	rows=0;
	if (!sql){
		SkynetLog::log(SkyNet_LOG_ALERT,"No se paso el sqlll");
		return rows;
	}

	res = sqlite3_prepare(SkyNetLocalIndex::db,sql,-1,&stmt,NULL);
	if (res != SQLITE_OK) {
		SkynetLog::log(SkyNet_LOG_ALERT,"Error al preparar sql: %s.",sqlite3_errmsg(SkyNetLocalIndex::db));
		throw new CanNotPrepareSqlDBException();
	}

	do {
		res = sqlite3_step(stmt);

		switch(res){
			case SQLITE_BUSY:
				SkynetLog::log(SkyNet_LOG_ALERT,"Error al ejecutar sql: Busy");
				throw new CanNotExecQueryDBException();
				break;

			case SQLITE_ERROR:
				SkynetLog::log(SkyNet_LOG_ALERT,"Error al ejecutar consulta: sql(%s) %s.",sql,sqlite3_errmsg(SkyNetLocalIndex::db));
				throw new CanNotExecQueryDBException();
				break;

			case SQLITE_ROW:
				sqlRow *line = new sqlRow;

				if (result == NULL)
					result = new sqlResult;

				col=sqlite3_data_count(stmt);
				for(int i = 0; i<col; i++){
					char *data;
					const char * dd;
					dd = (const char *)sqlite3_column_text(stmt,i);
					data = new char [strlen(dd)+1];
					memset(data,0,strlen(dd)+1);
					strcpy(data,dd);
					line->push_back(data);

				}
				if (result != NULL)
					result->push_back(line);
				rows++;
				break;
		}
	} while (res == SQLITE_ROW);

	if (sqlite3_finalize(stmt) != SQLITE_OK){
		SkynetLog::log(SkyNet_LOG_ALERT,"Error al finalizar un stmt: \"%s\".",sqlite3_errmsg(SkyNetLocalIndex::db));
	}

	return rows;
}

void SkyNetLocalIndex::checkStructure(){

	sqlResult res;

	if (!SkyNetLocalIndex::exceQuery((char *)sql_chk_tables,&res)){
		SkynetLog::log(SkyNet_LOG_DEBUG,"No se encontro tabla FILE.");
		SkyNetLocalIndex::exceQuery((char *)sql_table_struct_file,NULL);
		SkynetLog::log(SkyNet_LOG_DEBUG,"Creada tabla FILE");
	}

	if (!SkyNetLocalIndex::exceQuery((char *)sql_chk_tables_cfg,&res)){
		SkynetLog::log(SkyNet_LOG_DEBUG,"No se encontro tabla HOSTS_CONFIG.");
		SkyNetLocalIndex::exceQuery((char *)sql_table_struct_config,NULL);
		SkynetLog::log(SkyNet_LOG_DEBUG,"Creada tabla HOSTS_CONFIG");
	}

}

void SkyNetLocalIndex::deleteFile(SkyNetFile *file){
	char sql [100];

	sprintf(sql,sql_delete_file,file->getHashName());

	try{
		SkyNetLocalIndex::exceQuery(sql,NULL);
	} catch (CanNotPrepareSqlDBException *e) {
		SkynetLog::log(SkyNet_LOG_DEBUG,"No se pudo eliminar el archivo %s del indice.",file->getName());
	}
	SkynetLog::log(SkyNet_LOG_DEBUG,"Archivo %s eliminado del indice.",file->getName());
}

void SkyNetLocalIndex::renameFile(SkyNetFile *file,char *newName){
	char sql [100];
	char old[33];
	char oldname[1000];//ojo con esto.;

	memset(oldname,0,1000);
	strcpy(oldname,file->getName());

	memset(old,0,33);
	strcpy(old,file->getHashName());
	file->setName(newName);

	sprintf(sql,sql_update_file_name,file->getHashName(),file->getName(),old);
	try{
		SkyNetLocalIndex::exceQuery(sql,NULL);
	} catch (CanNotPrepareSqlDBException *e) {
		SkynetLog::log(SkyNet_LOG_DEBUG,"No se pudo cambiar el nombre archivo %s del indice.",file->getName());
	}

	SkynetLog::log(SkyNet_LOG_DEBUG,"Cambio de nombre de archivo %s -> %s.",oldname,file->getName());
}

SkyNetFile *SkyNetLocalIndex::checkFile(SkyNetFile *file){
	char sql[500];
	sqlResult res;
	SkyNetFile *dbFile;
	int diff;

	sprintf(sql,sql_get_file_info,file->getHashName());
	try{
	if (SkyNetLocalIndex::exceQuery(sql,&res)) {
		dbFile = new SkyNetFile(res.front());
		diff = file->equals(dbFile);
		if (diff & 1){
			SkynetLog::log(SkyNet_LOG_DEBUG,"Detectado Cambio de checksum en archivo %s: db->%s fs->%s",file->getName(),dbFile->getCheckSum(),file->getCheckSum());
			if (diff & 8){
				if (file->getTimeStamp() > dbFile->getTimeStamp()){
					SkynetLog::log(SkyNet_LOG_DEBUG,"Detectado Cambio resiente en archivo %s.",file->getName());
					file->setStatus(FILE_STATUS_CHECK);
					SkyNetLocalIndex::saveFile(file);
				} else {
					SkynetLog::log(SkyNet_LOG_DEBUG,"Detectado Cambio no resiente en archivo %s.",file->getName());
					file->setStatus(FILE_STATUS_CHECK);
					SkyNetLocalIndex::saveFile(file);
				}

			} else {
				SkynetLog::log(SkyNet_LOG_DEBUG,"Detectado Cambio de checksum en archivo %s con el mismo timestamp: db->%dl fs->%dl",file->getName(),dbFile->getTimeStamp(),file->getTimeStamp());
				file->setStatus(FILE_STATUS_CHECK);
				SkyNetLocalIndex::saveFile(file);
			}

		} else {
			SkynetLog::log(SkyNet_LOG_DEBUG,"Archivo %s chequeado.",file->getName());
			file->setStatus(FILE_STATUS_CHECK);
			SkyNetLocalIndex::saveFile(file);
		}
		delete dbFile;
	} else {
		SkynetLog::log(SkyNet_LOG_DEBUG,"Archivo nuevo detectado: %s.",file->getName());
		file->setStatus(FILE_STATUS_CHECK);
		SkyNetLocalIndex::saveFile(file);
	}
	} catch (CanNotPrepareSqlDBException *e) {
		SkynetLog::log(SkyNet_LOG_DEBUG,"No se pudo Obtener el archivo %s.",file->getName());
		return NULL;
	}
	return file;
}

SkyNetFile *SkyNetLocalIndex::createFile(char *hName){

	SkyNetFile *dbFile;
	char sql[500];
	sqlResult res;

	SkyNetFile *local;

	local = (SkyNetFile *)SkyNetSync::getAgentWhitFile(hName);

	if(local!= NULL){
		return local;
	}

	sprintf(sql,sql_get_file_info,hName);
	try{
	if (SkyNetLocalIndex::exceQuery(sql,&res)) {
		dbFile = new SkyNetFile(res.front());
	} else {
		return NULL;
	}
	} catch (CanNotPrepareSqlDBException *e) {
		SkynetLog::log(SkyNet_LOG_DEBUG,"No se pudo Obtener el archivo %s.",hName);
		return NULL;
	}

	return dbFile;
}

SkynetSyncLocalAgent *SkyNetLocalIndex::checkAll() {

	sqlResult res;
	SkyNetFile *dbFile;
	SkynetSyncLocalAgent *ag = new SkynetSyncLocalAgent();
	try{
	if (SkyNetLocalIndex::exceQuery((char *)sql_get_files_info,&res)) {

		while (!res.empty()){
			dbFile = new SkyNetFile(res.back());
			res.pop_back();
			if(!dbFile)
				continue;
			if (dbFile->getStatus() != FILE_STAUTS_REGISTRED
					&& dbFile->getStatus() != FILE_STATUS_LOCAL_LOCK
					&& dbFile->getStatus() != FILE_STATUS_REMOTE_LOCK
					&& dbFile->getStatus() != FILE_STATUS_SYNC_FROM_LOCAL
					&& dbFile->getStatus() != FILE_STATUS_SYNC_FROM_REMOTE){
				ag->addFile(dbFile);
				continue;
			}
			delete(dbFile);
		}
	}
	} catch (CanNotPrepareSqlDBException *e) {
		SkynetLog::log(SkyNet_LOG_DEBUG,"No se pudo Obtener la lista de archivos.");
		delete(ag);
		return NULL;
	}
	return ag;
}

