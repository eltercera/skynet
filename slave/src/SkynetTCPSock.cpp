/*
 * SkynetTCPSock.cpp
 */

#include <SkynetTCPSock.h>
#include <unistd.h>
#include <SkyNetException.h>


std::thread SkynetTCPSock::t;

int SkynetTCPSock::sock;

int SkynetTCPSock::inWork;

std::list<SkynetClient *> SkynetTCPSock::clients;

void SkynetTCPSock::run() {
	int client;
	struct sockaddr_in server;
	struct sockaddr_in client_in;
	unsigned int client_in_len;
	SkynetClient *client_node;


	SkynetTCPSock::sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (!Config::Nat)
		server = Config::service;
	else {
		server = Config::masterUnicast;
		server.sin_port = Config::service.sin_port;
	}

	if (bind(SkynetTCPSock::sock, (struct sockaddr *) &server, sizeof(struct sockaddr)) < 0) {
		SkynetLog::log(SkyNet_LOG_ERR,"Error al bind del socket TCP: %s",strerror(errno));
		throw new SockUdpException();
	}

	if (listen(SkynetTCPSock::sock,100) < 0) {
		SkynetLog::log(SkyNet_LOG_ERR,"Error al bind del socket TCP: %s",strerror(errno));
		return;
	}

	while(SkynetTCPSock::inWork) {
		client = accept(SkynetTCPSock::sock, (struct sockaddr *) &client_in, &client_in_len);

		if (client < 0) {
			if (errno != 22){
				SkynetLog::log(SkyNet_LOG_ERR,"Error al aceptar la conexion: %s",strerror(errno));
			}
			break;
		}
		SkynetLog::log(SkyNet_LOG_DEBUG,"Aceptado una conexión.");
		client_node = new SkynetClient(client, client_in);
		SkynetTCPSock::clients.push_front(client_node);
		client_node->start();
	}

	SkynetTCPSock::emptyList();

	close(SkynetTCPSock::sock);
}


void SkynetTCPSock::emptyList(){

	SkynetClient *client_node;

	while (!SkynetTCPSock::clients.empty()) {
		client_node = SkynetTCPSock::clients.front();
		SkynetTCPSock::clients.pop_front();
		//client_node->stop();
		free(client_node);
	}
}


void SkynetTCPSock::startSkynetTCPSock() {
	SkynetTCPSock::inWork = 1;
	SkynetTCPSock::t = std::thread(SkynetTCPSock::run);
}

void SkynetTCPSock::stopSkynetTCPSock() {
	SkynetTCPSock::inWork = 0;
	shutdown(SkynetTCPSock::sock,2);
	SkynetTCPSock::t.join();
}
