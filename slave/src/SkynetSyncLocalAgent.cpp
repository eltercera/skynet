/*
 * SkynetSyncLocalAgent.cpp
 *
 *  Created on: Dec 28, 2015
 *      Author: rrodriguez
 */

#include <SkynetSyncLocalAgent.h>
#include <SkyNetSync.h>
#include <Masters.h>
#include <SkyNetLocalIndex.h>
#include <Config.h>
#include <string.h>
#include <unistd.h>

int SkynetSyncLocalAgent::getType(){
	return 1;
}

int SkynetSyncLocalAgent::work(){
	return !this->files.empty();
}

int SkynetSyncLocalAgent::count(){
	return this->files.size();
}

void SkynetSyncLocalAgent::run(){

	SkyNetFile *file;

	int sock;
	int intentos;

	intentos = 0;
	sock = 0;
	SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)->Iniciando como local.",this->getAgentNumber());
	if (this->files.empty()){
		while(1){
			SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Iniciando la verificacion de la db.",this->getAgentNumber());
			SkynetSyncLocalAgent *agent = SkyNetLocalIndex::checkAll();
			if (agent->work())
				SkyNetSync::addAgent(agent);
			else
				delete(agent);

			if (agent->count()>50){
				agent->join();
			}

			while(SkyNetSync::count() > 5){
				SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)->Hay mas de 5 agentes trabajando, Espero 5seg.",this->getAgentNumber());
				this->sleep(5);
			}
			this->sleep(10);
		}
	} else {
		while(SkyNetSync::count() > 5){
			SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)->Hay mas de 5 agentes trabajando, Espero 5seg.",this->getAgentNumber());
			this->sleep(5);
		}
		while (intentos<3 && sock <= 0 ){
			SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Solicitando la conexión a un maestro, Intento %d",this->getAgentNumber(),intentos);
			sock = Masters::getSock();
			intentos++;
		}

		if(!sock){
			SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)->  No se logro la conexion con un agente",this->getAgentNumber());
			this->end();
			return;
		}

		while(!this->files.empty()){
			file = 	this->files.front();
			SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Sacado a la cola '%s'.",this->getAgentNumber(),file->getName());
			this->files.pop_front();

			switch (file->getStatus()) {
				case FILE_STATUS_UNCHECK:
					SkyNetFile *file2;
					try{
						file2 = new SkyNetFile(file->getName());
					} catch (PathIsNullException *e) {
						SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Archivo %s se encontraba en indice pero no existe en FS.",this->getAgentNumber(),file->getName());
						SkyNetLocalIndex::deleteFile(file);
						delete(file);
						continue;
					}
					delete(file);
					file = file2;
					SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Archivo %s no chequeado se procede a su chequeo.",this->getAgentNumber(),file->getName());
					SkyNetLocalIndex::checkFile(file);
					this->files.push_back(file);

					break;
				case FILE_STATUS_CHECK:

					this->registerFile(file,sock);

					break;
				case FILE_STAUTS_REGISTRED:

					SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> No se que hacer con un archivo registrado.",this->getAgentNumber());

					break;
				case FILE_STATUS_SYNC_FROM_LOCAL:

					this->registerFile(file,sock);

					break;
				case FILE_STATUS_SYNC_FROM_REMOTE:

					this->remoteSync(file,sock);

					break;
				case FILE_STATUS_SYNC_ON_TRASH:
					//? No da tiempo
					break;
				case FILE_STATUS_LOCAL_NEW:
					this->localNew(file,sock);
					break;
				case FILE_STATUS_REMOTE_NEW:
					//? creo que no es necesario
					break;
				case FILE_STATUS_LOCAL_LOCK:
					this->remoteLock(file,sock);
					break;
				case FILE_STATUS_REMOTE_LOCK:
					this->remoteLock(file,sock);
					break;
				default:
					SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)-> Estado de archiv no conocido %d.",this->getAgentNumber(),file->getStatus());
					delete(file);
					break;
			}

		}
		Masters::sendEnd(sock);
	}
	this->end();
}

SkynetSyncLocalAgent::SkynetSyncLocalAgent(): SkyNetSyncAgent() {


}

void SkynetSyncLocalAgent::addFile(SkyNetFile *file) {

	if (file == NULL)
		return;

	this->files.push_front(file);

}

void SkynetSyncLocalAgent::registerFile(SkyNetFile *file,int sock){
	char buff[512];
	SkyNetSyncMsg *msg = new SkyNetSyncMsg("registerFile");

	SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)->  Registrando archivo %s",this->getAgentNumber(),file->getName());

	memset(buff,0,512);

	msg->setVar("default","name",file->getName());
	msg->setVar("default","name_hash",file->getHashName());
	sprintf(buff,"%ld",file->getTimeStamp());
	msg->setVar("default","timeStamp",buff);
	msg->setVar("default","checksum",file->getCheckSum());
	sprintf(buff,"%ld",file->getSize());
	msg->setVar("default","size",buff);

	msg->sendTCP(sock);

	delete(msg);
	msg = SkyNetSyncMsg::generate(sock);
	if(!msg){
		SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)->  No se recivio respuesta al registrar archivo %s.",this->getAgentNumber(),file->getName());
		delete(file);
		return;
	}

	if (strcmp(msg->getAction(),"registerFile")){
		SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)-> Accion del maestro no reconocida %s.",this->getAgentNumber(),msg->getAction());
		delete(file);
		return;
	}

	if (!strcmp(msg->getVar("default","result"),"Sucsses")) {
		SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Se registro sactisfactoriamente archivo %s.",this->getAgentNumber(),file->getName());
		file->setStatus(FILE_STAUTS_REGISTRED);
		SkyNetLocalIndex::saveFile(file);
	} else if (!strcmp(msg->getVar("default","result"),"Fail")){
		SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)-> Error al registrar archivo %s. master: %s",this->getAgentNumber(),file->getName(),msg->getVar("default","Message"));
	} else if (!strcmp(msg->getVar("default","result"),"Repli")) {
		SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Master respondio que se necesita sincronizar el archivo %s.",this->getAgentNumber(),file->getName());
		file->setStatus(FILE_STATUS_SYNC_FROM_REMOTE);
		this->files.push_back(file);
	} else {
		SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)-> Respuesta no reconcida %s.",this->getAgentNumber(),msg->getVar("default","result"));
		this->files.push_back(file);
	}

}

void SkynetSyncLocalAgent::remoteSync(SkyNetFile *file,int sock){

	char buff[512];
	int n_sclavos;
	int sock_slv;
	int fileSync;
	char *ip;
	char *port;
	struct sockaddr_in slv_addr;

	SkyNetSyncMsg *msg = new SkyNetSyncMsg("slaveRepliFile");
	SkyNetSyncMsg *msg2;

	SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Syncronizando archivo '%s'.",this->getAgentNumber(),file->getName());

	memset(buff,0,512);

	msg->setVar("default","name_hash",file->getHashName());
	SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Enviando solicitudad esclavos.",this->getAgentNumber());
	msg->sendTCP(sock);

	delete(msg);
	msg = SkyNetSyncMsg::generate(sock);

	if(!msg){
		SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)->  No se recivio respuesta al solicitar informacion de replica de archivo %s.",this->getAgentNumber(),file->getName());
		delete(file);
		return;
	}
	if (strcmp(msg->getAction(),"slaveRepliFile")){
		SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)-> Accion del maestro no reconocida %s.",this->getAgentNumber(),msg->getAction());
		delete(file);
		return;
	}

	n_sclavos = atoi(msg->getVar("default","count"));
	SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Recividos %d Esclavos desde donde replicar.",this->getAgentNumber(),n_sclavos);
	fileSync = 0;
	for (int i = 0; i < n_sclavos; i++){
		SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Intento con escalvo numero %d.",this->getAgentNumber(),i+1);
		sock_slv = socket(AF_INET, SOCK_STREAM, 0);

		if (sock_slv < 0){
			SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)-> No se logra crear el sock: %s.",this->getAgentNumber(),strerror(errno));
			break;
		}

		sprintf(buff,"Slave-%d",i);
		ip = msg->getVar(buff,"manageip");
		port = msg->getVar(buff,"manageport");

		if (!ip or !port){
			SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)-> No se logra Obtener informacion de esclavo %d.",this->getAgentNumber(),i+1);
			continue;
		}
		SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Se intentara conectar a esclavo %d -> %s:%s.",this->getAgentNumber(),i+1,ip, port);
		Config::setSockaddrIn(ip, port, &slv_addr);

		if (connect(sock_slv, (struct sockaddr*)&slv_addr, sizeof(slv_addr)) < 0) {
			SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)-> No se logra eslabrecer coneccion con %s:%s.",this->getAgentNumber(),ip,port);
			continue;
		}
		msg2 = new SkyNetSyncMsg("getFileData");
		msg2->setVar("default","name_hash",file->getHashName());
		msg2->setVar("default","Checksum",msg->getVar("File","checksum"));
		SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Enviando Mensaje para obtener el archivo.",this->getAgentNumber());
		msg2->sendTCP(sock_slv);

		delete(msg2);
		msg2 = SkyNetSyncMsg::generate(sock_slv);
		SkynetLog::log(SkyNet_LOG_DEBUG,"Ressssssssssssss.............");

		if(!msg2){
			SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)->  No se recivio respuesta del esclavo %s:%s.",this->getAgentNumber(),ip,port);
			Masters::sendEnd(sock_slv);
			continue;
		}
		SkynetLog::log(SkyNet_LOG_DEBUG,"22222222222222.............");
		if (!msg2->isAction("getFileData")){
			SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)-> Accion del sclavo %s:%s no reconocida %s.",this->getAgentNumber(),ip,port,msg2->getAction());
			Masters::sendEnd(sock_slv);
			continue;
		}
		SkynetLog::log(SkyNet_LOG_DEBUG,"3333333333333333333333333333.............");
		if(!strcmp(msg2->getVar("default","result"),"Sucsses")){
			SkynetLog::log(SkyNet_LOG_DEBUG,"dddddddddd.............");
			SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Se Obtiene la nueva información del archivo.",this->getAgentNumber());
			file->setChecksum(msg2->getVar("File","checksum"));
			SkynetLog::log(SkyNet_LOG_DEBUG,"9999999999999999.............");
			file->setTimeStamp(atol(msg2->getVar("File","timeStamp")));
			SkynetLog::log(SkyNet_LOG_DEBUG,"10.............");
			file->setSize(atol(msg2->getVar("File","size")));
			SkynetLog::log(SkyNet_LOG_DEBUG,"111111.............");
			SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Guardando archivo.",this->getAgentNumber());
			msg2->setFile(file);
			try{
				msg2->saveFile(sock_slv);
				Masters::sendEnd(sock_slv);
			} catch (CanNotOpenFileException *e) {
				SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> No se pudo Guardar el archivo.",this->getAgentNumber());
			}
			fileSync = 1;
		}
		SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Finalizando la conexion.",this->getAgentNumber());
		break;
	}

	if (fileSync){
		SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)-> Se sincronizo archivo %s.",this->getAgentNumber(),file->getName());
		file->setStatus(FILE_STATUS_CHECK);
		SkyNetLocalIndex::saveFile(file);
		this->files.push_back(file);
	} else {
		SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)-> No se logro sincronizar archivo %s.",this->getAgentNumber(),file->getName());
		//this->files.push_back(file);
	}
}

void SkynetSyncLocalAgent::remoteLock(SkyNetFile *file,int sock){
	SkyNetSyncMsg *msg = new SkyNetSyncMsg("lockFile");
	msg->setVar("default","name_hash",file->getHashName());

	msg->sendTCP(sock);

	delete(msg);
	msg = SkyNetSyncMsg::generate(sock);

	if(!msg){
		SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)->  No se recivio respuesta al solicitar bloqueo de archivo %s.",this->getAgentNumber(),file->getName());
		return;
	}

	if (strcmp(msg->getAction(),"lockFile")){
		SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)-> Accion del maestro no reconocida %s.",this->getAgentNumber(),msg->getAction());
		return;
	}

	if(strcmp(msg->getVar("default","result"),"Sucsses")){
		SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)-> No se bloqueo el archivo %s.",this->getAgentNumber(),file->getName());
		file->setStatus(FILE_STAUTS_REGISTRED);
		return;
	}
	file->setStatus(FILE_STATUS_REMOTE_LOCK);
}


SkyNetFile *SkynetSyncLocalAgent::getFile(char *hash){

	SkyNetFile *file;
	std::list<SkyNetFile *>::iterator it;

	for (it = this->files.begin(); it != this->files.end(); it++){
		file = *it;
		if (!strcmp(hash,file->getHashName())){
			return file;
		}
	}

	return NULL;
}

void SkynetSyncLocalAgent::localNew(SkyNetFile *file,int sock){

	SkyNetSyncMsg *msg = new SkyNetSyncMsg("unlockFile");
	msg->setVar("default","name_hash",file->getHashName());

	msg->sendTCP(sock);
	delete(msg);
	msg = SkyNetSyncMsg::generate(sock);

	if(!msg){
		SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)->  No se recivio respuesta al solicitar Descloquear archivo %s.",this->getAgentNumber(),file->getName());
		return;
	}

	if (strcmp(msg->getAction(),"unlockFile")){
		SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)-> Accion del maestro no reconocida %s.",this->getAgentNumber(),msg->getAction());
		return;
	}

	if(strcmp(msg->getVar("default","result"),"Sucsses")){
		SkynetLog::log(SkyNet_LOG_ERR,"Agente(%d)-> No se Desbloqueo el archivo %s.",this->getAgentNumber(),file->getName());
		this->files.push_back(file);
		return;
	}
	file->setStatus(FILE_STATUS_UNCHECK);
	this->files.push_back(file);
}

SkynetSyncLocalAgent::~SkynetSyncLocalAgent() {

}

