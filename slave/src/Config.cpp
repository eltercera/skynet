/*
 * Config.cpp
 */

#include <Config.h>
#include <stdlib.h>
#include <string.h>
#include <SkyNetException.h>
#include <SkyNetLocalIndex.h>

const char *defualtHost = "0.0.0.0";
const char *defaultPortService = "5555";
const char *defaultPortMaster = "6666";
const char *defaultPortSync = "7777";

const char *sql_get_config = "select type,host,port from `HOSTS_CONFIG`";

const char *sql_get_config_host = "select host,port from `HOSTS_CONFIG` where type = '%s'";

const char *sql_set_config = "insert into `HOSTS_CONFIG` (`type`,`host`,`port`) values ('%s','%s','%s')";

const char *sql_update_config = "update `HOSTS_CONFIG` set `host` = '%s', `port` = '%s' where `type` = '%s'";

const char *sql_del_config = "delete from `HOSTS_CONFIG` where `type` = '%s'";

struct sockaddr_in Config::service;
struct sockaddr_in Config::masterUnicast;
struct sockaddr_in Config::sync;
struct sockaddr_in Config::masterServer;


int Config::Nat;


sqlResult Config::getConfig(){
	sqlResult res;

	try{
		SkyNetLocalIndex::exceQuery((char *)sql_get_config,&res);
	} catch (exception *e) {
		SkynetLog::log(SkyNet_LOG_ERR,"Error al ejecutar consulta.");
	}
	return res;
}


void Config::init() {

	sqlResult results;
	sqlRow *row;
	const char *host;
	const char *port;
	char sql[500];

	Config::service.sin_family = AF_INET;
	Config::masterUnicast.sin_family = AF_INET;
	Config::sync.sin_family = AF_INET;
	Config::masterServer.sin_family = AF_INET;

	memset(sql,0,500);
	sprintf(sql,sql_get_config_host,HOST_CONFIG_TYPE_SERVICE);
	if(SkyNetLocalIndex::exceQuery(sql,&results)){
		row = results.front();
		host = row->at(0);
		port = row->at(1);
	} else {
		SkynetLog::log(SkyNet_LOG_INFO,"No se encontro configuración para el socket de servicio, se usara el defecto %s:%s",defualtHost,defaultPortService);
		host = defualtHost;
		port = defaultPortService;
	}
	try	{
		Config::setSockaddrIn((char *)host, (char *)port,&Config::service);
	} catch (InvalidServicePortException *e) {
		SkynetLog::log(SkyNet_LOG_INFO,"No se logro configurar socket de servicio con host: %s port: %s.",host,port);
		host = defualtHost;
		port = defaultPortService;
		Config::setSockaddrIn((char *)host, (char *)port,&Config::service);
	}
	SkynetLog::log(SkyNet_LOG_INFO,"Configurado soket de servicio en %s:%s",host,port);
	results.clear();
	memset(sql,0,500);
	sprintf(sql,sql_get_config_host,HOST_CONFIG_TYPE_MASTER);
	if(SkyNetLocalIndex::exceQuery(sql,&results)){
		row = results.front();
		host = row->at(0);
				port = row->at(1);
	} else {
		SkynetLog::log(SkyNet_LOG_INFO,"No se encontro configuración para el socket Master Unicast, se usara el defecto %s:%s",defualtHost,defaultPortMaster);
		host = defualtHost;
		port = defaultPortMaster;
	}
	try	{
		Config::setSockaddrIn((char *)host, (char *)port,&Config::masterUnicast);
	} catch (InvalidServicePortException *e) {
		SkynetLog::log(SkyNet_LOG_INFO,"No se logro configurar socket de MasterUnicat con host: %s port: %s.",host,port);
		host = defualtHost;
		port = defaultPortMaster;
		Config::setSockaddrIn((char *)host, (char *)port,&Config::masterUnicast);
	}
	SkynetLog::log(SkyNet_LOG_INFO,"Configurado soket MasterUnicat en %s:%s",host,port);
	results.clear();
	memset(sql,0,500);
	sprintf(sql,sql_get_config_host,HOST_CONFIG_TYPE_SYNC);
	if(SkyNetLocalIndex::exceQuery(sql,&results)){
		row = results.front();
		host = row->at(0);
				port = row->at(1);
	} else {
		SkynetLog::log(SkyNet_LOG_INFO,"No se encontro configuración para el socket de sincronización, se usara el defecto %s:%s",defualtHost,defaultPortSync);
		host = defualtHost;
		port = defaultPortSync;
	}
	try	{
		Config::setSockaddrIn((char *)host, (char *)port,&Config::sync);
	} catch (InvalidServicePortException *e) {
		SkynetLog::log(SkyNet_LOG_INFO,"No se logro configurar socket de MasterUnicat con host: %s port: %s.",host,port);
		host = defualtHost;
		port = defaultPortSync;
		Config::setSockaddrIn((char *)host, (char *)port,&Config::sync);
	}
	results.clear();
	memset(sql,0,500);
	sprintf(sql,sql_get_config_host,HOST_CONFIG_TYPE_MASTER_S);
	if(SkyNetLocalIndex::exceQuery(sql,&results)){
		row = results.front();
		host = row->at(0);
		port = row->at(1);
	} else {
		SkynetLog::log(SkyNet_LOG_INFO,"No se encontro configuración para la conexion del master");
	}
	try	{
		SkynetLog::log(SkyNet_LOG_INFO,"Seting host-------: %s port: %s.",host,port);

		Config::setSockaddrIn((char *)host, (char *)port,&Config::masterServer);
	} catch (InvalidServicePortException *e) {
		SkynetLog::log(SkyNet_LOG_INFO,"No se logro configurar socket de MasterUnicat con host: %s port: %s.",host,port);
		throw new exception();
	}
	SkynetLog::log(SkyNet_LOG_INFO,"Configurado soket de sincronización en %s:%s",host,port);
}

void Config::setHostInfo(char *ipaddess, char *port, char *type) {

	sqlResult results;
	char sql[500];

	memset(sql,0,500);
	sprintf(sql,sql_get_config_host,type);
	if (SkyNetLocalIndex::exceQuery(sql,&results)){
		memset(sql,0,500);
		sprintf(sql,sql_update_config,ipaddess,port,type);
	} else {
		memset(sql,0,500);
		sprintf(sql,sql_set_config,type,ipaddess,port);
	}
	SkyNetLocalIndex::exceQuery(sql,NULL);
}

void Config::delHostInfo(char *type) {
	char sql[500];

	memset(sql,0,500);
	sprintf(sql,sql_del_config,type);
	SkyNetLocalIndex::exceQuery(sql,NULL);
}

void Config::setSockaddrIn(char *ipaddess, char *port,sockaddr_in *sockadd){

	in_addr_t address;
	int num;

	if (ipaddess != NULL) {
		address = inet_addr(ipaddess);

		if (address == INADDR_NONE)
			throw InvalidServiceIPException();

		sockadd->sin_addr.s_addr=address;
	}

	if (port != NULL) {
		num = atoi(port);
		if (num <= UDP_MIN_PORT ||  num >=UDP_TOP_PORT)
			throw InvalidServicePortException();

		sockadd->sin_port = htons(num);
	}
	sockadd->sin_family = AF_INET;

}

void Config::setNat(int nat){
	Config::Nat = nat;
}

int Config::getNat(){
	return Config::Nat;
}
