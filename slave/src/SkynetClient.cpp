/*
 * SkynetClient.cpp
 *
 *  Created on: Nov 26, 2015
 *      Author: rrodriguez
 */

#include <SkynetClient.h>
#include <UniqueID.h>
#include <string.h>
#include <SkyNetLocalIndex.h>
#include <fcntl.h>
#include <unistd.h>
#include <SkynetSyncLocalAgent.h>
#include <SkyNetSync.h>

const int max_read_size = 2396745;


void StringExplode(string str, string separator, vector<string>* results){
    int found;
    found = str.find_first_of(separator);
    while(found != (int)string::npos){
        if(found > 0){
            results->push_back(str.substr(0,found));
        }
        str = str.substr(found+1);
        found = str.find_first_of(separator);
    }
    if(str.length() > 0){
        results->push_back(str);
    }
}


SkynetClient::~SkynetClient() {

}


void SkynetClient::run()
{
	this->work = 1;
	vector<string> ss = {};
	int salto;

	salto = 0;

	while (this->work){
		this->resv(&ss);
		if (salto < 3){
			if (ss.size() == 2
					&& !ss[0].compare(CP_HELLO)
					&& ss[1].length() == 32 && !ss[1].compare(UniqueID::getString())
					&& UniqueID::slaveStatus() == SID_SLAVE_ENABLE) {
				salto = 0;
				break;
			} else {
				this->sendMsj(CP_ERROR);
				this->sendMsj(CP_END);
			}
			salto++;
		} else {

			this->sendMsj(CP_BYE);
			this->sendMsj(CP_END);
			this->stop();
			return;
		}
	}
	this->sendMsj(CP_HELLO);
	this->sendMsj(CP_END);

	while (1){
		this->resv(&ss);
		if (salto < 3){
			if (ss.size() == 2
					&& !ss[0].compare(CP_IAM)
					&& ss[1].length() == 32 && ss[1].compare(UniqueID::getString())
					&& UniqueID::slaveStatus() == SID_SLAVE_ENABLE) {
				salto = 0;
				UniqueID::setSlaveStatus(SID_SLAVE_DISABLE);
				this->UID = ss[1];
				break;
			} else {
				this->sendMsj(CP_ERROR);
				this->sendMsj(CP_END);
			}
			salto++;
		} else {
			this->sendMsj(CP_BYE);
			this->sendMsj(CP_END);
			this->stop();
			return;
		}
	}
	this->sendMsj(CP_OK);
	this->sendMsj(CP_END);

	while (1){
		this->resv(&ss);
		if (ss.size() <= 0)
			continue;
		if (!ss[0].compare("OPEN")){
			SkynetLog::log(SkyNet_LOG_DEBUG,"Recibiendo de clientes Solicitud de apertura: %s",ss[2].c_str());
			if (ss.size() == 4) {
				this->openCommand(ss[1].c_str(),ss[2].c_str(),ss[3].c_str());
			} else if(ss.size() == 3){
				this->openCommand(ss[1].c_str(),ss[2].c_str(),NULL);
			} else {
				this->sendMsj(CP_FAIL);
				this->sendMsj(CP_LINE);
				this->sendMsj("Formato de OPEN Erroneo.");
			}
			this->sendMsj(CP_END);
		} else if(!ss[0].compare("LOCKFILE")) {
			SkynetLog::log(SkyNet_LOG_DEBUG,"Recibiendo de clientes Solicitud de bloqueo");
			if(!this->file){
				this->sendMsj(CP_FAIL);
				this->sendMsj(CP_LINE);
				this->sendMsj("No tienes un archivo abierto.");
			} else {
				if (!this->lockFile()){
					this->sendMsj(CP_FAIL);
					this->sendMsj(CP_LINE);
					this->sendMsj("El archivo No se pudo bloquear.");
				} else {
					this->sendMsj(CP_SUCCESS);
				}
			}
			this->sendMsj(CP_END);
		} else if(!ss[0].compare("SAVE") and ss.size() == 2) {
			SkynetLog::log(SkyNet_LOG_DEBUG,"Recibiendo de clientes Solicitud de guardado.");
			if(this->file->getStatus()!=FILE_STATUS_REMOTE_LOCK){
				this->sendMsj(CP_FAIL);
				this->sendMsj(CP_LINE);
				this->sendMsj("El archivo no se encontraba bloqueado.");
			} else {

				char name[33];

				memset(name,0,33);
				strcpy(name,this->file->getHashName());

				this->sendMsj(CP_SUCCESS);
				this->sendMsj(CP_END);
				this->saveFile(atol(ss[1].c_str()));
				this->file->setStatus(FILE_STATUS_LOCAL_NEW);
				SkynetSyncLocalAgent *agent = new SkynetSyncLocalAgent();
				agent->addFile(this->file);
				agent->start();
				agent->join();
				this->file = SkyNetLocalIndex::createFile(name);
				if (this->file && this->file->getStatus() == FILE_STAUTS_REGISTRED){
					this->sendMsj(CP_SUCCESS);
				} else {
					this->sendMsj(CP_FAIL);
				}
			}
			this->sendMsj(CP_END);
		} else if (!ss[0].compare(CP_BYE)) {

			if (this->file != NULL && this->file->getStatus() == FILE_STATUS_REMOTE_LOCK){
				this->file->setStatus(FILE_STATUS_LOCAL_NEW);
				SkynetSyncLocalAgent *agent = new SkynetSyncLocalAgent();
				agent->addFile(this->file);
				agent->start();
				agent->join();
			}

			SkynetLog::log(SkyNet_LOG_DEBUG,"Cliente se despidio.");
			this->sendMsj(CP_BYE);
			this->sendMsj(CP_END);
			break;
		} else {
			SkynetLog::log(SkyNet_LOG_DEBUG,"Comando no encontrado %s.",ss[0].c_str());
			this->sendMsj("Comando no Encontrado.");
			this->sendMsj(CP_END);
		}
	}
	UniqueID::setSlaveStatus(SID_SLAVE_ENABLE);
	this->stop();


}

void SkynetClient::start() {
	this->hilo = std::thread([this] {
		this->run();
	});
}

void SkynetClient::stop() {
	this->work = 0;
	shutdown(this->sock,SHUT_WR);
}

SkynetClient::SkynetClient(int sock, struct sockaddr_in sock_in) {

	this->sock = sock;
	this->work = 0;
	this->client_in = sock_in;
	this->file=NULL;

}


void SkynetClient::sendMsj(const char *msj){
	SkynetLog::log(SkyNet_LOG_ERR,"=======================<<<<<: %s",msj);
	send(this->sock,msj,strlen(msj),0);

}

void SkynetClient::resv(vector<string> *a) {

	char buff[1024];
	int len = 0;

	memset(buff,0,1024);

	while (!a->empty())
		a->pop_back();

	len = recv(this->sock, &buff, 1024, 0);
	if (len <=0 ){
		SkynetLog::log(SkyNet_LOG_ERR,"Error al recivir paquete: %s",strerror(errno));
		this->stop();
	}
	SkynetLog::log(SkyNet_LOG_ERR,"=======================>>: %s",buff);
	StringExplode(string(buff),string(" \n\r"),a);
}

void SkynetClient::openCommand(const char *hash,const char *chks,const char *type){
	char buff[100];

	memset(buff,0,100);
	this->file = SkyNetLocalIndex::createFile((char *) hash);

	if (!this->file) {
		SkynetLog::log(SkyNet_LOG_DEBUG,"Cliente trato de abrir un archivo que no existe: %s",hash);
		this->sendMsj(CP_FAIL);
		this->sendMsj(CP_LINE);
		this->sendMsj("El archivo no existe en este servidor.");
		this->sendMsj(CP_END);
		return;
	}

	if (strcmp(this->file->getCheckSum(),chks)){
		SkynetLog::log(SkyNet_LOG_DEBUG,"Cliente trato de abrir un archivo: %s con distindo ckecksum en db. %s <-> %s",hash,this->file->getCheckSum(),chks);
		this->sendMsj(CP_FAIL);
		this->sendMsj(CP_LINE);
		this->sendMsj("El archivo en el servidor contine un checksum distinto.");
		this->sendMsj(CP_END);
		return;
	}

	if(type != NULL){
		if (!this->lockFile()){
			SkynetLog::log(SkyNet_LOG_DEBUG,"Cliente trato de abrir un archivo: %s pero no se pudo bloquear.",hash);
			this->sendMsj(CP_FAIL);
			this->sendMsj(CP_LINE);
			this->sendMsj("El archivo No se pudo bloquear.");
			this->sendMsj(CP_END);
			return;
		}
	}

	this->sendMsj(CP_SUCCESS);
	this->sendMsj(" ");
	sprintf(buff,"%ld",this->file->getSize());
	this->sendMsj(buff);
	this->sendMsj(CP_END);
	this->sendFile();
}

int SkynetClient::lockFile(){
	if (!this->file)
		return 0;

	if(this->file->getStatus()!=FILE_STAUTS_REGISTRED){
		return 0;
	}

	SkynetSyncLocalAgent *agent = new SkynetSyncLocalAgent();

	this->file->setStatus(FILE_STATUS_LOCAL_LOCK);

	SkyNetLocalIndex::saveFile(this->file);

	agent->addFile(this->file);
	agent->start();
	agent->join();

	if(this->file->getStatus()!=FILE_STATUS_REMOTE_LOCK){
		SkyNetLocalIndex::saveFile(this->file);
		return 0;
	}

	SkyNetLocalIndex::saveFile(this->file);
	return 1;
}

void SkynetClient::sendFile() {

	char data [max_read_size];
	int read_size;

	int fd;
	fd = open((const char *)this->file->getName(),O_RDONLY);

	if (fd == -1) {
		throw new CanNotOpenFileException();
	}

	do {
		read_size = 0;
		memset(data,0,max_read_size);
		read_size = read(fd,data,max_read_size);
		if (read_size>0)
			send(this->sock,data,read_size,0);
	}while(read_size>0);

	close(fd);
}

void SkynetClient::saveFile(long size) {

	char data [max_read_size];
	int read_size;
	int read_size_total = 0;

	int fd;
	fd = open((const char *)this->file->getName(),O_WRONLY|O_TRUNC);

	if (fd == -1) {
		throw new CanNotOpenFileException();
	}

	do {
		read_size = 0;
		memset(data,0,max_read_size);
		read_size = read(this->sock,data,size);
		write(fd,data,read_size);
		read_size_total += read_size;
	}while(read_size_total<size);

	close(fd);
}

