/*
 * SkyNetSyncRemoteAgent.cpp
 *
 *  Created on: Dec 28, 2015
 *      Author: rrodriguez
 */

#include <SkyNetSyncRemoteAgent.h>
#include <SkyNetSync.h>
#include <SkynetSyncLocalAgent.h>
#include <SkyNetSyncMsg.h>
#include <sys/socket.h>
#include <unistd.h>
#include <string.h>
#include <SkyNetLocalIndex.h>

int SkyNetSyncRemoteAgent::getType(){
	return 2;
}


SkyNetSyncRemoteAgent::SkyNetSyncRemoteAgent(int sock,struct sockaddr_in client_in) : SkyNetSyncAgent() {

	this->sock = sock;
	this->client_in = client_in;

}

SkyNetSyncRemoteAgent::~SkyNetSyncRemoteAgent(){

}

void SkyNetSyncRemoteAgent::run(){
	SkyNetSyncMsg *mensaje = NULL;
	SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)->Iniciando como remoto.",this->getAgentNumber());
	mensaje = SkyNetSyncMsg::generate(this->sock);
	while(mensaje != NULL){
		if(mensaje->isAction("repliFile")){

			this->repliFile(mensaje);

		} else if(mensaje->isAction("getFileData")){

			this->getFileData(mensaje);
			SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)->iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii",this->getAgentNumber());

		} else if(mensaje->isAction("end")){
			break;
		} else {
			SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Recivida accion desnonocida.",this->getAgentNumber());
		}
		delete(mensaje);
		mensaje = SkyNetSyncMsg::generate(this->sock);
	}
	shutdown(this->sock,SHUT_RDWR);
	close(this->sock);
}

void SkyNetSyncRemoteAgent::getFileData(SkyNetSyncMsg *mensaje) {

	SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Recivido mensaje de 'getFileData'.",this->getAgentNumber());

	SkyNetFile* file = SkyNetLocalIndex::createFile(mensaje->getVar("default","name_hash"));
	SkyNetSyncMsg *resp = new SkyNetSyncMsg("getFileData");
	if (file) {
		if (!strcmp(file->getCheckSum(),mensaje->getVar("default","Checksum"))){
			char buff[512];
			memset(buff,0,512);
			resp->setVar("default","result",(char *)"Sucsses");
			resp->setVar("File","name_hash",file->getHashName());
			sprintf(buff,"%ld",file->getTimeStamp());
			resp->setVar("File","timeStamp",buff);
			resp->setVar("File","checksum",file->getCheckSum());
			sprintf(buff,"%ld",file->getSize());
			resp->setVar("File","size",buff);
			resp->setFile(file);
		} else {
			resp->setVar("default","result",(char *)"Fail");
			resp->setVar("default","Message",(char *)"Checksum distinto.");
		}
	} else {
		resp->setVar("default","result",(char *)"Fail");
		resp->setVar("default","Message",(char *)"no tengo el archivo.");
	}
	resp->sendTCP(this->sock);
	SkynetLog::log(SkyNet_LOG_DEBUG,"Enviadi444444444.");
	delete(resp);
	delete(file);
	SkynetLog::log(SkyNet_LOG_DEBUG,"Enviadi5555555555.");
}

void SkyNetSyncRemoteAgent::repliFile(SkyNetSyncMsg *mensaje) {

	SkyNetSyncMsg *resp;

	SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Recivido mensaje de 'repliFile'.",this->getAgentNumber());

	SkyNetFile* file = SkyNetLocalIndex::createFile(mensaje->getVar("default","name_hash"));

	if (!file) {
		file = new SkyNetFile();
		SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Creando nuevo archivo %s.",this->getAgentNumber(),mensaje->getVar("default","name"));
		file->setName(mensaje->getVar("default","name"));
		SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Creando nuevo archivo %s.",this->getAgentNumber(),file->getName());
		file->setChecksum(mensaje->getVar("default","checksum"));
		file->setSize(atol(mensaje->getVar("default","size")));
		file->setTimeStamp(atol(mensaje->getVar("default","timeStamp")));
		file->setStatus(FILE_STATUS_SYNC_FROM_REMOTE);
	} else {
		if (file->getStatus() == FILE_STATUS_SYNC_FROM_REMOTE){
			SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Ya se sabe que el arhivo tiene que ser replicado '%s'.",this->getAgentNumber(),file->getName());
			resp = new SkyNetSyncMsg("repliFile");
			resp->setVar("default","result",(char *)"Sucsses");
			resp->sendTCP(this->sock);
			delete(resp);
			return;
		} else {
			char *ch = mensaje->getVar("default","checksum");
			long time = atol(mensaje->getVar("default","timeStamp"));
			if(strcmp(ch,file->getCheckSum()) && time >= file->getTimeStamp()){
				file->setChecksum(mensaje->getVar("default","checksum"));
				file->setStatus(FILE_STATUS_SYNC_FROM_REMOTE);
			}
		}

	}

	resp = new SkyNetSyncMsg("repliFile");

	if (file->getStatus() == FILE_STATUS_SYNC_FROM_REMOTE){
		SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Archivo a replicar = '%s'.",this->getAgentNumber(),file->getName());
		SkyNetLocalIndex::saveFile(file);
		resp->setVar("default","result",(char *)"Sucsses");

		SkynetSyncLocalAgent *localA = new SkynetSyncLocalAgent();
		localA->addFile(file);
		SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Metido a la cola '%s'.",this->getAgentNumber(),file->getName());
		SkyNetSync::addAgent(localA);

	} else {
		SkynetLog::log(SkyNet_LOG_DEBUG,"Agente(%d)-> Archivo '%s' no se puede replicar.",this->getAgentNumber(),file->getName());
		resp->setVar("default","result",(char *)"Fail");
		resp->setVar("default","Message",(char *)"Tengo un timestamp Mayor, lo cual no puede pasar.");
	}
	resp->sendTCP(this->sock);
	delete(resp);
}



