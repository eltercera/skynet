/*
 * SkynetLog.cpp
 *
 *  Created on: Dec 24, 2015
 *      Author: rrodriguez
 */

#include <SkynetLog.h>
#include <stdarg.h>


int SkynetLog::log_level;
void SkynetLog::init(int level) {
	SkynetLog::log_level = level;
	openlog("SkynetSlave",LOG_PID | LOG_CONS, LOG_DAEMON);
}

void SkynetLog::close() {
	closelog();
}

void SkynetLog::log(int level,const char *str, ...){
	va_list args;

	if (level <= SkynetLog::log_level) {
		va_start(args,str);
		vsyslog(level,str,args);
		va_end(args);
	}
}
