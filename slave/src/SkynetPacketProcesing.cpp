/*
 * SkynetPacketProcesing.cpp
 */

#include <SkynetPacketProcesing.h>
#include <SkyNetException.h>
#include <Masters.h>


std::thread SkynetPacketProcesing::t;
std::queue<SkyNetPacket *> *SkynetPacketProcesing::packetQueue;
std::mutex SkynetPacketProcesing::queueMutex;
std::mutex SkynetPacketProcesing::mutextCV;
std::condition_variable SkynetPacketProcesing::CV;
int SkynetPacketProcesing::inWork = 0;

void SkynetPacketProcesing::run() {

	SkyNetPacket *pk;

	std::unique_lock<std::mutex> lock(SkynetPacketProcesing::mutextCV);

	while (SkynetPacketProcesing::inWork) {
		SkynetLog::log(SkyNet_LOG_ERR,"Esperando para pocesar paquete");
		SkynetPacketProcesing::CV.wait(lock);
		SkynetLog::log(SkyNet_LOG_ERR,"Recivido paquete para procesar");
		while (!SkynetPacketProcesing::packetQueue->empty()){
			SkynetPacketProcesing::queueMutex.lock();
				pk = SkynetPacketProcesing::packetQueue->front();
				SkynetPacketProcesing::packetQueue->pop();
			SkynetPacketProcesing::queueMutex.unlock();

			try {
				SkynetPacketProcesing::proccessPacket(pk);
			} catch (PaqueteInvalidoException *e) {
				SkynetLog::log(SkyNet_LOG_ERR,"Paquete Invalido Recivido. UDI %s",pk->getUIDString());
			}
		}

	}

}

void SkynetPacketProcesing::startProcesing(){

	if (SkynetPacketProcesing::inWork)
		throw new ProcesingAlreadyStartedException();

	SkynetPacketProcesing::packetQueue = new std::queue<SkyNetPacket *>;
	SkynetPacketProcesing::inWork = 1;

	SkynetPacketProcesing::t = std::thread(SkynetPacketProcesing::run);

}

void SkynetPacketProcesing::addPacket(SkyNetPacket *pk) {
	if (pk == NULL)
		return;

	SkynetPacketProcesing::queueMutex.lock();
		SkynetPacketProcesing::packetQueue->push(pk);
	SkynetPacketProcesing::queueMutex.unlock();
	SkynetPacketProcesing::CV.notify_one();

}

void SkynetPacketProcesing::stopProcesing() {
	if (!SkynetPacketProcesing::inWork)
			return;
	SkynetPacketProcesing::inWork = 0;
	SkynetPacketProcesing::CV.notify_one();
	SkynetPacketProcesing::t.join();
}


void SkynetPacketProcesing::proccessPacket(SkyNetPacket *pk) {
	if (pk == NULL)
		return;
	SkynetLog::log(SkyNet_LOG_ERR,"Procesando Paquete.");
	switch (pk->getAcctionID()) {

		case AID_SLAVE_STATUS:
			SkynetLog::log(SkyNet_LOG_ERR,"Pakete es de slave status.");
			SkynetPacketProcesing::proccessPacketMasterStatus(pk);
			break;
		/*case AID_REGISTER_ACCEPT:
			SkynetPacketProcesing::proccessPacketMasterAcept(pk);
			break;

		case AID_REGISTER_SERVICE_ACCEPT:
			SkynetPacketProcesing::proccessPacketMasterAcept2(pk);
			break;

		case AID_REGISTER_SERVICE_DENY:
			SkynetPacketProcesing::proccessPacketMasterDeny(pk);
			break;

		case AID_REGISTER_DENY:
			SkynetPacketProcesing::proccessPacketMasterDeny(pk);
			break;
		case AID_UNREGISTER_ACCEPT:
			SkynetPacketProcesing::proccessPacketUnregisterAcept(pk);
			break;
		case AID_REGISTER_REJECT:
			SkynetPacketProcesing::proccessPacketRegisterReject(pk);
			break;*/

		default:
			throw new PaqueteInvalidoException();
			break;

	}
	free(pk);
}

void SkynetPacketProcesing::proccessPacketMasterStatus(SkyNetPacket *pk) {
	SkynetMaster *maestro = Masters::getMaster(pk->getUID());
	SkynetLog::log(SkyNet_LOG_ERR,"Tocando a master.");
	maestro->touch(pk->getTimeStamp());
}


/*void SkynetPacketProcesing::proccessPacketMasterAcept(SkyNetPacket *pk) {

	SkynetMaster *maestro = Masters::getMaster(pk->getUID());

	if (maestro == NULL) {
		maestro = new SkynetMaster();
		maestro->setUid(pk->getUID());
		maestro->setStatus(SID_MASTER_OFF);
		maestro->setSockaddr(pk->getSockAddr());
		cout << "Master UID:" << maestro->getUidString() << " Aceptado 1 recivido\n";
		Masters::addMaster(maestro);
	}

}

void SkynetPacketProcesing::proccessPacketMasterAcept2(SkyNetPacket *pk) {

	SkynetMaster *maestro = Masters::getMaster(pk->getUID());
	cout << "Master UID:" << maestro->getUidString() << " Aceptado 2 recivido\n";
	maestro->setStatus(pk->getStatusID());
}

void SkynetPacketProcesing::proccessPacketMasterDeny(SkyNetPacket *pk){
	cout << "El Maestro " << pk->getUIDString() << "Denego el registro \n";
}


void SkynetPacketProcesing::proccessPacketUnregisterAcept(SkyNetPacket *pk){
	Masters::delMaster(pk->getUID());
}

void SkynetPacketProcesing::proccessPacketRegisterReject(SkyNetPacket *pk){

	cout << "Master " << pk->getUIDString() << " Aborto un Maquete de Actualización de estado.\n";
	Masters::delMaster(pk->getUID());

}*/
