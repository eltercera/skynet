/*
 * SkyNetFile.cpp
 *
 *  Created on: Dec 23, 2015
 *      Author: rrodriguez
 */


#include <SkyNetFile.h>
#include <openssl/md5.h>
#include <UniqueID.h>
#include <Config.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>


const int max_read_size = 2396745;

SkyNetFile::SkyNetFile() {
	this->hashName = NULL;
	this->name = NULL;
	this->size = 0;
	this->checkSum = NULL;
	this->status = -1;
	this->timeStamp = 0;
}

SkyNetFile::SkyNetFile(char *path) {

	struct stat st;

	if (path == NULL)
		throw new PathIsNullException();

	if (stat(path, &st) == -1){
		throw new PathIsNullException();
	}

	this->hashName = NULL;
	this->name = NULL;

	this->setName(path);

	this->size = st.st_size;
	this->timeStamp = st.st_mtim.tv_sec;
	this->checkSum = NULL;
	this->loadCHS();
	this->status = FILE_STATUS_UNCHECK;

}

SkyNetFile::SkyNetFile(vector < const char * > *dbrow){

	const char *vv;
	vector < const char * >::iterator it;

	it = dbrow->begin();


	vv = *it;
	this->hashName = new char [strlen(vv)+1];
	memset(this->hashName,0,strlen(vv)+1);
	strcpy(this->hashName,vv);
	it++;
	vv=*it;


	this->name = new char [strlen(vv)+1];
	memset(this->name,0,strlen(vv)+1);
	strcpy(this->name,vv);
	it++;
	vv=*it;


	this->checkSum = new char [strlen(vv)+1];
	memset(this->checkSum,0,strlen(vv)+1);
	strcpy(this->checkSum,vv);

	it++;
	vv=*it;
	this->timeStamp = stol(*it);
	it++;
	this->size = stol(*it);
	it++;

	this->status = stoi(*it);

	delete dbrow;
}

void SkyNetFile::setName(char *name) {
	unsigned char *md5hashname;
	MD5_CTX ctx;

	md5hashname = new unsigned char[16];
	MD5_Init(&ctx);
	MD5_Update(&ctx, name, strlen(name));
	MD5_Final(md5hashname, &ctx);
	this->hashName = UniqueID::getString(md5hashname);
	this->name = new char[strlen(name)+1];
	strcpy(this->name,name);
}


SkyNetFile::~SkyNetFile() {
	delete this->name;
	delete this->hashName;
	delete this->checkSum;
}


void SkyNetFile::loadCHS(){

	unsigned char md5digest [16];
	char data [max_read_size];
	int read_size;

	MD5_CTX ctx;
	int fd;

	fd = open(this->name,O_RDONLY);

	if (fd == -1) {
		throw new CanNotOpenFileException();
	}
	memset(md5digest,0,16);
	MD5_Init(&ctx);
	do {
		read_size = 0;
		memset(data,0,max_read_size);
		read_size = read(fd,data,max_read_size);
		if (read_size > 0){
			MD5_Update(&ctx,data,read_size);
		} else if (read_size < 0) {
			throw new CanNotMakeMd5OfFileException();
		}
	}while(read_size>0);
	MD5_Final(md5digest, &ctx);
	close(fd);
	this->checkSum = UniqueID::getString(md5digest);
}


char *SkyNetFile::getName(){
	return this->name;
}
long SkyNetFile::getTimeStamp(){
	return this->timeStamp;
}
char *SkyNetFile::getCheckSum(){
	return this->checkSum;
}
char *SkyNetFile::getHashName(){
	return this->hashName;
}

long SkyNetFile::getSize(){
	return this->size;
}

int SkyNetFile::getStatus(){
	return this->status;
}

int SkyNetFile::equals(SkyNetFile *file){

	int ret = 0;

	if (strcmp(this->checkSum,file->getCheckSum()) != 0)
		ret = ret | 1;

	if (strcmp(this->hashName,file->getHashName()) != 0)
		ret = ret | 2;

	if (strcmp(this->name,file->getName()) != 0)
		ret = ret | 4;

	if (this->timeStamp != file->getTimeStamp())
		ret = ret | 8;

	if (this->size != file->getSize())
		ret = ret | 16;

	return ret;
}

void SkyNetFile::setStatus(int status){
	this->status = status;
}

void SkyNetFile::setChecksum(char *ch){
	free(this->checkSum);
	this->checkSum = new char [strlen(ch)+1];
	memset(this->checkSum,0,strlen(ch)+1);
	strcpy(this->checkSum,ch);
}
void SkyNetFile::setTimeStamp(long times){
	this->timeStamp = times;
}

void SkyNetFile::setSize(long size){
	this->size = size;
}

