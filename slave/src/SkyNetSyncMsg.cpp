/*
 * SkyNetSyncMsg.cpp
 *
 *  Created on: Jan 10, 2016
 *      Author: rrodriguez
 */

#include <SkyNetSyncMsg.h>
#include <SkyNetException.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <UniqueID.h>
#include <sys/stat.h>

const int max_read_size = 2396745;

SkyNetSyncMsg::SkyNetSyncMsg(const char *action) {
	if (action == NULL)
		throw new exception();

	int len = strlen(action);
	this->action = new char[len+1];
	memset(this->action,0,len+1);
	strcpy(this->action,action);

	this->setVar("default","uid",UniqueID::getString());

	this->file = NULL;
	this->getFileSize = 0;
}

int SkyNetSyncMsg::isAction(const char *action){
	return !strcmp(this->action,action);
}

SkyNetSyncMsg::~SkyNetSyncMsg() {
	int len = strlen(this->action);
	memset(this->action,0,len+1);
	free(this->action);
	context *con;
	contextvar *var;
	while(!this->contexts.empty()){
		con = this->contexts.front();
		len = strlen(con->name);
		memset(con->name,0,len+1);
		free(con->name);

		while(!con->vars.empty()){
			var = con->vars.front();

			len = strlen(var->name);
			memset(var->name,0,len+1);
			free(var->name);

			len = strlen(var->value);
			memset(var->value,0,len+1);
			free(var->value);

			con->vars.pop_front();
			free(var);
		}

		this->contexts.pop_front();
	}
}

context *SkyNetSyncMsg::getCCon(const char *name){
	context *c;
	std::list<context *>::iterator itc;

	for (itc = this->contexts.begin(); itc != this->contexts.end(); ++itc) {
		c = *itc;
		if (!strcmp(c->name,name)){
			return c;
		}
	}
	return NULL;
}
contextvar *SkyNetSyncMsg::gatVVar(context *con,const char *name){
	contextvar *v;
	std::list<contextvar *>::iterator itv;
	if (con == NULL)
		return NULL;
	for (itv = con->vars.begin(); itv != con->vars.end(); itv++) {
		v = *itv;
		if (!strcmp(v->name,name)) {
			return v;
		}
	}
	return NULL;
}

void SkyNetSyncMsg::setVar(const char *cont,const char *name, char *value){
	context *c;
	contextvar *v;
	int len;
	char *nn;

	c = this->getCCon(cont);
	if (name == NULL)
		return;
	if (c) {
		v = this->gatVVar(c,name);
		if(v){

			len = strlen(v->value);
			memset(v->value,0,len+1);
			free(v->value);

			len = strlen(value);
			nn = new char[len+1];
			memset(nn,0,len+1);
			strcpy(nn,value);
			v->value = nn;
		} else {
			v = new contextvar;

			len = strlen(name);
			nn = new char[len+1];
			memset(nn,0,len+1);
			strcpy(nn,name);
			v->name = nn;
			len = strlen(value);
			nn = new char[len+1];
			memset(nn,0,len+1);
			strcpy(nn,value);
			v->value = nn;
			c->vars.push_back(v);
		}
	} else {
		c = new context;

		len = strlen(cont);
		nn = new char[len+1];
		memset(nn,0,len+1);
		strcpy(nn,cont);
		c->name = nn;
		if (name != NULL) {
			v = new contextvar;

			len = strlen(name);
			nn = new char[len+1];
			memset(nn,0,len+1);
			strcpy(nn,name);
			v->name = nn;

			len = strlen(value);
			nn = new char[len+1];
			memset(nn,0,len+1);
			strcpy(nn,value);
			v->value = nn;

			c->vars.push_back(v);
		}
		this->contexts.push_back(c);
	}

}

char *SkyNetSyncMsg::getAction(){
	return this->action;
}
char *SkyNetSyncMsg::getVar(const char *cont,const char *name){
	context *c;
	contextvar *v;

	c = this->getCCon(cont);

	if (c){
		v = this->gatVVar(c,name);
		if (v)
			return v->value;
	}

	return NULL;
}

void SkyNetSyncMsg::sendTCP(int sock){
	char buff [256];

	context *c;
	std::list<context *>::iterator itc;
	contextvar *v;
	std::list<contextvar *>::iterator itv;

	memset(buff,0,256);

	sprintf(buff,"action:%s\r\n",this->action);
	SkynetLog::log(SkyNet_LOG_DEBUG,"<------------- action:%s",this->action);
	send(sock,buff,strlen(buff),0);


	for (itc = this->contexts.begin(); itc != this->contexts.end(); ++itc) {
		c = *itc;
		sprintf(buff,"context:%s\r\n",c->name);
		SkynetLog::log(SkyNet_LOG_DEBUG,"<------------- Context:%s",c->name);
		send(sock,buff,strlen(buff),0);
		for (itv = c->vars.begin(); itv != c->vars.end(); ++itv) {
			v = *itv;
			SkynetLog::log(SkyNet_LOG_DEBUG,"<------------- %s:%s",v->name,v->value);
			sprintf(buff,"%s:%s\r\n",v->name,v->value);
			send(sock,buff,strlen(buff),0);
		}

	}

	if (this->file != NULL) {
		sprintf(buff,"sendfilesize:%ld\r\n",this->file->getSize());
		send(sock,buff,strlen(buff),0);
	}
	sprintf(buff,"\r\n");
	send(sock,buff,strlen(buff),0);
	if (this->file != NULL) {
		this->sendFile(sock);
	}
}

void SkyNetSyncMsg::sendFile(int sock) {

	char data [max_read_size];
	int read_size;

	int fd;
	fd = open(this->file->getName(),O_RDONLY);

	if (fd == -1) {
		throw new CanNotOpenFileException();
	}

	SkynetLog::log(SkyNet_LOG_DEBUG,"Enviiiando Archiiivoooo.");
	int i = 0;

	while(i < this->file->getSize()){
		read_size = 0;
		memset(data,0,max_read_size);
		read_size = read(fd,data,max_read_size);
		send(sock,data,read_size,0);
		i+=read_size;
	}
	SkynetLog::log(SkyNet_LOG_DEBUG,"Enviadi.");
	close(fd);
	SkynetLog::log(SkyNet_LOG_DEBUG,"Enviadioooo.");
}

void SkyNetSyncMsg::saveFile(int sock) {
	char data [max_read_size];
	int read_size;
	int fd;
	fd = open(this->file->getName(),O_WRONLY | O_CREAT,S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
	SkynetLog::log(SkyNet_LOG_DEBUG,"Guarrrrdanto Archiiivoooo. %s . %s",this->file->getName(),strerror(errno));
	char *pointer = &this->file->getName()[2];
	int count = 0;
	char path[1024];
	memset(path,0,1024);
	while (fd == -1) {
		while(strlen(pointer) > 0 and *pointer!='/'){
			path[count]=*pointer;
			count++;
			pointer++;
		}
		if(!strlen(pointer)){
			throw new CanNotOpenFileException();
		}
		mkdir(path, S_IRWXU);
		fd = open(this->file->getName(),O_WRONLY | O_CREAT,S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
		if (fd == -1) {
			path[count]=*pointer;
			count++;
			pointer++;
		}
	}

	int i = 0;

	while(i< this->getFileSize){
		read_size = 0;
		memset(data,0,max_read_size);
		read_size = read(sock,data,max_read_size);
		write(fd,data,read_size);
		i+=read_size;
	};

	close(fd);
}

void SkyNetSyncMsg::setFile(SkyNetFile *file) {

	if (file == NULL)
		return;

	this->file = file;
}

int getvars(char *in,char *out1, char *out2){
	strcpy(out1,strtok(in,":\r\n"));
	strcpy(out2,strtok(NULL,":\r\n"));

	return 1;
}

SkyNetSyncMsg *SkyNetSyncMsg::generate(int sock) {

	SkyNetSyncMsg *msg = NULL;
	char buff[512];
	char var[512];
	char value[512];
	char bb;
	int rev_len;
	int end;
	int rr;

	char lastc[512];


	memset(lastc,0,512);

	end = 0;

	while(!end) {
		memset(buff,0,512);
		rev_len = 0;
		bb = 0;
		while(bb != '\n'){
			rr = recv(sock, &bb, 1, 0);
			if (rr == -1){
				rev_len=0;
				break;
			}
			if (rr == 0){
				break;
			}
			buff[rev_len]=bb;
			rev_len++;
		}


		if (!rev_len){
			SkynetLog::log(SkyNet_LOG_ERR,"Error al recivir datos.");
			break;
		}
		if (buff[0] != '\r' and buff[1] != '\n') {
			memset(var,0,512);
			memset(value,0,512);
			getvars(buff,var,value);
			if(!strcmp(var,"action")){
				SkynetLog::log(SkyNet_LOG_DEBUG,"----->action:%s",value);
				msg = new SkyNetSyncMsg(value);
			} else if(!strcmp(var,"sendfilesize")) {
				msg->getFileSize = atol(value);
				SkynetLog::log(SkyNet_LOG_DEBUG,"----->tamañooooooooooooooo:%s",value);
				end = 1;
			} else if (!strcmp(var,"context")){
				if(!msg) {
					SkynetLog::log(SkyNet_LOG_ERR,"Formato de Protocolo No se envio action de primero");
					continue;
				}
				memset(lastc,0,512);
				strcpy(lastc,value);
				SkynetLog::log(SkyNet_LOG_DEBUG,"----->context:%s",value);
				msg->setVar(lastc,NULL,NULL);
			} else {
				if(!msg) {
					SkynetLog::log(SkyNet_LOG_ERR,"Formato de Protocolo No se envio action de primero");
					continue;
				}
				SkynetLog::log(SkyNet_LOG_DEBUG,"----->%s:%s",var,value);
				msg->setVar(lastc,var,value);
			}
			memset(buff,0,512);
			rev_len = 0;
		} else {
			end = 1;
		}
	}

	return msg;
}


