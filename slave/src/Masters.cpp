/*
 * Masters.cpp
 */

#include <Masters.h>
#include <SkynetUdpSock.h>
#include <Config.h>
#include <unistd.h>
#include <string.h>

std::list<SkynetMaster *> *Masters::list;

pthread_mutex_t *Masters::lock;

void Masters::init(){
	Masters::lock = new pthread_mutex_t;
	Masters::list = new std::list<SkynetMaster *>;
	pthread_mutex_init(Masters::lock,NULL);
}

void Masters::lockList(){

	pthread_mutex_lock(Masters::lock);

}

void Masters::unlockList(){

	pthread_mutex_unlock(Masters::lock);

}

void Masters::trylocklist(){

	pthread_mutex_trylock(Masters::lock);

}

void Masters::addMaster(SkynetMaster *master){

	if (master==NULL)
		return;

	Masters::lockList();

	Masters::list->push_front(master);

	Masters::unlockList();

}

void Masters::delMaster(unsigned char *uid) {

	SkynetMaster *master;

	if (uid==NULL)
		return;

	std::list<SkynetMaster *>::iterator it;

	Masters::lockList();
	it = Masters::list->begin();
	for (it = Masters::list->begin(); it != Masters::list->end(); ++it){
		master = *it;
		if (*(master->getUid()) == *(uid)) {
			it = Masters::list->erase(it);
			Masters::unlockList();
		}
	}
	Masters::unlockList();
	free(master);
}

SkynetMaster *Masters::getMaster(unsigned char *uid){
	std::list<SkynetMaster *>::iterator it;
	SkynetMaster * master;

	if (uid!=NULL) {
		Masters::lockList();
		it = Masters::list->begin();
		for (it = Masters::list->begin(); it != Masters::list->end(); ++it){
			//Masters::trylocklist();
			master = *it;
			if (*(master->getUid()) == *(uid)) {
				Masters::unlockList();
				return master;
			}
		}
		Masters::unlockList();
	} else {
		Masters::lockList();
		it = Masters::list->begin();
		for (it = Masters::list->begin(); it != Masters::list->end(); ++it){
			master = *it;
			if (master->time() < 3) {
				Masters::unlockList();
				return master;
			}
		}
		Masters::unlockList();
		return Masters::list->front();
	}

	return NULL;
}

SkynetMaster *Masters::getMasterBySock(struct sockaddr_in *sock){
	std::list<SkynetMaster *>::iterator it;
	SkynetMaster * master;

	if (sock!=NULL) {
		Masters::lockList();
		it = Masters::list->begin();
		for (it = Masters::list->begin(); it != Masters::list->end(); ++it){
			//Masters::trylocklist();
			master = *it;
			if (master->getSockaddr()->sin_addr.s_addr == sock->sin_addr.s_addr &&
					master->getSockaddr()->sin_port == sock->sin_port) {
				Masters::unlockList();
				return master;
			}
		}
		Masters::unlockList();
	}

	return NULL;
}

void Masters::sendToAll(SkyNetPacket *pk){
	SkynetMaster *master;
	std::list<SkynetMaster *>::iterator it;

	Masters::lockList();
	for (it = Masters::list->begin(); it != Masters::list->end(); ++it){
		master = *it;
		SkynetUdpSock::sendPacket(pk,master);
	}
	Masters::unlockList();
}

void Masters::end(){

	SkynetMaster *master;

	Masters::lockList();

	while (!Masters::list->empty()) {
		master = Masters::list->front();
		Masters::list->pop_front();
	 	free(master);
	}
	Masters::unlockList();
	pthread_mutex_destroy(Masters::lock);
}

void Masters::printStatus(){
	SkynetMaster *master;
	std::list<SkynetMaster *>::iterator it;

	Masters::lockList();
	for (it = Masters::list->begin(); it != Masters::list->end(); ++it){
		master = *it;
		cout << "\nUID: " << master->getUidString() << "\n";
		cout << "Unicast: " << inet_ntoa(master->getSockaddr()->sin_addr) << ":" << htons(master->getSockaddr()->sin_port) << "\n";
	}
	Masters::unlockList();
}

int Masters::getSock(){

	struct sockaddr_in slv_addr;
	int sock;

	if (Masters::list->empty()){
		slv_addr = Config::masterServer;
	} else {
		slv_addr = *Masters::getMaster(NULL)->getSockaddrSync();
	}

	sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock < 0){
		SkynetLog::log(SkyNet_LOG_DEBUG,"No se pudo crear socket.");
		return -1;
	}
	if(connect(sock, (struct sockaddr*)&slv_addr, sizeof(slv_addr))==-1){
		SkynetLog::log(SkyNet_LOG_DEBUG,"No conecto %s",strerror(errno));
		return -1;
	}
	return sock;
}

void Masters::sendEnd(int sock){
	SkyNetSyncMsg *msj = new SkyNetSyncMsg("end");
	msj->sendTCP(sock);
	shutdown(sock,SHUT_RDWR);
	close(sock);
}

int Masters::login(int out){
	int sock = Masters::getSock();
	SkyNetSyncMsg *msj;
	if (sock == -1){
		SkynetLog::log(SkyNet_LOG_ERR,"No se establecio la conexion.");
		return 0;
	}
	char port[100];

	if(!out) {
		msj = new SkyNetSyncMsg("registerSlave");
		msj->setVar("default","serviceIP",inet_ntoa(Config::service.sin_addr));
		sprintf(port,"%d",htons(Config::service.sin_port));
		msj->setVar("default","servicePort",port);
		msj->setVar("default","managerIP",inet_ntoa(Config::sync.sin_addr));
		sprintf(port,"%d",htons(Config::sync.sin_port));
		msj->setVar("default","managerPort",port);
		msj->setVar("default","kaliveIP",inet_ntoa(Config::masterUnicast.sin_addr));
		sprintf(port,"%d",htons(Config::masterUnicast.sin_port));
		msj->setVar("default","kalivePort",port);
	} else {
		msj = new SkyNetSyncMsg("unregisterSlave");
	}

	msj->sendTCP(sock);

	delete(msj);
	msj = SkyNetSyncMsg::generate(sock);
	if (!msj){
		SkynetLog::log(SkyNet_LOG_ERR,"No se recivio una respueta del master.");
		Masters::sendEnd(sock);
		return 0;
	}
	if(msj->isAction("unregisterSlave")){
		Masters::sendEnd(sock);
		delete(msj);
		return 1;
	}else if (!msj->isAction("registerSlave")){
		SkynetLog::log(SkyNet_LOG_ERR,"Codigo Desconocido %s.",msj->getAction());
		Masters::sendEnd(sock);
		delete(msj);
		return 0;
	}
	if (strcmp(msj->getVar("default","result"),"Sucsses")){
		SkynetLog::log(SkyNet_LOG_ERR,"El registro no fue satisfactorio: %s ",msj->getVar("default","message"));
		Masters::sendEnd(sock);
		delete(msj);
		return 0;
	} else {
		SkynetLog::log(SkyNet_LOG_DEBUG,"El registro fue satisfactorio.");

		int nMasters = atoi(msj->getVar("default","mastersCount"));
		char buff[500];

		memset(buff,0,500);
		SkynetMaster *master;

		for(int i = 0; i < nMasters; i++){
			sprintf(buff,"masterServerInfo-%d",i);
			master = new SkynetMaster();
			master->setSockaddr(msj->getVar(buff,"kaliveIP"),msj->getVar(buff,"kalivePort"));
			master->setSockaddrSync(msj->getVar(buff,"managerIP"),msj->getVar(buff,"managerPort"));
			master->setUid(msj->getVar(buff,"uid"));
			Masters::addMaster(master);
		}
	}
	Masters::sendEnd(sock);
	delete(msj);
	SkyNetSync::addAgent(new SkynetSyncLocalAgent());
	return 1;
}
