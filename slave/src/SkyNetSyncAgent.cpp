/*
 * SkyNetSyncAgent.cpp
 */

#include <SkyNetSyncAgent.h>

int SkyNetSyncAgent::agentsCount = 0;

SkyNetSyncAgent::SkyNetSyncAgent() {
	this->agentNumber = SkyNetSyncAgent::getNextCount();
}

SkyNetSyncAgent::~SkyNetSyncAgent() {

}

int SkyNetSyncAgent::getNextCount() {
	SkyNetSyncAgent::agentsCount++;
	return SkyNetSyncAgent::agentsCount;
}

int SkyNetSyncAgent::getAgentNumber(){
	return this->agentNumber;
}

void SkyNetSyncAgent::start(){
	SkynetLog::log(SkyNet_LOG_DEBUG,"Iniciando agente(%d)",this->agentNumber);
	this->hilo = std::thread([this]{
		this->run();
	});
	this->work = 1;
}

int SkyNetSyncAgent::isInWork(){
	return this->work;
}

void SkyNetSyncAgent::end(){
	SkynetLog::log(SkyNet_LOG_DEBUG,"Finalizando agente(%d)",this->agentNumber);
	this->work = 0;
}

void SkyNetSyncAgent::stop(){
	this->work = 0;

	this->hilo.join();
}

void SkyNetSyncAgent::join(){
	this->hilo.join();
}

void SkyNetSyncAgent::sleep(int sec){
	std::this_thread::sleep_for(std::chrono::seconds(sec));
}

