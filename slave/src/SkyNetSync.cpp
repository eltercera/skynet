/*
 * SkyNetSync.cpp
 *
 *  Created on: Dec 28, 2015
 *      Author: rrodriguez
 */

#include <SkyNetSync.h>
#include <SkyNetSyncRemoteAgent.h>
#include <Config.h>
#include <unistd.h>
#include <string.h>


int SkyNetSync::sock;
int SkyNetSync::inWork = 0;
std::thread SkyNetSync::t;
std::list<SkyNetSyncAgent *> SkyNetSync::agnetList;


void SkyNetSync::run(){
	int client;
	struct sockaddr_in server;
	struct sockaddr_in client_in;
	unsigned int client_in_len;
	SkyNetSyncRemoteAgent *ra;

	SkyNetSync::sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	server = Config::sync;
	server.sin_port = Config::sync.sin_port;

	if (bind(SkyNetSync::sock, (struct sockaddr *) &server, sizeof(struct sockaddr)) < 0) {
		throw new SockUdpException();
	}
	if (listen(SkyNetSync::sock,100) < 0) {
		return;
	}

	while(SkyNetSync::inWork) {
		client = accept(SkyNetSync::sock, (struct sockaddr *) &client_in, &client_in_len);
		ra = new SkyNetSyncRemoteAgent(client,client_in);
		SkyNetSync::addAgent(ra);
	}
}

void SkyNetSync::emptyList(){

	SkyNetSyncAgent *agent;

	while (!SkyNetSync::agnetList.empty()) {
		agent = SkyNetSync::agnetList.front();
		SkyNetSync::agnetList.pop_front();
		agent->stop();
		delete agent;
	}
}

int SkyNetSync::addAgent(SkyNetSyncAgent *agent){

	agent->start();

	return 1;
}

void SkyNetSync::startSkynetSyncSock(){

	if (SkyNetSync::inWork)
			throw new ProcesingAlreadyStartedException();
	SkyNetSync::inWork = 1;
	SkyNetSync::t = std::thread(SkyNetSync::run);

}

void SkyNetSync::stopSkynetSyncSock(){

	if (!SkyNetSync::inWork)
		return;
	SkyNetSync::inWork = 0;

	shutdown(SkyNetSync::sock,SHUT_RDWR);
	close(SkyNetSync::sock);
	SkyNetSync::t.join();
}

void *SkyNetSync::getAgentWhitFile(char* hash_name) {

	SkyNetSyncAgent *agent;
	SkynetSyncLocalAgent *agentL;
	std::list<SkyNetSyncAgent *>::iterator it;

	for (it = SkyNetSync::agnetList.begin(); it != SkyNetSync::agnetList.end(); it++){

		agent = *it;
		if (agent->getType() == 2)
			continue;
		agentL = (SkynetSyncLocalAgent *)*it;
		SkyNetFile *ff = agentL->getFile(hash_name);
		if (ff != NULL){
			return ff;
		}
	}
	return NULL;
}

void SkyNetSync::clean() {

	SkyNetSyncAgent *agent;
	std::list<SkyNetSyncAgent *>::iterator it;

	for (it = SkyNetSync::agnetList.begin(); it != SkyNetSync::agnetList.end(); it++){

		agent = *it;

		if (!agent->isInWork()){
			 SkyNetSync::agnetList.remove(agent);
		}
	}

}

int SkyNetSync::count(){
	return SkyNetSync::agnetList.size();
}
