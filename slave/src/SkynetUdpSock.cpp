/*
 * SkynetUdpSock.cpp
 *
 *  Created on: Nov 16, 2015
 *      Author: rrodriguez
 */

#include <SkyNetException.h>
#include <SkynetUdpSock.h>
#include <SkynetPacketProcesing.h>
#include <Config.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <chrono>
#include <UniqueID.h>
#include <Masters.h>
#include <string.h>
#include <unistd.h>

int SkynetUdpSock::sock;
std::thread SkynetUdpSock::t;
std::thread SkynetUdpSock::keepAlive;
int SkynetUdpSock::inWork;
int SkynetUdpSock::inWorkeepAlive;


void SkynetUdpSock::run(){

	SkyNetPacket *pk;

	while (SkynetUdpSock::inWork){
		try{
			SkynetLog::log(SkyNet_LOG_CRIT,"UDP - Esperando por paquete.");
			pk = SkynetUdpSock::recvPacket();
			SkynetLog::log(SkyNet_LOG_CRIT,"Recivido paquete desde");
		} catch (InvalidPacketSizeException *e) {
			SkynetLog::log(SkyNet_LOG_CRIT,"Tamaño del paquete recivido invalido.");
			continue;
		}
		SkynetPacketProcesing::addPacket(pk);
	}

}

void SkynetUdpSock::runKeepAlive(){

	SkyNetPacket *pk;

	while (SkynetUdpSock::inWorkeepAlive){
		std::this_thread::sleep_for(std::chrono::seconds(1));

		pk = new SkyNetPacket();

		pk->setAcctionID(AID_SLAVE_STATUS);
		pk->setServiceAddress(Config::masterUnicast.sin_addr.s_addr);
		pk->setServicePort(Config::masterUnicast.sin_port);
		pk->setStatusID(UniqueID::slaveStatus());
		pk->setUID(UniqueID::get());
		Masters::sendToAll(pk);
		delete(pk);
	}
}

void SkynetUdpSock::init() {

	SkynetUdpSock::sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	if (SkynetUdpSock::sock == -1)
		throw new SockUdpException();

	if (bind(SkynetUdpSock::sock,(struct sockaddr *)&Config::masterUnicast,sizeof(struct sockaddr_in)) == -1)
		throw new SockUdpException();

}

void SkynetUdpSock::startSkynetUdpSock() {

	if (SkynetUdpSock::inWork)
		throw new ProcesingAlreadyStartedException();

	SkynetUdpSock::inWork = 1;
	SkynetUdpSock::t = std::thread(SkynetUdpSock::run);
}

void SkynetUdpSock::startSkynetKeepAlive() {

	if (SkynetUdpSock::inWorkeepAlive)
		throw new ProcesingAlreadyStartedException();

	SkynetUdpSock::inWorkeepAlive = 1;
	SkynetUdpSock::keepAlive = std::thread(SkynetUdpSock::runKeepAlive);
}


SkyNetPacket *SkynetUdpSock::recvPacket() {
	ssize_t rsv = 0;
	char pk[32];
	SkyNetPacket *packet;
	unsigned int client_in_len;
	struct sockaddr_in socka = Config::masterUnicast;


	if (!SkynetUdpSock::sock)
		return NULL;

	rsv = recvfrom(SkynetUdpSock::sock, pk, 32, 0, (struct sockaddr *) &socka, &client_in_len);

	if (rsv < 0){
		throw new SockUdpTimeOutException();
	}

	packet = new SkyNetPacket(pk);
	return packet;
}

void SkynetUdpSock::sendPacket(SkyNetPacket * pk, SkynetMaster *master) {
	ssize_t sends = 0;

	if ( pk == NULL)
		return;
	pk->setTimeStamp();

	sends = sendto(SkynetUdpSock::sock, pk->getPacket(), 32, 0, (struct sockaddr*) master->getSockaddr(), sizeof(struct sockaddr_in));
	if (sends != 32) {
		SkynetLog::log(SkyNet_LOG_CRIT,"Error no se enviaron bien lo 32 bytes. Enviados %d %s %d",sends,strerror(errno));
	}
}

void SkynetUdpSock::stopSkynetUdpSock() {

	if (!SkynetUdpSock::inWork)
		return;

	SkynetUdpSock::inWork = 0;
	shutdown(SkynetUdpSock::sock,SHUT_RDWR);
	close(SkynetUdpSock::sock);
	SkynetUdpSock::t.join();
}

void SkynetUdpSock::stopSkynetKeepAlive() {

	if (!SkynetUdpSock::inWorkeepAlive)
		return;

	SkynetUdpSock::inWorkeepAlive = 0;
	SkynetUdpSock::keepAlive.join();
}
