/*
 * SkyNetSyncMsg.h
 *
 *  Created on: Jan 10, 2016
 *      Author: rrodriguez
 */

#ifndef SKYNETSYNCMSG_H_
#define SKYNETSYNCMSG_H_

#include <list>
#include <SkyNetFile.h>

// Estructura para variablr de contexto
typedef struct __contextvar {
	char *name;
	char *value;
} contextvar;

// Estructura de contexto
typedef struct __context {
	char *name;
	std::list <contextvar *> vars;
} context;

// Clase que representa un mensaje
class SkyNetSyncMsg {

private:
	// Tipo de mensaje
	char *action;

	// lista de Contextos
	std::list <context *> contexts;

	//archivo referente a la sincronizacion
	SkyNetFile *file;

	context *getCCon(const char *name);
	contextvar *gatVVar(context *con,const char *name);
	void sendFile(int sock);


public:
	//Contructores Destructores
	SkyNetSyncMsg(const char *action);
	SkyNetSyncMsg();
	virtual ~SkyNetSyncMsg();

	// Crea o cambia una variable en un contexo
	void setVar(const char *context,const char *name, char *value);

	// Obtiene una variable de un contexto
	char *getVar(const char *context,const char *name);

	// Obtiene el tipo de mensaje
	char *getAction();

	// Envia el mensaje completo po el sock pasado por parametro
	void sendTCP(int sock);

	void setFile(SkyNetFile *file);

	// Comparación rapida del tipo de mensaje
	int isAction(const char *action);

	// Genera un mensaje a partir de los datos que se reciban de un sock
	static SkyNetSyncMsg *generate(int sock);

	long getFileSize;

	// Guarda el archivo en disco.
	void saveFile(int sock);
};

#endif /* SKYNETSYNCMSG_H_ */
