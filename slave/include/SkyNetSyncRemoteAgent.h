/*
 * SkyNetSyncRemoteAgent.h
 *
 *  Created on: Dec 28, 2015
 *      Author: rrodriguez
 */

#ifndef SKYNETSYNCREMOTEAGENT_H_
#define SKYNETSYNCREMOTEAGENT_H_

#include <SkyNetSyncAgent.h>
#include <arpa/inet.h>
#include <SkyNetSyncMsg.h>



class SkyNetSyncRemoteAgent: public SkyNetSyncAgent {

private:
	int sock;
	struct sockaddr_in client_in;
	SkyNetSyncMsg *generate();
	void run();
	void repliFile(SkyNetSyncMsg *mensaje);
	void getFileData(SkyNetSyncMsg *mensaje);

public:
	SkyNetSyncRemoteAgent(int sock,struct sockaddr_in client_in);
	virtual ~SkyNetSyncRemoteAgent();
	virtual int getType();

};

#endif /* SKYNETSYNCREMOTEAGENT_H_ */
