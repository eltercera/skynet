/*
 * SkyNetException.h
 * Definiciones para la captura de Excepciones.
 */


#ifndef SKYNETEXCEPTION_H_
#define SKYNETEXCEPTION_H_
#include <SkyNetSlave.h>
#include <SkyNetPacket.h>
#include <exception>

class InvalidServicePortException: public exception {

};

class InvalidServiceIPException: public exception {

};

class InvalidPacketSizeException: public exception {

};

class ProcesingAlreadyStartedException: public exception {

};

class PaqueteInvalidoException: public exception {

};

class SockUdpException: public exception {

};

class SockUdpTimeOutException: public exception {

};

class PathIsNullException: public exception {

};

class CanNotOpenFileException: public exception {

};

class CanNotOpenIndexDBException: public exception {

};

class CanNotGetMACException: public exception {

};

class CanNotPrepareSqlDBException: public exception {

};

class InvalidStructureDBException: public exception {

};

class CanNotExecQueryDBException: public exception {

};

class FilePanameterNullException: public exception {

};

class CanNotMakeMd5OfFileException: public exception {

};


#endif /* SKYNETEXCEPTION_H_ */
