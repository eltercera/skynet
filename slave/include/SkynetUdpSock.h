/*
 * SkynetUdpSock.h
 *
 *  Created on: Nov 16, 2015
 *      Author: rrodriguez
 */

#ifndef SKYNETUDPSOCK_H_
#define SKYNETUDPSOCK_H_

#include <SkyNetPacket.h>
#include <SkyNetPacket.h>
#include <SkynetMaster.h>
#include <thread>

class SkynetUdpSock {

private:

	// Hilo escucha del sock.
	static std::thread t;

	// Hilo para enviar estado al Master periodicamente.
	static std::thread keepAlive;

	// FD del socket
	static int sock;

	// Estado del hilo
	static int inWork;

	static int inWorkeepAlive;

	// Funcion que se ejecuta dentro del hilo.
	static void run();

public:
	//Funcion que ejecuta el hilo KeepAlive.
	static void runKeepAlive();

	// Inicializa las estructuras
	static void init();

	// Inicia el Hilo
	static void startSkynetUdpSock();

	// Inicia el Hilo
	static void startSkynetKeepAlive();

	// Recibe un paquete del socket
	// Exceptions:
	static SkyNetPacket *recvPacket();

	// Envia un paquete al master
	static void sendPacket(SkyNetPacket * pk, SkynetMaster *master);

	// Detiene el hilo
	static void stopSkynetUdpSock();

	// Detiene el hilo
	static void stopSkynetKeepAlive();
};

#endif /* SKYNETUDPSOCK_H_ */
