/*
 * SkyNetLocalIndex.h
 *
 *  Created on: Dec 23, 2015
 *      Author: rrodriguez
 */

#ifndef SKYNETLOCALINDEX_H_
#define SKYNETLOCALINDEX_H_

#include "SkyNetSlave.h"
#include "SkyNetFile.h"
#include "sqlite3.h"
#include <SkynetSyncLocalAgent.h>
#include <vector>

//Tipos de datos para resultados de una consulta
typedef vector < const char * > sqlRow;
typedef vector< sqlRow *> sqlResult;

class SkyNetLocalIndex {

private:

	/**
	 * Conexión continua a la .loca.db
	 */
	static sqlite3 *db;

	/**
	 * Chequeo de estructura de .loca.db
	 */
	static void checkStructure();


public:

	/**
	 * Ejecuta una sentencia sql en .loca.db
	 */
	static int exceQuery(char *sql,sqlResult *result);

	/**
	 * Prueba si un archivo existe en .loca.db
	 * O -> no existe
	 */
	static int fileExist(SkyNetFile *file);

	/**
	 * Guarda la información de un SkyNetFile en .loca.db
	 */
	static void saveFile(SkyNetFile *file);

	/**
	 * Elimina un archivo en .loca.db
	 */
	static void deleteFile(SkyNetFile *file);

	/**
	 * Renombra un archivo en .loca.db.
	 * Renombra al objeto.
	 */
	static void renameFile(SkyNetFile *file,char *newName);

	/**
	 * Compara un SkyNetFile con la informacion que sen encuentra,
	 * en .loca.db.
	 *
	 * Si el archivo existe y tiemela misma info el estado pasa a FILE_STATUS_CHECK
	 * Si el archiv no existe, guarda su información con estado FILE_STATUS_LOCAL_NEW
	 * Si existe y difiere en el checksum, Se ferifican los timestamp.
	 * 		Si el que es pasado por paramatro tiene mayor timestam este es guardado con estado FILE_STATUS_SYNC_FROM_LOCAL
	 * 		Si el que es pasado por paramatro tiene menor timestam se mantien la info y se marca como FILE_STATUS_SYNC_FROM_REMOTE.
	 * 		Si tienen igual timeStamp cosa muy improvable se coloca como FILE_STATUS_SYNC_FROM_LOCAL
	 */
	static SkyNetFile *checkFile(SkyNetFile *file);

	/**
	 * Inicializacion de la conexión con .loca.db
	 */
	static void init();

	/**
	 * Finalización de la conexión con .loca.db
	 */
	static void end();

	/**
	 * Inicializa todos los archivos en .loca.db a FILE_STATUS_UNCHECK
	 */
	static void init_files_status();

	static SkyNetFile *createFile(char *hName);

	static SkynetSyncLocalAgent *checkAll();

};

#endif /* SKYNETLOCALINDEX_H_ */
