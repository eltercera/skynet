/*
 * SkynetMaster.h
 */

#ifndef SKYNETMASTER_H_
#define SKYNETMASTER_H_

#include <ctime>
#include <thread>
#include <sys/socket.h>
#include <SkyNetException.h>

class SkynetMaster {

private:
	// Estructura para el socket
	struct sockaddr_in *addr_in;

	socklen_t addr_inSize;

	struct sockaddr_in *addr_in_sync;

	socklen_t addr_inSize_sync;

	// UID del mastes
	unsigned char uid [16];

	// Estado del Master
	unsigned char status;

	time_t lastTimeStamp;

public:

	// Constructor
	SkynetMaster();

	// Configura el informacion de la Estructura para el socket
	void setSockaddr(char *ipaddess, char *port) throw(InvalidServicePortException,InvalidServiceIPException) ;

	void setSockaddr(struct sockaddr_in *addr_in);

	void setSockaddrSync(char *ipaddess, char *port) throw(InvalidServicePortException,InvalidServiceIPException) ;

	void setSockaddrSync(struct sockaddr_in *addr_in);

	// Retorna la Estructura para el socket
	struct sockaddr_in *getSockaddr();

	socklen_t getSockaddrSize();

	struct sockaddr_in *getSockaddrSync();

	socklen_t getSockaddrSizeSync();

	// Define el UID del master
	void setUid(unsigned char *uid);

	void setUid(char *uid);

	// Retorna el UID del master
	unsigned char *getUid();

	char *getUidString();

	// Define el estado del master
	void setStatus(unsigned char status);

	// retorna el estado del master
	unsigned char getStatus();

	void touch();

	void touch(time_t lastTimeStamp);

	int time();

	virtual ~SkynetMaster();
};

#endif /* SKYNETMASTER_H_ */
