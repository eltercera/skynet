/*
 * SkynetSyncLocalAgent.h
 *
 *  Created on: Dec 28, 2015
 *      Author: rrodriguez
 */

#ifndef SKYNETSYNCLOCALAGENT_H_
#define SKYNETSYNCLOCALAGENT_H_

#include <SkyNetSync.h>
#include <SkyNetFile.h>
#include <SkyNetException.h>
#include <list>

// implementación de un agente local
class SkynetSyncLocalAgent: public SkyNetSyncAgent {

private:
	// Lista de archivos a procesar
	std::list <SkyNetFile *> files;

	// Implementacion de el metodo abstacto
	void run();

	// Accion de resgistrar un archivo
	void registerFile(SkyNetFile *file,int sock);

	// accion de sincronizar un archivo desde otro esclavo
	void remoteSync(SkyNetFile *file,int sock);

	void remoteLock(SkyNetFile *file,int sock);

	void localNew(SkyNetFile *file,int sock);

public:
	// Constructor Destructor
	SkynetSyncLocalAgent();
	virtual ~SkynetSyncLocalAgent();
	// Agrega un archivo a la lista
	void addFile(SkyNetFile *file);
	int work();
	void clean();
	int count();
	SkyNetFile *getFile(char *hash);
	virtual int getType();
};

#endif /* SKYNETSYNCLOCALAGENT_H_ */
