/*
 * UniqueID.h
 */

#ifndef UNIQUEID_H_
#define UNIQUEID_H_

#include <SkyNetSlave.h>


class UniqueID {
private:

	// ByteArray que representa el Unique ID
	static unsigned char *uniqueID;

	// Funcion que retorna un string con la mac address
	static char *getMac(int nat);

	static unsigned char status;

public:

	// Genera el uniqueID
	// Retorna 1 si no se genero por un error,
	// 0 si se genero
	static void generate();

	// Retrona el ByteArray que representa el Unique ID
	static unsigned char *get();

	// Retrona la representacion en string del MD5
	static char *getString();

	// Retrona la representacion en string del MD5 pasado
	// por el parametro uid
	static char *getString(unsigned char uid [16]);

	static unsigned char slaveStatus();

	static void setSlaveStatus(unsigned char sta);

};

#endif /* UNIQUEID_H_ */
