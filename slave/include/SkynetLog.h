/*
 * SkynetLog.h
 *
 *  Created on: Dec 24, 2015
 *      Author: rrodriguez
 */

#ifndef SKYNETLOG_H_
#define SKYNETLOG_H_

#include <syslog.h>

/* Niveles de log */
enum _ivrs_log_level {
	SkyNet_LOG_EMERG = 0,
	SkyNet_LOG_ALERT = 1,
	SkyNet_LOG_CRIT = 2,
	SkyNet_LOG_ERR = 3,
	SkyNet_LOG_WARNING = 4,
	SkyNet_LOG_NOTICE = 5,
	SkyNet_LOG_INFO = 6,
	SkyNet_LOG_DEBUG = 7
};


class SkynetLog {

private:
	/* nivel actual de log */
	static int log_level;

public:

	/* Inicialización de la conexión con syslog del systema */
	static void init(int level);
	/* Cerrar la conexión con syslog del systema */
	static void close();
	/* Envio de un mensaje log */
	static void log(int level,const char *str, ...);
};

#endif /* SKYNETLOG_H_ */
