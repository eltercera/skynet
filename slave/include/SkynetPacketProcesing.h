/*
 * SkynetPacketProcesing.h
 *
 *  Created on: Nov 16, 2015
 *      Author: rrodriguez
 */

#ifndef SKYNETPACKETPROCESING_H_
#define SKYNETPACKETPROCESING_H_

#include <condition_variable>
#include <SkyNetSlave.h>
#include <SkyNetPacket.h>
#include <thread>
#include <mutex>
#include <queue>
#include <condition_variable>


class SkynetPacketProcesing {

private:

	// Hilo
	static std::thread t;

	// Cola de paquetes
	static std::queue<SkyNetPacket *> *packetQueue;

	// Mutex para la cola de paquete.
	static std::mutex queueMutex;

	// Mutex para la variable de condición
	static std::mutex mutextCV;

	// Variable de condición
	static std::condition_variable CV;

	// Estado de procesamiento
	static int inWork;

	// Inicializador de las estructuras
	static void run();

	// Logica de procesamiento de paquetes.
	static void proccessPacket(SkyNetPacket *pk);

	// Procesamiento de los distintos Paquetes
	/*static void proccessPacketMasterStatus(SkyNetPacket *pk);

	static void proccessPacketMasterAcept(SkyNetPacket *pk);

	static void proccessPacketUnregisterAcept(SkyNetPacket *pk);

	static void proccessPacketMasterAcept2(SkyNetPacket *pk);

	static void proccessPacketMasterDeny(SkyNetPacket *pk);

	static void proccessPacketRegisterReject(SkyNetPacket *pk);*/

	static void proccessPacketMasterStatus(SkyNetPacket *pk);

public:

	// Uniciar el procesamiento de paquetes
	static void startProcesing();

	// Agregar un paquete a la cola
	static void addPacket(SkyNetPacket *pk);

	// Detener el procesamiento de paquetes
	static void stopProcesing();

};

#endif /* SKYNETPACKETPROCESING_H_ */
