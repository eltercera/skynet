/*
 * skynetSlave.h
 * Header principal del servidor esclavo.
 */

#ifndef SKYNETSLAVE_H_
#define SKYNETSLAVE_H_

//#define SKYNETSLAVE_WINDOWS

#define SKYNETSLAVE_LINUX


// Librerias de estandares POSIX
#include <iostream>
#include <cstdlib>
#include <SkynetLog.h>
using namespace std;

// Constantes globales
#define PROGRAM_NAME "SkyNet Slave Server"
#define PROGRAM_VERSION  "0.1"

#define BYTE_NULL 0x00

#define UDP_TOP_PORT 65535
#define UDP_MIN_PORT 10

#define AID_SLAVE_STATUS 2

#define SID_SLAVE_ENABLE 1
#define SID_SLAVE_DISABLE 2
#define SID_SLAVE_DOWN 3

#define SID_MASTER_ON 4
#define SID_MASTER_OFF 5

#define CP_HELLO "HELLO"
#define CP_IAM "IAM"
#define CP_BYE "BYE"
#define CP_OK "OK"
#define CP_SUCCESS "SUCCESS"
#define CP_FAIL "FAIL"
#define CP_ERROR "ERROR"
#define CP_LINE "\r\n"
#define CP_END "\r\n\r\n"

#endif /* SKYNETSLAVE_H_ */
