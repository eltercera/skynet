/*
 * SkynetTCPSock.h
 *
 *  Created on: Nov 26, 2015
 *      Author: rrodriguez
 */

#ifndef SKYNETTCPSOCK_H_
#define SKYNETTCPSOCK_H_

#include <SkyNetSlave.h>
#include <thread>
#include <SkynetClient.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <Config.h>
#include <list>

class SkynetTCPSock {

private:
	// Hilo escucha del sock.
	static std::thread t;

	// Lista de clientes
	static std::list<SkynetClient *> clients;

	// Bind Sock
	static int sock;

	// Variable de estado de trabajo para el hilo
	static int inWork;

	// Ejecución del hilo
	static void run();

	// Vacia la lista de clientes
	static void emptyList();
public:

	// Inicia el hilo de escucha TCP
	static void startSkynetTCPSock();

	// Dinaliza el hilo de Escucha
	static void stopSkynetTCPSock();

};

#endif /* SKYNETTCPSOCK_H_ */
