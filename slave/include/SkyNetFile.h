/*
 * SkyNetFile.h
 */

#ifndef SKYNETFILE_H_
#define SKYNETFILE_H_

#include "SkyNetSlave.h"
#include "SkyNetException.h"
#include <vector>

// Constante para los estados de un archivo.
#define FILE_STATUS_REMOTE_NEW 7
#define FILE_STATUS_LOCAL_NEW 6
#define FILE_STATUS_SYNC_ON_TRASH 5
#define FILE_STATUS_SYNC_FROM_REMOTE 4
#define FILE_STATUS_SYNC_FROM_LOCAL 3
#define FILE_STAUTS_REGISTRED 2
#define FILE_STATUS_CHECK 1
#define FILE_STATUS_UNCHECK 0

#define FILE_STATUS_LOCAL_LOCK 8
#define FILE_STATUS_REMOTE_LOCK 9


/**
 * Clase para que representa la información de un
 * archivo dentro de sistema de archivo.
 */
class SkyNetFile {

private:

	/**
	 * Nombre del archivo, Path completo desde
	 * el directorio raiz.
	 */
	char *name;

	/**
	 * TimeStamp en segundos
	 */
	long timeStamp;

	/**
	 * MD5 checksum del archivo.
	 */
	char *checkSum;

	/**
	 * MD5 hash de name
	 */
	char *hashName;

	/**
	 * Tamaño del archivo en bytes.
	 */
	long size;

	/**
	 * Estado de un archivo.
	 */
	int status;

	/**
	 * Miembro privado para la carga del checksum
	 */
	void loadCHS();


public:
	/**
	 * Constructor a partir de un path.
	 */
	SkyNetFile();
	SkyNetFile(char * path);
	SkyNetFile(vector < const char * > *dbrow);
	virtual ~SkyNetFile();

	/**
	 * Gets and Sets.
	 */
	char *getName();
	long getTimeStamp();
	char *getCheckSum();
	char *getHashName();
	long getSize();
	int getStatus();
	void setName(char *name);
	void setStatus(int status);
	void setChecksum(char *ch);
	void setTimeStamp(long times);
	void setSize(long size);

	/**
	 * Para comparar el archivo actual con otro.
	 * retorna 0 si son iguales.
	 * Para indicar las diferencias se encienden los bit
	 * del entero retornado.
	 *
	 * bit 0 -> Checksum Diferente.
	 * bit 1 -> hashName Diferente.
	 * bit 2 -> name Diferente.
	 * bit 3 -> timeStamp diferente
	 * bir 4 -> size Diferente.
	 */
	int equals(SkyNetFile *file);

};

#endif /* SKYNETFILE_H_ */
