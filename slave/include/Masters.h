/*
 * Masters.h
 *
 *  Created on: Nov 15, 2015
 *      Author: rrodriguez
 */

#ifndef MASTERS_H_
#define MASTERS_H_

#include <SkyNetSlave.h>
#include <SkynetMaster.h>
#include <list>
#include <thread>
#include <SkyNetSyncMsg.h>


typedef struct _masterShortInfo {
	char uid [33];
	char ipPort [33];
} masterShortInfo;

class Masters {

private:

	// Estructura de lista
	static std::list<SkynetMaster *> *list;

	// Exclucion mutua
	static pthread_mutex_t *lock;

	// Bloque a lista
	static void lockList();

	// Desbloquea la lista
	static void unlockList();

	// Espera si la lista esta bloqueada
	static void trylocklist();

public:

	// Inicializa las escruturas necesarias
	static void init();

	// Agrega un master
	static void addMaster(SkynetMaster *master);

	// Barra el master en el IDU pasado
	static void delMaster(unsigned char *uid);

	// Retorna un master el el uid pasado
	static SkynetMaster *getMaster(unsigned char *uid);

	static SkynetMaster *getMasterBySock(struct sockaddr_in *sock);

	static void sendToAll(SkyNetPacket *pk);

	static void printStatus();

	//Retorna un sock de cualquier master listo para comunicarse
	static int getSock();

	// Destruye todo.
	static void end();

	static int login(int out);

	static void sendEnd(int sock);
};

#endif /* MASTERS_H_ */
