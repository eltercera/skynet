/*
 * SkynetPacket.h
 * Definicion de la clase SkynetPacket
 */

#ifndef SKYNETPACKET_H_
#define SKYNETPACKET_H_

#include <arpa/inet.h>
#include <SkyNetException.h>
#include <SkyNetSlave.h>

class SkyNetPacket {

private:
	// 32 Bytes que representa a el paquete
	unsigned char packet[32];

public:

	// Contructor a partir de un arreglo de bytes
	// dispara un InvalidPacketSizeException si la estructura
	// pasada en datagrames mayor a 32 bytes.
	SkyNetPacket(char *datagram);

	// Constructor vacio
	SkyNetPacket();

	// Asigna una acction_id al paquete
	void setAcctionID(char acction_id);

	// Asigna un status_id al paquete
	void setStatusID(char status_id);

	// Asigna un service_port al paquete
	// int port tiene que ser menor a 2^16 y mayor que 0
	// de lo contrario dispara un InvalidServicePortException
	void setServicePort(int port);

	// Asigna un service_address al paquete
	void setServiceAddress(uint32_t service_address);

	// Asigna un timeStamp al paquete
	void setTimeStamp(uint64_t timestamp);

	// Asigna el timeStamp Actual al paquete
	void setTimeStamp();

	// Asigna un UID al paquete
	void setUID(unsigned char digest[16]);

	// Retorna el action_id del paquete
	char getAcctionID();

	// Retorna el status_id del paquete
	char getStatusID();

	// Retorna el service_port del paquete
	int getServicePort();

	// Reporta una estructura sockaddr_in para usarla directo en el un
	// sokc apartir del service_addres en el paquete
	struct sockaddr_in *getSockAddr();

	// Retorta el serivice_addres del paquete
	uint32_t getServiceAddr();

	// Retorta el timestamp del paquete
	uint64_t getTimeStamp();

	// Reporta el UID el paquete
	unsigned char *getUID();

	// Reporta un MD5 String a partir del UID del paquette
	char *getUIDString();

	// Retorna todo el paquete en un byteArray
	unsigned char *getPacket();

	virtual ~SkyNetPacket();
};



#endif /* SKYNETPACKET_H_ */
