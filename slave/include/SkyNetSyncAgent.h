/*
 * SkyNetSyncAgent.h
 *
 *  Created on: Dec 28, 2015
 *      Author: rrodriguez
 */

#ifndef SKYNETSYNCAGENT_H_
#define SKYNETSYNCAGENT_H_


#include <thread>
#include <SkyNetSlave.h>

// Clase Abstacta que representa a un agente
class SkyNetSyncAgent {

private:
	//hilo para un agente
	std::thread hilo;

	// metodo a implementar por carda tipo de agente
	virtual void run() = 0;

	// Numero de agente
	int agentNumber;

	// Para identificar si esta trabajando
	int work;

	// Contador estatico del numero de agente
	static int agentsCount;



public:
	// Construnctores Destructores
	SkyNetSyncAgent();
	virtual ~SkyNetSyncAgent();

	// inicia el trabajdo del agente
	virtual void start();

	virtual int getType() = 0;

	// Detiene al agente
	virtual void stop();

	// Obtiene el sigiente contador de agente
	static int getNextCount();

	// Ombiene el numero del agente
	int getAgentNumber();

	// Si el agente esta trabajando
	int isInWork();

	// finaliza el trabajo del agente
	void end();
	void join();
	void sleep(int sec);
};

#endif /* SKYNETSYNCAGENT_H_ */
