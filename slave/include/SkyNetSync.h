/*
 * SkyNetSync.h
 *
 *  Created on: Dec 28, 2015
 *      Author: rrodriguez
 */

#ifndef SKYNETSYNC_H_
#define SKYNETSYNC_H_

#include <arpa/inet.h>
#include <sys/socket.h>
#include <SkyNetSlave.h>
#include <SkyNetSyncAgent.h>
#include <list>
#include <SkynetSyncLocalAgent.h>

// Clase para contener metodos estaticos para la sincronizacion
// de archivos.
class SkyNetSync {

private:

	//Lista de agentes activos
	static std::list<SkyNetSyncAgent *> agnetList;

	//Hilo para socket de escucha de agentes remotos
	static std::thread t;

	//sock
	static int sock;

	static int inWork;

	static void run();

	static void emptyList();

public:

	static void *getAgentWhitFile(char *hash_name);

	//Agrega un agente a la lista
	static int addAgent(SkyNetSyncAgent *agent);

	//Inicializacion de escucha y procesamiento de agentes remotos
	static void startSkynetSyncSock();

	// detener.
	static void stopSkynetSyncSock();

	// limpia la lista de agentes muertos.
	static void clean();

	static int count();
};

#endif /* SKYNETSYNC_H_ */
