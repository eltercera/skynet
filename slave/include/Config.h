/*
 * Config.h
 */

#ifndef CONFIG_H_
#define CONFIG_H_

#include <netinet/in.h>
#include <arpa/inet.h>
#include <SkyNetSlave.h>
#include <SkyNetLocalIndex.h>

//Constantes para guardar en db.
#define HOST_CONFIG_TYPE_SERVICE "service"
#define HOST_CONFIG_TYPE_MASTER "master"
#define HOST_CONFIG_TYPE_MASTER_S "masterserver"
#define HOST_CONFIG_TYPE_SYNC "sync"


class Config {

public:

	// Información de socket para el sevicio
	static struct sockaddr_in service;

	// Información de socket para la conxión contra el
	// Master
	static struct sockaddr_in masterUnicast;

	// Información de socket para la conexión de sincronizacion.
	static struct sockaddr_in sync;

	static struct sockaddr_in masterServer;

	// Size le
	static socklen_t syncSize;

	// 1 si esta detras de un nat
	static int Nat;


	static sqlResult getConfig();

	// Inicializa las estructuras service, masterUnicast, masterMulticast
	static void init();

	// Cambia la variable Nat
	static void setNat(int nat);

	// Obtiene la variable Nat
	static int getNat();

	// Configura la estructura service
	static void setHostInfo(char *ipaddess, char *port, char *type);

	static void delHostInfo(char *type);

	// Configura un sockaddr_in a partir de una direccion y pueto
	static void setSockaddrIn(char *ipaddess, char *port,sockaddr_in *sockadd);

};

#endif /* CONFIG_H_ */
