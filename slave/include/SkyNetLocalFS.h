/*
 * SkyNetLocalFS.h
 *
 *  Created on: Dec 26, 2015
 *      Author: rrodriguez
 */

#ifndef SKYNETLOCALFS_H_
#define SKYNETLOCALFS_H_

#include <SkyNetSlave.h>


class SkyNetLocalFS {


public:

	/**
	 * Chequeo de archivos en directorio local.
	 */
	static void checkLocalDir();

};

#endif /* SKYNETLOCALFS_H_ */
