/*
 * SkynetClient.h
 */

#ifndef SKYNETCLIENT_H_
#define SKYNETCLIENT_H_

#include <SkyNetSlave.h>
#include <thread>
#include <string.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <vector>
#include <string.h>
#include <SkyNetFile.h>

// Clase que contiene la informacion de un cliente
class SkynetClient {

private:
	// Sock de connexión
	int sock;

	// Identificador Unico del cliente
	std::string UID;

	// Hilo de ejecución para el cliente
	std::thread hilo;

	int work;

	// Información del Sock
	struct sockaddr_in client_in;

	SkyNetFile *file;

	int split(char *data,string **dd);

	// Ejecución del Hilo
	void run();

	void openCommand(const char *hash,const char *chks,const char *type);

	void sendFile();

	int lockFile();

	void saveFile(long size);

public:
	// Constrctor
	SkynetClient(int sock, struct sockaddr_in sock_in);

	// Inicia el hilo de ejecucion
	void start();

	// Detiene el hilo de ejecución
	void stop();

	// Envia un mensaje al cliente
	void sendMsj(const char *mensaje);

	// Recive un mensaje del cliente
	void  resv(vector<string>  *a);

	virtual ~SkynetClient();

};

#endif /* SKYNETCLIENT_H_ */
