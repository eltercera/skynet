package skynetclient;

import Controlador.DispatcherPortType;
import Controlador.DispatcherService;
import java.awt.Color;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.UIManager;
import Modulo.*;
import java.net.MalformedURLException;
import java.net.URL;
import Controlador.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class TreeView extends javax.swing.JFrame {
    
    private static boolean useSystemLookAndFeel = false;
    
    public TreeView(String[] dir, String[] ar_m5) {
        getContentPane().setBackground(Color.decode("#26a9e9"));
        initComponents();
        initMyComponents(dir, ar_m5);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jP_Tree = new javax.swing.JPanel();
        jB_Solicitar = new javax.swing.JButton();
        jB_Editar = new javax.swing.JButton();
        jB_summary = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jP_Tree.setBackground(new java.awt.Color(254, 254, 254));
        jP_Tree.setMaximumSize(new java.awt.Dimension(376, 244));
        jP_Tree.setPreferredSize(new java.awt.Dimension(377, 244));

        javax.swing.GroupLayout jP_TreeLayout = new javax.swing.GroupLayout(jP_Tree);
        jP_Tree.setLayout(jP_TreeLayout);
        jP_TreeLayout.setHorizontalGroup(
            jP_TreeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 376, Short.MAX_VALUE)
        );
        jP_TreeLayout.setVerticalGroup(
            jP_TreeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 244, Short.MAX_VALUE)
        );

        jB_Solicitar.setText("Solicitar");
        jB_Solicitar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_SolicitarMouseClicked(evt);
            }
        });

        jB_Editar.setText("Editar");
        jB_Editar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_EditarMouseClicked(evt);
            }
        });

        jB_summary.setText("Summary");
        jB_summary.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jB_summaryMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jP_Tree, javax.swing.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(jB_summary)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jB_Solicitar)
                .addGap(55, 55, 55)
                .addComponent(jB_Editar)
                .addGap(73, 73, 73))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jP_Tree, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jB_Solicitar)
                    .addComponent(jB_Editar)
                    .addComponent(jB_summary))
                .addContainerGap(27, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jB_SolicitarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_SolicitarMouseClicked
        URL url = null;
        try {
            url = new URL(SkyNetClient.ip);
        } catch (MalformedURLException ex) {
            Logger.getLogger(SkyNetClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        DispatcherService dispat = new DispatcherService(url); 
        DispatcherPortType port = dispat.getDispatcher();
        OpenFile esc = new OpenFile();
        esc.setHashName(TreePaneFile.aux_m5);
        OpenFileResponse info = port.openFile(esc);
        OpenFileResponsefile info_slave = info.getFile();
        if (info_slave.getResult().equalsIgnoreCase("Sucsses")){
            SkyNetClient.sock= new SkyNetClientSocket(info_slave.getServerName(), info_slave.getServerPort());
            String respuesta = SkyNetClient.sock.SendToSlave(("HELLO "+info_slave.getSuid()));
            respuesta = SkyNetClient.sock.SendToSlave("IAM "+SkyNetClient.ClientUID);
            byte[] bytes;
            bytes = SkyNetClient.sock.SendToOpen("OPEN "+TreePaneFile.aux_m5+" "+info_slave.getChecksum());
            SkyNetDocument Editor = new SkyNetDocument(false, bytes);
            Editor.setVisible(true);
            dispose();
        }
        else
           JOptionPane.showMessageDialog(null, info_slave.getMessage());

    }//GEN-LAST:event_jB_SolicitarMouseClicked

    private void jB_summaryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_summaryMouseClicked
        URL url = null;
        try {
            url = new URL(SkyNetClient.ip);
        } catch (MalformedURLException ex) {
            Logger.getLogger(SkyNetClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DispatcherService dispat = new DispatcherService(url); 
        DispatcherPortType port = dispat.getDispatcher();
        Controlador.FileInfo fileinf = new Controlador.FileInfo();
        fileinf.setHashName(TreePaneFile.aux_m5);
        FileInfoResponse infos = port.fileInfo(fileinf);
        FileInfoResponseFileInfo info_file = infos.getFileInfo();
        SkynetSummaryFile sum = null;
        if (info_file.getResult().equalsIgnoreCase("Sucsses")) {
             sum = new SkynetSummaryFile(info_file.getName(), info_file.getChecksum(), info_file.getNameHash());
             sum.setVisible(true);
        }
        else
             JOptionPane.showMessageDialog(null, "Error opteniendo informacion");

    }//GEN-LAST:event_jB_summaryMouseClicked

    private void jB_EditarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jB_EditarMouseClicked
        URL url = null;
        try {
            url = new URL(SkyNetClient.ip);
        } catch (MalformedURLException ex) {
            Logger.getLogger(SkyNetClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        DispatcherService dispat = new DispatcherService(url); 
        DispatcherPortType port = dispat.getDispatcher();
        OpenFile esc = new OpenFile();
        esc.setHashName(TreePaneFile.aux_m5);
        OpenFileResponse info = port.openFile(esc);
        OpenFileResponsefile info_slave = info.getFile();
        if (info_slave.getResult().equalsIgnoreCase("Sucsses")){
            SkyNetClient.sock= new SkyNetClientSocket(info_slave.getServerName(), info_slave.getServerPort());
            String respuesta = SkyNetClient.sock.SendToSlave(("HELLO "+info_slave.getSuid()));
            respuesta = SkyNetClient.sock.SendToSlave("IAM "+SkyNetClient.ClientUID);
            byte[] bytes;
            bytes = SkyNetClient.sock.SendToOpen("OPEN "+TreePaneFile.aux_m5+" "+info_slave.getChecksum()+" W");
            SkyNetDocument Editor = new SkyNetDocument(true, bytes);
            Editor.setVisible(true);
            dispose();
        }
        else
           JOptionPane.showMessageDialog(null, info_slave.getMessage());
    }//GEN-LAST:event_jB_EditarMouseClicked
     
     private void initMyComponents(String[] dir, String[] ar_m5){
         createAndShowGUI(dir, ar_m5);
     }
    private void createAndShowGUI(String[] dir, String[] ar_m5) {
        if (useSystemLookAndFeel) {
            try {
                UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
            } catch (Exception e) {
                System.err.println("Couldn't use system look and feel.");
            }
        }
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jP_Tree.setBackground(Color.white);
        jP_Tree.setLayout(new BoxLayout(jP_Tree, BoxLayout.Y_AXIS));

        //Add content to the window.
        jP_Tree.add(new TreePaneFile(dir,ar_m5));
        jP_Tree.revalidate();

    }
    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                String[] a ={"./Here/ThisOne.txt","./Here/maybe/OtherOne.txt","./Here/stuff/OtherOne.txt","./Other/Cap.txt","./Here/stuff/OtherTwo.txt"};
                String[] b={"Here/ThisOne.txt","Here/maybe/OtherOne.txt","Here/stuff/OtherOne.txt","Other/Cap.txt","Here/stuff/OtherTwo.txt"};
                new TreeView(a,b).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jB_Editar;
    private javax.swing.JButton jB_Solicitar;
    private javax.swing.JButton jB_summary;
    private javax.swing.JPanel jP_Tree;
    // End of variables declaration//GEN-END:variables
}
