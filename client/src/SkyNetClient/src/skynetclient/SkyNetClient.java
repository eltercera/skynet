/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skynetclient;

import Modulo.SkyNetClientSocket;
import Modulo.SkyNetClientUID;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase para el inicio y definicion de atributos del cliente
 * Atributo IP: Es un atributo constante para indicar la direccion y puerto del webservice
 * Atributo ClientUID: Almacena el md5 del UID del cliente
 * Atributo Sock: Ip y Muerto para establecer la conexion TCP con el servicio o servidor esclavo
 * Atributo list: Lista de mensajes que ha recibido el cliente
 * 
 */
public class SkyNetClient {
    public static final String ip ="http://skynet.net:8888/"; 
    public static String ClientUID;
    public static SkyNetClientSocket sock;
    public static List<String> list = new ArrayList<String>();
       
   /**
    * Funcion para encriptar el UID del cliente
    * @param clear : El texto a encriptar
    * @return: El hash md5 en forma de string
    * @throws Exception 
    */
    public static String md5(String clear) throws Exception {
    MessageDigest md = MessageDigest.getInstance("MD5");
    byte[] b = md.digest(clear.getBytes());
    int size = b.length;
    StringBuffer h = new StringBuffer(size);
    //algoritmo y arreglo md5
        for (int i = 0; i < size; i++) {
            int u = b[i] & 255;
                if (u < 16) {
                    h.append("0" + Integer.toHexString(u));
                }
               else {
                    h.append(Integer.toHexString(u));
               }
           }
      //clave encriptada
      return h.toString();
    }
    
public static void main(String[] args) {
     SkyNetClientUID e = new SkyNetClientUID();
        try {
            ClientUID = md5(e.getMac() +"/"+e.getTimeStamp()+"/"+ e.getPID());
        } catch (Exception ex) {
            Logger.getLogger(SkyNetClient.class.getName()).log(Level.SEVERE, null, ex);
        }
     System.out.println("ClientUID: "+ ClientUID);
     SkyNetConection viewConection = new SkyNetConection();
     viewConection.setVisible(true);     
    }
}
