package skynetclient;

import Controlador.*;
import java.awt.Color;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 * Clase para realizar la peticion de un servicio al Maestro
 * Genera una ventana simple con el boton de conexion
 */

public class SkyNetConection extends javax.swing.JFrame {
    /**
     * Constructor de la clase
     */
       
      public SkyNetConection() {
        getContentPane().setBackground(Color.decode("#26a9e9"));
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        B_Conectar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SkyNet Corporation");
        setResizable(false);

        B_Conectar.setText("Conectar");
        B_Conectar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                B_ConectarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(89, 89, 89)
                .addComponent(B_Conectar)
                .addContainerGap(97, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(B_Conectar)
                .addContainerGap(52, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    private void B_ConectarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_B_ConectarActionPerformed
        
        URL url = null;
        try {
            url = new URL(SkyNetClient.ip);
        } catch (MalformedURLException ex) {
            Logger.getLogger(SkyNetClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        DispatcherService dispat = new DispatcherService(url); 
        DispatcherPortType port = dispat.getDispatcher();
        FileList Param = new FileList();
        FileListResponse Aux_param = port.fileList(Param);
        if (Aux_param.getResult().equalsIgnoreCase("Sucsses")) {
            ArrayOfFileListResponsefiles sl = Aux_param.getFiles();
            List<ArrayOfFileListResponsefilesfile> arr_file= sl.getFile();
            String[] dir = new String[arr_file.size()];
            String[] ar_m5 = new String[arr_file.size()];
            for (int i = 0 ; i < arr_file.size(); i++){
                dir[i]=arr_file.get(i).getNombre();
                ar_m5[i]=arr_file.get(i).getUID();
            }
            TreeView viewTree = new TreeView(dir,ar_m5);
            viewTree.setVisible(true);
            dispose();
        }
        else
            JOptionPane.showMessageDialog(null, Aux_param.getMessage());

    }//GEN-LAST:event_B_ConectarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton B_Conectar;
    // End of variables declaration//GEN-END:variables
}
