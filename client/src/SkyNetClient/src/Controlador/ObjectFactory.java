
package Controlador;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the net.skynet._8888 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: net.skynet._8888
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link OpenFile }
     * 
     */
    public OpenFile createOpenFile() {
        return new OpenFile();
    }

    /**
     * Create an instance of {@link FileInfo }
     * 
     */
    public FileInfo createFileInfo() {
        return new FileInfo();
    }

    /**
     * Create an instance of {@link FileListResponse }
     * 
     */
    public FileListResponse createFileListResponse() {
        return new FileListResponse();
    }

    /**
     * Create an instance of {@link ArrayOfFileListResponsefiles }
     * 
     */
    public ArrayOfFileListResponsefiles createArrayOfFileListResponsefiles() {
        return new ArrayOfFileListResponsefiles();
    }

    /**
     * Create an instance of {@link FileList }
     * 
     */
    public FileList createFileList() {
        return new FileList();
    }

    /**
     * Create an instance of {@link FileInfoResponse }
     * 
     */
    public FileInfoResponse createFileInfoResponse() {
        return new FileInfoResponse();
    }

    /**
     * Create an instance of {@link FileInfoResponseFileInfo }
     * 
     */
    public FileInfoResponseFileInfo createFileInfoResponseFileInfo() {
        return new FileInfoResponseFileInfo();
    }

    /**
     * Create an instance of {@link OpenFileResponse }
     * 
     */
    public OpenFileResponse createOpenFileResponse() {
        return new OpenFileResponse();
    }

    /**
     * Create an instance of {@link OpenFileResponsefile }
     * 
     */
    public OpenFileResponsefile createOpenFileResponsefile() {
        return new OpenFileResponsefile();
    }

    /**
     * Create an instance of {@link ArrayOfFileListResponsefilesfile }
     * 
     */
    public ArrayOfFileListResponsefilesfile createArrayOfFileListResponsefilesfile() {
        return new ArrayOfFileListResponsefilesfile();
    }

}
