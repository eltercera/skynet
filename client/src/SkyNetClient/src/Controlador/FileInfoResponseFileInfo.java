
package Controlador;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FileInfoResponseFileInfo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FileInfoResponseFileInfo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="lock_ts" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="name_hash" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="lock" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="result" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="checksum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FileInfoResponseFileInfo", propOrder = {

})
public class FileInfoResponseFileInfo {

    @XmlElement(name = "lock_ts")
    protected int lockTs;
    @XmlElement(name = "name_hash", required = true)
    protected String nameHash;
    protected boolean lock;
    @XmlElement(required = true)
    protected String result;
    @XmlElement(required = true)
    protected String checksum;
    @XmlElement(required = true)
    protected String name;

    /**
     * Gets the value of the lockTs property.
     * 
     */
    public int getLockTs() {
        return lockTs;
    }

    /**
     * Sets the value of the lockTs property.
     * 
     */
    public void setLockTs(int value) {
        this.lockTs = value;
    }

    /**
     * Gets the value of the nameHash property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameHash() {
        return nameHash;
    }

    /**
     * Sets the value of the nameHash property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameHash(String value) {
        this.nameHash = value;
    }

    /**
     * Gets the value of the lock property.
     * 
     */
    public boolean isLock() {
        return lock;
    }

    /**
     * Sets the value of the lock property.
     * 
     */
    public void setLock(boolean value) {
        this.lock = value;
    }

    /**
     * Gets the value of the result property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResult() {
        return result;
    }

    /**
     * Sets the value of the result property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResult(String value) {
        this.result = value;
    }

    /**
     * Gets the value of the checksum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChecksum() {
        return checksum;
    }

    /**
     * Sets the value of the checksum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChecksum(String value) {
        this.checksum = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

}
