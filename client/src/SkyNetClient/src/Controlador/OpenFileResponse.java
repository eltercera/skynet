
package Controlador;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="file" type="{http://skynet.net:8888/}openFileResponsefile"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "openFileResponse")
public class OpenFileResponse {

    @XmlElement(required = true)
    protected OpenFileResponsefile file;

    /**
     * Gets the value of the file property.
     * 
     * @return
     *     possible object is
     *     {@link OpenFileResponsefile }
     *     
     */
    public OpenFileResponsefile getFile() {
        return file;
    }

    /**
     * Sets the value of the file property.
     * 
     * @param value
     *     allowed object is
     *     {@link OpenFileResponsefile }
     *     
     */
    public void setFile(OpenFileResponsefile value) {
        this.file = value;
    }

}
