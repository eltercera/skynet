/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modulo;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Date;

/**
 * Clase para generar el UID del cliente en base a su MAC address, el timestamp de ejecucion
 * y el identificador del proceso
 */
public class SkyNetClientUID {
    
    /**
     * Funcion para extraer la MAC address del PC
     * @return retorna la direccion MAC en un string
     */
    public String getMac() {
		
	InetAddress ip;
	try {
			
            ip = InetAddress.getLocalHost();
            
            NetworkInterface network = NetworkInterface.getByInetAddress(ip);
            byte[] mac;
            if (network != null) {
                mac = network.getHardwareAddress();
            } else {
                mac = "asdjasldlaksd".getBytes();
            }
            
		
		
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < mac.length; i++) {
		sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));		
            }
            return sb.toString();
			
	} catch (UnknownHostException | SocketException e) {		
		e.printStackTrace();	
	}    
        return null;
   }
    
    /**
     * Funcion para extraer el Identificador de Proceso de la aplicacion
     * @return el ProcessID en forma de string
     */
    public String getPID(){
        String pid = (java.lang.management.ManagementFactory.getRuntimeMXBean().getName()).split("@")[0];
        return pid;
    }
    
    /**
     * Funcion para extraer la marca de tiempo de ejecucion del cliente
     * @return El momento en segundo que se ejucuto el cliente
     */
    public String getTimeStamp(){
        Date date= new Date();
        int time = (int) date.getTime();
        return Integer.toString(time);
    }
}

