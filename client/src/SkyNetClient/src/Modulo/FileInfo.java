package Modulo;

import java.net.URL;

public class FileInfo {
    public String fileName;
    public URL fileURL;
    public String filem5;

    public FileInfo(String book, String filename, String filehash) {
        fileName = book;
        filem5=filehash;
        fileURL = getClass().getResource(filename);
        if (fileURL == null) {
            System.err.println("Couldn't find file: "+ filename);
            }
        }
    public String getm5(){
        return filem5;
    }
    
    public String toString() {
        return fileName;
        }
}
