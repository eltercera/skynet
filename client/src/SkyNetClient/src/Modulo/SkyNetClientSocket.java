/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modulo;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/*Clase que define el socket para la conexion con el esclavo*/

public class SkyNetClientSocket {
    private Socket socket;
    private DataOutputStream out_packet;
    private DataInputStream in_packet;

    /**
     * Constructor del socket
     * @param ip: Direccion a la cual se va a conectar
     * @param puerto: Puerto a levantar para la conexion
     */
    public  SkyNetClientSocket(String ip, int puerto) {
        try {
           // socket = new Socket(InetAddress.getLocalHost(),8000);
            socket = new Socket (ip, puerto);
            out_packet = new DataOutputStream(socket.getOutputStream());
            in_packet = new DataInputStream(socket.getInputStream());
        } catch (Exception ex) {
            Logger.getLogger(SkyNetClientSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
    /**
     * Funcion para cerrar el socket, y el buffer de entrada y salida de informacion
     */
    public void KillSocket(){
        try {
            socket.close();
            out_packet.close();
            in_packet.close();
        } catch (Exception ex) {
            Logger.getLogger(SkyNetClientSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
    /**
     * Funcion que concatena los paquetes de respuesta del servicio en un solo
     * string
     * @return El mensaje final enviado por el esclavo.
     */
    public String SlaveResponse(){
        String mensaje="";
        String buffer;
        boolean Conectado = true;
        while (Conectado) {
            try {
                buffer = this.in_packet.readLine();
                System.out.println("Servidor respondio->>>>:"+ buffer);
                if (buffer.equalsIgnoreCase(""))
                    Conectado = false;
                else
                    if (mensaje.equalsIgnoreCase("")) 
                        mensaje = mensaje + buffer+"&";
                    else
                        mensaje = mensaje + " " + buffer;
                } catch (IOException ex) {
                Logger.getLogger(SkyNetClientSocket.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("Servidor respondio:"+ mensaje);
        return mensaje;
    }
    /**
     * Funcion para el envio de mensajes al servicio, verifica si el mensaje fue aceptado
     * o no.
     * @param text: Texto a enviar
     * @return La respuesta otorgada por el servicio
     */
    public String SendToSlave(String text){     
        String respuesta=null;
        try {
            String mensaje = text+"\r\n"; 
            out_packet.write(mensaje.getBytes());
            out_packet.flush();
            respuesta = SlaveResponse();
            String split[] = respuesta.split("&");
            if (split[0].equalsIgnoreCase("BYE")){
                KillSocket();
                respuesta = "Desconectado o servidor ocupado";
                }
        } catch (IOException ex) {
            Logger.getLogger(SkyNetClientSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
        return respuesta;
    }
    
    public byte[] SendToOpen(String text){     
        String respuesta=null;
        byte[] respuesta2=null;
        try {
            String mensaje = text+"\r\n"; 
            out_packet.write(mensaje.getBytes());
            out_packet.flush();
            respuesta = SlaveResponse();
            String split[] = respuesta.split(" ");
            System.out.println(split[0]);
            if (split[0].equalsIgnoreCase("SUCCESS")){
                int a = Integer.parseInt(split[1].replaceAll("&", ""));
                respuesta2=SlaveOpenResponse(a);
                }
        } catch (IOException ex) {
            Logger.getLogger(SkyNetClientSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
        return respuesta2;
    }
    
    
    public String SendToSave(byte[] text){     
        String respuesta=null;
        try {
            out_packet.write(text);
            out_packet.flush();
            respuesta = SlaveResponse();
            String split[] = respuesta.split("&");
            if (split[0].equalsIgnoreCase("SUCCESS")){
                 JOptionPane.showMessageDialog(null, "Guardado");
                }
            else
                JOptionPane.showMessageDialog(null, "Error guardando el archivo");

        } catch (IOException ex) {
            Logger.getLogger(SkyNetClientSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
        return respuesta;
    }
    
    public byte[] SlaveOpenResponse(int size){
        byte[] message = new byte[size];
        boolean Conectado = true;
        while (Conectado) {
               System.out.print("While");
            try {
                int i = 0;
                while(i< size){
                    message[i] = this.in_packet.readByte();
                    i++;
                }
                String a=new String(message);
                Conectado = false;
                String SlaveResponse = SlaveResponse();
                SlaveResponse = SlaveResponse();
                } catch (IOException ex) {
                Logger.getLogger(SkyNetClientSocket.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return message;
    }
}
