package Modulo;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.net.URL;
import java.util.Enumeration;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

public class TreePaneFile extends JPanel implements TreeSelectionListener {
    private JEditorPane htmlPane;
    private JTree tree;
    private URL helpURL;
    private static boolean DEBUG = false;
    public static String aux_dir;
    public static String aux_m5;



    //Optionally play with line styles.  Possible values are
    //"Angled" (the default), "Horizontal", and "None".
    private static boolean playWithLineStyle = false;
    private static String lineStyle = "Horizontal";
    
    //Optionally set the look and feel.
    private static boolean useSystemLookAndFeel = false;
    
    public TreePaneFile(String[] ar, String[] ar_m5){
       
        super(new GridLayout(1,0));
        
        //Create the nodes.
        DefaultMutableTreeNode top = new DefaultMutableTreeNode(".");
        DefaultTreeModel model = new DefaultTreeModel(top);
        add(tree=new JTree(top));
        DefaultTreeModel modelo;        
	modelo = (DefaultTreeModel)tree.getModel();
        CreatePath(ar,ar_m5, (DefaultMutableTreeNode) modelo.getRoot());
        
        //Create a tree that allows one selection at a time.
        tree.getSelectionModel().setSelectionMode
                (TreeSelectionModel.SINGLE_TREE_SELECTION);

        //Listen for when the selection changes.
        tree.addTreeSelectionListener(this);

        if (playWithLineStyle) {
            System.out.println("line style = " + lineStyle);
            tree.putClientProperty("JTree.lineStyle", lineStyle);
        }

        //Create the scroll pane and add the tree to it. 
        JScrollPane treeView = new JScrollPane(tree);

        //Create the HTML viewing pane.
        htmlPane = new JEditorPane();
        htmlPane.setEditable(false);
    
        add(treeView);
 
        Dimension minimumSize = new Dimension(100, 50);
        treeView.setMinimumSize(minimumSize);
       }

 private void CreatePath(String[] path,String[] m5, DefaultMutableTreeNode root){
    DefaultMutableTreeNode actual=null;
    DefaultMutableTreeNode sig=null;
    DefaultMutableTreeNode to_in= null;
    int aux=0;
    for (int i=0;i<path.length;i++){
        actual=root;
         String split[]=path[i].split("/");
         for (int j=1;j<(split.length);j++) {
             sig=FindPath(actual, split[j]);
             System.out.println(actual.toString());
             System.out.println(sig.toString());
             if ((actual.toString().equals(sig.toString()) || (j==(split.length)-1)) ){
                 for (int h=j;h<(split.length)-1;h++){
                     
                     to_in= new DefaultMutableTreeNode(split[h]);
                     actual.add(to_in);
                     actual=to_in;
                 }

                 to_in=new DefaultMutableTreeNode(new FileInfo(split[split.length-1],split[split.length-1],m5[aux]));
                 aux=aux+1;
                 actual.add(to_in);
                 j=split.length;
             }else {
                 actual=sig;
             }
         }
    }
}       
 private DefaultMutableTreeNode FindPath(DefaultMutableTreeNode root, String Path){
        Enumeration<DefaultMutableTreeNode> e = root.depthFirstEnumeration();
        while (e.hasMoreElements()) {
            DefaultMutableTreeNode node = e.nextElement();
            if (node.toString().equalsIgnoreCase(Path)) {
               return node;
            }
        }
        return root;
    }
 
    @Override
    public void valueChanged(TreeSelectionEvent e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                           tree.getLastSelectedPathComponent();
        
        if (node == null) return;
        TreePath[] paths = tree.getSelectionPaths();
        String dir = paths[0].toString().replace("[","");
        dir=dir.replace("]", "");
        dir=dir.replace(",","/");
        aux_dir=dir.replace(" ", "");
        Object nodeInfo = node.getUserObject();
        if (node.isLeaf()) {
            FileInfo archivo = (FileInfo)nodeInfo;
            aux_m5=archivo.getm5();
            System.out.println(archivo.filem5);
            if (DEBUG) {
                System.out.print(":  \n    ");
            }
        }
        if (DEBUG) {
            System.out.println(nodeInfo.toString());
        }
    }
}
