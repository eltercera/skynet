"""
    UniqueID.py
    Clase de generar el Identificador Unico (UID) del master server
    El UID sera el MD5 de la direccion mac / timeStamp / PID

    timeStamp = tiempo en que se levanto el servidor
    PID = Identificador de proceso del servidor

    UID = MD5(MAC/timeStamp/PID)

"""
import uuid, re, psutil
import hashlib
import time
import binascii


class UniqueID(object):

    uniqueID = bytearray(16)
    mac = ''.join(re.findall('..', '%012x' % uuid.getnode()))

    @staticmethod
    def getMAC():
        return UniqueID.mac

    @staticmethod
    def generateUID():
        p = psutil.Process()
        id =( UniqueID.mac +'/'+ str(time.time()) +'/'+str(p.pid))
        ids = hashlib.md5()
        ids.update(id)
        uid = ids.digest()
        UniqueID.uniqueID[0:16] = uid

    @staticmethod
    def getUniqueID():
        return UniqueID.uniqueID


    @staticmethod
    def getStringUniqueID(uid):
        return binascii.hexlify(uid)

    @staticmethod
    def getStringUID():
        return binascii.hexlify(UniqueID.uniqueID)
