from SkyNetMaster import MasterStateID
from skyNetMaster.SkynetDataBase import SkynetDataBase
from skyNetSlave import *
from random import randint
import time

#Clase para el manejo de la lista de esclavos en el maestro

class SlaveList(object):

    list = []

    #Numero de servidores registrados
    @staticmethod
    def getNumberSlave():
        return SkynetDataBase.countSlave()

    @staticmethod
    def getAllSlave():
        return SkynetDataBase.getAllSlaveFromDB()

    #Retorna un esclavo segun el ID
    @staticmethod
    def getSlaveFromList(ID):
        return SkynetDataBase.getSlaveFromDB(ID)

    #Agrega un esclavo a la lista
    @staticmethod
    def addSlave(Esclavo):
        SkynetDataBase.addSlavetoDB(Esclavo)


    #Actualiza el estado del esclavo segun su UID
    @staticmethod
    def statusSlave(ID, estado):
        SkynetDataBase.modifyStatusSlave(ID,estado)

    #Busca, segun el UID, la posicion en la lista de un esclavo
    @staticmethod
    def searchSlave(ID):
        slave = SkynetDataBase.getSlaveFromDB(ID)
        return slave

    #Elimina el esclavo con la UID correspondiente
    @staticmethod
    def dropSlave(ID):
        SkynetDataBase.delSlave(ID)


    #Retorna el Socket (Ip,Puerto) del esclavo con UID de la lista
    @staticmethod
    def getSocketSlave(ID):
        if (SlaveList.searchSlave(ID) is None):
           return None
        else:
            sock = SlaveList.list[SlaveList.searchSlave(ID)].getSocket()
            return sock

    #Retorna un esclavo con estatus activo en una posicion random
    @staticmethod
    def getRamdonSlave(tr):
        pass

    #Retorna el numero de servidores activos
    @staticmethod
    def getNumActiveSlave():
        j=0
        for i in range(0,len(SlaveList.list),1):
            slave = SlaveList.list[i]
            if (slave.getStatus()==MasterStateID.SID_SLAVE_ENABLE):
                j=j+1
        return j

    #Retorna el numero de servidores ocupados
    @staticmethod
    def getNumBussySlave():
        j=0
        for i in range(0,len(SlaveList.list),1):
            slave = SlaveList.list[i]
            if (slave.getStatus()==MasterStateID.SID_SLAVE_DISABLE):
                j=j+1
        return j

    #Retorna el numero de servidores caidos
    @staticmethod
    def getNumDownSlave():
        j=0
        for i in range(0,len(SlaveList.list),1):
            slave = SlaveList.list[i]
            if (slave.getStatus()==MasterStateID.SID_SLAVE_DOWN):
                j=j+1
        return j

    #Realiza una busqueda secuencial para encontrar un slaveServer disponible
    @staticmethod
    def getSlavesec():
        j=0
        for i in range(0,len(SlaveList.list),1):
            slave = SlaveList.list[i]
            if (slave.getStatus()==MasterStateID.SID_SLAVE_ENABLE):
                return slave
            else:
                j=j+1
        return None
    
    @staticmethod
    def updateStatus(uid,time):
        SkynetDataBase.modifyTimestampSlave(uid, time)
