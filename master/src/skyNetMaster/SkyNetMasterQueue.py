import SkyNetPipe

__author__ = 'carlos-herrera'

"""
    Clase Principal para el manejo de la cola de paquetes
"""
class SkyNetQueue(object):

    threatWork = None

    """
        Metodo para poner los paquetes que llegan a la cola
    """
    @staticmethod
    def putInQueue(packet):
        if (not (SkyNetQueue.threatWork is None)):
            SkyNetQueue.threatWork.putInQueue(packet)


    """
        Inicializa el hilo de procesamiento de la cola
    """
    @staticmethod
    def initWork():
        SkyNetQueue.threatWork = SkyNetPipe.SkyNetQueueWorking()

    """
        Metodo para verificar si el proceso esta activo.
    """
    @staticmethod
    def isAliveWork():
        return SkyNetQueue.threatWork.isAlive()

    """
        Detenia el procesamiento de la cola
    """
    @staticmethod
    def stopThreat():
        print("------")
        SkyNetQueue.threatWork.closeThreat()



