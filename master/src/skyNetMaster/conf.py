
import socket
from skyNetMaster.SkyNetMaster import MasterStateID

__author__ = 'carlos-herrera'


"""
    Conf.py
    Clase para el manejo de constantes del MasterServer
        Direccion ip y puerto de escucha UDP
        Direcciones IP y Puerto del WebService
"""
class Conf(object):

    UDP_IP = 0
    UDP_port = 0
    Manage_IP = 0
    Manage_PORT = 0
    WS_IP = 0
    WS_Port = 0

    serviceSocket = None
    serviceType = None
    serviceTcp = None

    socket = None
    socketTCP = None

    serverStatus =  MasterStateID.SID_MASTER_OFF

    """
        Inicializador del Socket
    """
    @staticmethod
    def init():
        Conf.serviceSocket = socket.AF_INET
        Conf.serviceType = socket.SOCK_DGRAM
        Conf.serviceTcp = socket.SOCK_STREAM

    """
        Asignar la direccion ip de escucha UDP
    """
    @staticmethod
    def setServiceAddress(ip):
        Conf.UDP_IP = ip

    """
        Asignar el puerto de escucha UDP
    """
    @staticmethod
    def setServicePort(port):
        Conf.UDP_port = int(port)

    """
        Inicializar el Socket.
    """
    @staticmethod
    def initSocket():
        if (Conf.socket is None):
            Conf.socket = socket.socket(Conf.serviceSocket,Conf.serviceType)
        else:
            print ("El socket ya esta iniciado")

    """
        Inicializar el socket TCP
    """
    @staticmethod
    def initSocketTCP():
        if (Conf.socketTCP is None):
            Conf.socketTCP = socket.socket(Conf.serviceSocket, Conf.serviceTcp)
        else:
            print ("Socket TCP ya iniciado")

    @staticmethod
    def getSocketTcp():
        return Conf.socketTCP

    """
        Asignar estatus del masterServer
    """
    @staticmethod
    def setServerStatus(status):
        Conf.serverStatus = status

    """
        Obtener el status del masterServer
    """
    @staticmethod
    def getServerStatus():
        return Conf.serverStatus

    """
        Asignar la ip del socket tcp
    """
    @staticmethod
    def setManageIP(ip):
        Conf.Manage_IP = ip

    """
        Asignar el puerto del socket tcp
    """
    @staticmethod
    def setManagePORT(port):
        Conf.Manage_PORT = int(port)

    """
        Asignar la direccion ip del WebService
    """
    @staticmethod
    def setWebServiceAddress(ip):
        Conf.WS_IP = ip

    """
        Asignar el puerto del WebService
    """
    @staticmethod
    def setWebServicePort(port):
        Conf.WS_Port = int(port)
