import socket
import threading
import netaddr
import time
from skyNetMaster import SkynetDataBase
from skyNetMaster.SkyNetMaster import SlaveActionID
from skyNetMaster.SkynetFile import skynet_file
from skyNetMaster.SkynetMsj import SkynetMsj
from skyNetMaster.UniqueID import UniqueID
from skyNetMaster.conf import Conf

__author__ = 'carlos-herrera'


class AgentLocal(threading.Thread):


    def __init__(self,file, act):
        threading.Thread.__init__(self)
        self.isWork = True
        self.file = file
        self.action = act


    def run(self):

            if (self.file is None):
                while self.isWork:
                    time.sleep(2)
                    try:
                        if (SkynetDataBase.SkynetDataBase.deleteSlave() > 0):
                            print ("Un slaves se elimina de la DB. Mucho tiempo sin recivir actualizacion")

                        elif(SkynetDataBase.SkynetDataBase.udpateStatusSlave() > 0):
                            print ("Un slave paso a estado Down por no recivir actualizacion ")

                        if(SkynetDataBase.SkynetDataBase.deleleteMaster(UniqueID.getStringUID()) > 0):
                            print ("Un Master se elimina de la DB. Mucho tiempo sin recivir actualizacion")
                        elif (SkynetDataBase.SkynetDataBase.checkStatusMaster(UniqueID.getStringUID()) > 0):
                            print ("Un Master paso a estado Down por no actualizar su timeStamp")
                    except:
                        print "!!!!!!!!!!!!!!!!!!!!"
                        
                        
                    SkynetDataBase.SkynetDataBase.modifyTimeMaster(UniqueID.getStringUID())

                    infoAll = SkynetDataBase.SkynetDataBase.infocompare()
                    for info in infoAll:
                        if(SkynetDataBase.SkynetDataBase.countSlave() >= SlaveActionID.COUNT_SLAVE_MAX):
                            infoFile = SkynetDataBase.SkynetDataBase.getInfoFile(info[0])
                            f = skynet_file(infoFile[0], infoFile[1], infoFile[2], infoFile[3], infoFile[4], infoFile[5])
                            AgentL = None
                            if(info[1] < SlaveActionID.COUNT_SLAVE_MAX):
                                if(info[1] == info[2]):
                                    AgentL = AgentLocal(f,"rand")
                                else:
                                    AgentL = AgentLocal(f, "update")
                            elif (info[1] != info[2]):
                                AgentL = AgentLocal(f, "update")
                            if AgentL is not None:
                                AgentL.start()


            else:
                self.protocol(self.file)

    def protocol(self, file):
        mensaje = SkynetMsj("repliFile")
        mensaje.setVar("default","name_hash",file.get_name_Hash())
        mensaje.setVar("default","timeStamp",file.getTimeStam())
        mensaje.setVar("default","name",file.getname())
        mensaje.setVar("default","checksum",file.getChecksum())
        mensaje.setVar("default","size",file.getSize())
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if (self.action == "rand"):
            infoSlave = SkynetDataBase.SkynetDataBase.lookRandSlave(file.get_name_Hash())
            print "infoSlave -----> ",infoSlave
            if infoSlave:
                tup = (infoSlave[0], infoSlave[1])
                self.send(tup, sock, mensaje)
            else:
                print("hasnt slave to repliFile ",file.get_name_Hash())

        elif (self.action == "update"):
            infoSlave = SkynetDataBase.SkynetDataBase.lookRandSlaveWithFile(file.get_name_Hash())
            for info in infoSlave:
                self.send((info[0], info[1]), sock, mensaje)


    def send(self, infoSlave, sock, mensaje):
        server_address = (infoSlave)
        try:
            sock.connect(server_address)
        except:
            return
        mensaje.send(sock)
        answer = SkynetMsj.create(sock)
        mensaje = SkynetMsj("end")
        mensaje.send(sock)
        sock.shutdown(socket.SHUT_WR)
        if answer.getValue("default","result") != "Sucsses":
            AgentL = AgentLocal(self.file,"rand")
            AgentL.start()
        else:
            print ("--------> answer message: ",answer.getValue("default","message"))

    def stopAgentLocal(self):
        self.isWork = False