import threading
from SkyNetSlaveList import SlaveList
from SkyNetMasterServer import SkyNetMasterUDPServer
from SkyNetMaster import MasterStateID
from UniqueID import UniqueID
from conf import Conf
from SkyNetMasterQueue import SkyNetQueue
from skyNetMaster.SkynetMasterAgentLocal import AgentLocal
from skyNetMaster.SkynetMasterTCP import socketTCP
from skyNetMasterWebService.SkyNetMasterWS import WebService
from skyNetMaster.SkynetDataBase import SkynetDataBase

"""
    Console.py
    clase para iniciar o detener procesos
     InitServiceUDP inia el Escucha UDP y el proceso de Packets entre los Slave Server y el master Server
     InitWebService inia el WebService del maestro
     InitStateUP Cambia el estado del Maestro a ON
     InitStateDown Cambia el estado del Maestro a Down
     PrintSlave Muestra informacion de los esclavos del maestro
     Exit Cierra todos los procesos iniciados y finaliza el programa
"""
__author__ = 'carlos-herrera'

class Console(threading.Thread):

    def __init__(self):
        """
        Constructor
        Init Thread and set the work in True
        """
        threading.Thread.__init__(self)
        self.commands = []
        self.work = True
        self.serviceUDP = None
        self.webService = None
        self.serviceTCP = None
        self.agent = None
        
        self.initStateUP()
        self.initServiceUDP()
        self.initServiceTCP()
        self.initWebService()
        

    """
        Comandos del MasterServer
    """
    def init(self):
        self.addCmd("InitServiceUDP")
        self.addCmd("InitServiceTCP")
        self.addCmd("InitWebService")
        self.addCmd("InitStateUP")
        self.addCmd("InitStateDown")
        self.addCmd("PrintSlave")
        self.addCmd("PrintStatus")
        self.addCmd("Exit")

    """
        Agrega los comandos a ejecutar en una lista
    """
    def addCmd(self, command):
        self.commands.append(command)

    """
        Hilo principal de la consola
         Maneja los comandos para la manipulacion del MasterServer
    """
    def run(self):
        while self.work:
            command = raw_input('>>>')
            if (len(command) > 1):
                if(command == self.commands[0]):
                    self.initServiceUDP()
                if(command == self.commands[1]):
                    self.initServiceTCP()
                if(command == self.commands[2]):
                    self.initWebService()
                if (command == self.commands[3]):
                    self.initStateUP()
                if (command == self.commands[4]):
                    self.initStateDown()
                if (command == self.commands[5]):
                    self.infoSlave()
                if (command == self.commands[6]):
                    self.printStatus()
                if (command == self.commands[7]):
                    self.closeApp()
    """
        Cambia el estado del MasterServe a OFF
    """
    def initStateDown(self):
        Conf.setServerStatus(MasterStateID.SID_MASTER_OFF)
        print 'Cambio Status del maestro: OFF'

    """
        Cambia el estado del MasterServe a ON
    """
    def initStateUP(self):
        Conf.setServerStatus(MasterStateID.SID_MASTER_ON)

        print 'Cambio Status del maestro: ON'

    """
        Inicia el WebService SOAP
        Con la libreria PySimpleSOAP
    """
    def initWebService(self):
        if (Conf.serverStatus == MasterStateID.SID_MASTER_OFF):
            print("Estado del servidor Caido.")
            print("Ejecutar InitStateUP")
        elif (self.webService is None):
            print "Iniciando WebService"
            self.webService = WebService(Conf.WS_IP, Conf.WS_Port)
            self.webService.start()
        else:
            print "El WebService ya esta iniciado"

    """
        Inicia el servidor UDP y el menejo de la cola de Servicios
    """
    def initServiceUDP(self):
        if (Conf.serverStatus == MasterStateID.SID_MASTER_OFF):
            print("Estado del servidor Caido.")
            print("Ejecutar InitStateUP")
        else:
            print "Iniciando servicio UDP"
            self.serviceUDP = SkyNetMasterUDPServer()
            self.serviceUDP.start()

    """
        Inicia el Agente TCP
    """
    def initServiceTCP(self):
        if (Conf.serverStatus == MasterStateID.SID_MASTER_OFF):
            print("Estado del servidor Caido.")
            print("Ejecutar InitStateUP")
        else:
            print "Iniciando servicio TCP"
            self.serviceTCP = socketTCP()
            self.agent = AgentLocal(None,None)
            self.serviceTCP.start()
            self.agent.start()


    """
        Muestra la informacion del Master Server
    """
    def printStatus(self):
        print ("UID -> %s"%UniqueID.getStringUID())
        print("Status ID - > %s"%self.prin(Conf.serverStatus))
        print("Service Port - > %i"%Conf.UDP_port)
        print("Service Addr - > %s"%Conf.UDP_IP)
        print("Web Service Port - > %i"%Conf.WS_Port)
        print("Web Service Addr - > %s"%Conf.WS_IP)
        print("TCP manage Port - > %i"%Conf.Manage_PORT)
        print("TCP manage Addr - > %s"%Conf.Manage_IP)


    """
        Muestra la informacion de todos los slave server
        registrados
    """
    def infoSlave(self):
        tabla = SlaveList.getAllSlave()
        count = SlaveList.getNumberSlave()
        for n in range(count):
            i = 0
            slave = tabla[n]
            while (i < count):
                print ("Slave en lista [%s] "%i)
                print ("UID -> %s"%slave['uid'])
                print("Status ID - > %s"%self.prin(slave['status']))
                print("Listening Port - > %s" %slave['kaliveport'])
                print("Listening Addr - > %s" %slave['kaliveip'])
                print("Service Addr - > %s "%slave['serviceip'])
                print("Service Port - > %s"%slave['serviceport'])
                print("Manage Addr - > %s "%slave['manageip'])
                print("Manage Port - > %s"%slave['manageport'])
                print("")
                i+=1

    """
        retorna los estados de los servidores (Slave y Master)
    """
    def prin(self, wr):

        if (wr == MasterStateID.SID_MASTER_ON):
            return "On"
        if (wr == MasterStateID.SID_MASTER_OFF):
            return "Off"
        if (wr == MasterStateID.SID_SLAVE_ENABLE):
            return "Enable"
        elif (wr == MasterStateID.SID_SLAVE_DISABLE):
            return "Disable"
        elif (wr == MasterStateID.SID_SLAVE_DOWN):
            return "Down"


    """
        Cierra todas las instancias
        y finaliza la aplicacion
    """
    def closeApp(self):

        if(self.serviceUDP is not None):
            if (self.serviceUDP.isAliveListen()):
                self.serviceUDP.listen.closeThread()
                self.serviceUDP.checkSlave.closeThreath()

            if (SkyNetQueue.isAliveWork()):
                SkyNetQueue.stopThreat()
                print('Finalizo el procesados de la cola')
                print('Finalizo la escucha UDP')

        if(self.webService is not None):
            if (self.webService.is_alive):
                self.webService.closeWS()

        if (self.serviceTCP is not None):
            self.serviceTCP.stopSocketTCP()
            self.agent.stopAgentLocal()

        SkynetDataBase.delMaster(UniqueID.getStringUID())

        self.work=False