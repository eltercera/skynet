import netaddr

__author__ = 'carlos-herrera'
__version__ = '0.1'

"""
    SkyNetPacket.py
    Create on: Nov 14 2015

    Clase para la creacion y el manejo del paquete
"""

from time import time
from ExceptionMaster import InvalidPacketSizeException, InvalidServicePortException
from UniqueID import UniqueID
import socket

class skyNetPacket(object):

    """
        Variable global:
        packet : paquete de 32 Bytes para envio / recepcion del protocolo
    """
    def __init__(self):
        self.packet = bytearray(32)

    """
        Crea el paquete vacio
    """
    def skyNetPacket(self):
        self.packet[0:32] = 0

    """
        Crea el paquete desde uno ya creado
    """

    def skyNetPacket(self, data):
        if(len(data) > 32):
             raise InvalidPacketSizeException()
        self.packet[0:32] = data


    def setAccionID(self, acction_id):
        self.packet[0] = acction_id

    """
        set StatusID in position 1 from the packet
    """
    def setStatusID(self, status_id):
        self.packet[1] = status_id

    """
        Set Service port on position 2 and 3 from the packet
        it also validates port is between 65535 and 10
    """
    def setServicePort(self, port):
        if ((port > 65535) | (port < 10)):
            raise InvalidServicePortException()
        self.packet[2] = (port & 0xff00) >> 8
        self.packet[3] = (port & 0xff)

    """
        Set Service Address on positions 4 to 7 from the packet
    """
    def setServiceAddress(self, addr):
        position = 4
        service_Address = int(netaddr.IPAddress(addr))
        for l in range(0,32,8):
            global packet
            self.packet[position] =  (service_Address & (0xFF << l)) >> l
            position+=1

    """
        Set current time on same position of setTimeStamp(self, timestamp)
    """
    def setTimeStamp(self):
        self.setTimeStamp2(int(time()))

    """
        Set TimeStamp on position 8 to 15 from the packet
    """
    def setTimeStamp2(self, timestamp):
        position = 8
        for l in range(56,-8,-8):
            self.packet[position] = (timestamp & (0xFF << l)) >> l
            position+=1

    """
        Set UID on the packet
    """
    def setUID(self, digest):
        self.packet[16:32] = digest

    def getAcctionID(self):
        return self.packet[0]

    def getStatusID(self):
        return self.packet[1]

    def getServicePort(self):
        return ((self.packet[2]) | (self.packet[3]) << 8)

    def getUID(self):
        return self.packet[16:32]

    def getServiceAddress(self):
        add = 0
        j=4
        for i in range(0, 32,8):
            add = add | (self.packet[j] << i)
            j+=1

        return add

    def getTimeStamp(self):
        add = 0
        j = 8
        for i in range(56,-8,-8):
            add = add | (self.packet[j] << i)
            j+=1
        return add

    def getUID(self):
        return self.packet[16:32]

    def getStringUID(self):
        return UniqueID.getStringUniqueID(self.packet[16:32])

    def getPacket(self):
        return self.packet

