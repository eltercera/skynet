'''
Modulo de inicio del Servidor Maestro.
'''
from Consola import Console
from conf import Conf
from UniqueID import UniqueID
from SkyNetMaster import MasterStateID
from skyNetMaster.SkynetDataBase import SkynetDataBase

"""
    Inicializa la consola del MasterServer
"""
def init():
    UniqueID.generateUID()
    print ("Master UID >>>> %s"%UniqueID.getStringUID())
    Conf.init()
    Conf.initSocket()
    Conf.initSocketTCP()
    Conf.setServerStatus(MasterStateID.SID_MASTER_OFF)
    console = Console()
    console.init()
    SkynetDataBase.init()
    SkynetDataBase.addMaster()
    console.start()


"""
    Parametros para poder ejecutar el MasterServer
     -i : Direccion IP de escucha del socket
     -p : Puerto de escucha del socket
     -I : Direccion IP del WebService
     -P : Puerto del WebService
"""
def case(parameters):
        if (parameters[0] == '-i'):
            Conf.setServiceAddress(parameters[1])
        elif (parameters[0] == '-p'):
            Conf.setServicePort(parameters[1])
        elif (parameters[0] == '-I'):
            Conf.setWebServiceAddress(parameters[1])
        elif (parameters[0] == '-P'):
            Conf.setWebServicePort(parameters[1])
        elif (parameters[0] == '-T'):
            Conf.setManageIP(parameters[1])
        elif (parameters[0] == '-C'):
            Conf.setManagePORT(parameters[1])

