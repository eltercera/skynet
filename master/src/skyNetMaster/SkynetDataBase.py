from time import time
import psycopg2, psycopg2.extras
from skyNetMaster import UniqueID
from skyNetMaster.SkyNetMaster import MasterStateID, SlaveActionID
from skyNetMaster.skyNetSlave import skyNetSlave
from conf import Conf

__author__ = 'carlos-herrera'



class SkynetDataBase(object):

    conn = None
    cur = None

    @staticmethod
    def getConn():
        return psycopg2.connect(database='sd',user='distribuidos',password='123456789', host='localhost')
    
    @staticmethod
    def init():
        pass

    @staticmethod
    def countSlave():
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(
            "select count(UID) from skynet_slave"
        )
        rows = cur.fetchall()
        row = rows[0]
        cur.close()
        conn.close()
        return row[0]

    @staticmethod
    def infocompare():
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(
            "select f.name_hash, count(sl.fk_file), (select count(l.fk_file)"
            "                        from skynet_slave_file as l, skynet_slave as lave"
            "                        where l.checksum = f.checksum and l.fk_file = f.name_hash and l.fk_slave = lave.uid and (%(time)s - lave.timestam) < 3)"
            "from skynet_file as f, skynet_slave_file as sl, skynet_slave as slave "
            "where f.name_hash = sl.fk_file and sl.fk_slave = slave.uid and (%(time)s - slave.timestam) < 3 "
            "group by f.name_hash" %{"time":time()}
        )
        rows = cur.fetchall()
        cur.close()
        conn.close()
        return rows



    @staticmethod
    def addMaster():
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(
            "Insert into skynet_master (uid, status, manageip, manageport, serviceip, serviceport, timestam)"
            "values ('%(uid)s',%(sta)s,'%(mip)s',%(mport)s,'%(sip)s',%(sport)s, %(time)s)"
            %{"uid":UniqueID.UniqueID.getStringUID(), "sta":Conf.serverStatus, "mip":Conf.Manage_IP, "mport":Conf.Manage_PORT,
              "sip":Conf.UDP_IP,"sport":Conf.UDP_port,"time":time()}
        )
        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def getMasterinfo():
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(
            "select uid,manageip,manageport,serviceip, serviceport from skynet_master"
        )
        rows = cur.fetchall()
        cur.close()
        conn.close()
        return rows

    @staticmethod
    def getnumberMaster():
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(
            "select count(uid) from skynet_master"
        )
        rows = cur.fetchall()
        row = rows[0]
        cur.close()
        conn.close()
        return row[0]

    @staticmethod
    def deleleteMaster(uid):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(
            "delete from skynet_master where uid <> '%(uid)s' and (%(time)s - timestam) > 6"%{"time":time(),"uid":uid}
        )
        conn.commit()
        count = cur.rowcount
        cur.close()
        conn.close()
        return count

    @staticmethod
    def delMaster(uid):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(
            "delete from skynet_master where uid = '%(uid)s'"%{"uid":uid}
        )
        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def checkStatusMaster(uid):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(
            "update skynet_master set status = %(sta)s where uid <> '%(uid)s' and (%(time)s - timestam) > 3"%{"time":time(), "sta":MasterStateID.SID_MASTER_OFF,"uid":uid}
        )
        conn.commit()
        count =  cur.rowcount
        cur.close()
        conn.close()
        return count

    @staticmethod
    def modifyStatusMaster(uid, status):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(
            "update skynet_master set status = %(sta)s where uid = '%(uid)s'"%{"uid":uid, "sta":status}
        )
        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def udpateStatusSlave():
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(
            "Update skynet_slave set status = %(sta)s where (%(time)s - timestam) > 4"
            %{"sta":MasterStateID.SID_SLAVE_DISABLE, "time":time()}
        )
        conn.commit()
        count =cur.rowcount
        cur.close()
        conn.close()
        return count

    @staticmethod
    def deleteSlave():
        conn = SkynetDataBase.getConn()
        count = -1
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(
            "select uid from skynet_slave where (%(time)s - timestam) > 6 "%{"time":time()}
        )
        rows = cur.fetchall()
        cur.close()
        conn.close()
        for row in rows:
            print row
            SkynetDataBase.delSlave(row[0])
            count = 1

        return count

    @staticmethod
    def modifyTimeMaster(uid):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(
            "update skynet_master set timestam = %(time)s where uid = '%(uid)s'"%{"uid":uid, "time":time()}
        )
        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def addSlavetoDB(slave):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(
            "insert into skynet_slave (uid ,status,  serviceip , serviceport , manageip , manageport , kaliveip , kaliveport , timestam) "
            "values ('"
            "%(uid)s',"
            "%(status)s,'"
            "%(sip)s',"
            "%(sport)s,'"
            "%(mip)s',"
            "%(mport)s,'"
            "%(kip)s',%(kport)s,%(tim)s)"%
            {"uid":slave.getUID(),
             "status":slave.getStatus(),
             "sip":slave.getServiceIP(),"sport":slave.getServicePort(),
             "mip":slave.getManageIP(),"mport":slave.getManagePort(),
             "kip":slave.getIp(),"kport":slave.getPort(),
             "tim":slave.getTimeStamp()}
        )
        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def getSlaveFromDB(UID):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(
            "Select * from skynet_slave where uid = '%(uid)s'"%{"uid":UID}
        )
        rows = cur.fetchall()
        row = rows[0]
        slave = skyNetSlave(row['uid'],row['status'],row['kaliveip'],row['kaliveport'],row['serviceip'],row['serviceport'],
                                        row['manageip'],row['manageport'],row['timestam'])
        cur.close()
        conn.close()
        return slave

    @staticmethod
    def getAllSlaveFromDB():
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute(
            "Select * from skynet_slave"
        )
        rows = cur.fetchall()
        cur.close()
        conn.close()
        return rows

    @staticmethod
    def modifyTimestampSlave(uid,time):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("Update skynet_slave set timestam = %(tim)s where uid = '%(uid)s'"%{"tim":time,"uid":uid})
        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def modifyStatusSlave(uid,status):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("Update skynet_slave set status = '%(sta)s' where uid = '%(uid)s'"%{"sta":status,"uid":uid})
        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def openfile(hash):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        cur.execute("select s.uid, s.serviceip, s.serviceport, f.checksum from skynet_slave as s inner join Skynet_slave_file as sl on (s.uid = sl.fk_slave)"
                                   "inner join skynet_file as f on (sl.fk_file = f.name_hash) where f.checksum = sl.checksum and f.name_hash = '%(hash)s' and"
                                   " f.status = %(stat)s"
                                   "order by random() limit 1"
                                   %{"hash":hash, "stat":SlaveActionID.FID_UNBLOCK_FILE})
        row = cur.fetchone()
        cur.close()
        conn.close()
        return row

    @staticmethod
    def lookingAllSlave(hash_name):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("select uid, manageip, manageport "
                    "from skynet_slave inner join skynet_slave_file as fl ON (uid = fl.fk_slave) "
                    "inner join skynet_file as f ON (fl.fk_file = f.name_hash)"
                    "where fl.fk_file = '%(file)s' and fl.checksum = f.checksum"%{"file":hash_name})
        rows = cur.fetchall()
        print rows
        cur.close()
        conn.close()
        return rows

    @staticmethod
    def lookRandSlave(hash):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("select manageip, manageport "
                    "from skynet_slave "
                    "where uid not in ("
                    "   select fk_slave from skynet_slave_file"
                    "   where fk_file = '%(hash)s') and (%(time)s - timestam) < 3 order by random() limit 1"
                    %{"time":time(), "hash":hash}
                    )
        row = cur.fetchone()
        cur.close()
        conn.close()
        return row

    @staticmethod
    def lookRandSlaveWithFile(hash):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("select manageip, manageport from skynet_slave, skynet_slave_file "
                    "where uid = fk_slave and fk_file = '%(hash)s' and (%(time)s - timestam) < 3"
                    %{"time":time(), "hash":hash}
                    )
        row = cur.fetchall()
        cur.close()
        conn.close()
        return row
    
    @staticmethod
    def countSlaveFile(hash_name):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("select count(sl.fk_file),f.timestam,f.checksum "
                    "from skynet_file as f, skynet_slave_file as sl "
                    "where f.name_hash = sl.fk_file and f.name_hash = '%(hash)s' and f.checksum = sl.checksum "
                    "group by f.timestam, f.checksum limit 1" %{"hash":hash_name}
                    )
        row = cur.fetchone()
        cur.close()
        conn.close()
        return row

    @staticmethod
    def delSlave(uid):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("delete from skynet_slave_file where fk_slave = '%(uid)s'"%{"uid":uid})
        cur.execute("delete from skynet_slave where uid = '%(uid)s'"%{"uid":uid})
        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def addFile(uid, file):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
                                   
        cur.execute("Insert into skynet_file (_name, name_hash, timestam, checksum, size, status) "
                                   "values ('%(name)s', '%(hash)s', %(time)s,'%(check)s',%(size)s,%(sta)s)"
                                   %{"name":file.getname(),"hash":file.get_name_Hash(),"time":file.getTimeStam(),"check":file.getChecksum(),
                                     "size":file.getSize(),"sta":file.getStatus()})
        conn.commit()

        cur.execute("Insert into skynet_slave_file (fk_slave, fk_file, checksum) values ('%(uid)s','%(hash)s','%(check)s')"
                                   %{"uid":uid, "hash":file.get_name_Hash(), "check":file.getChecksum()})
        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def getCheckFile(uid):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        cur.execute("Select checksum, timestam from skynet_file where name_hash = '%(hash)s'" %{"hash":uid})
        row = cur.fetchone()
        cur.close()
        conn.close()
        return row

    @staticmethod
    def updateRalationSlaveFile(hash, uid, checksum):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        cur.execute("update skynet_slave_file set checksum = '%(check)s' where fk_slave = '%(uid)s' and fk_file ='%(hash)s' "
                                   %{"hash":hash, "uid":uid, "check":checksum})

        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def insertRalationSlaveFile(hash, uid, checksum):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        cur.execute("insert into skynet_slave_file (fk_slave, fk_file, checksum) values ('%(uid)s','%(hash)s','%(check)s') "
                                   %{"hash":hash, "uid":uid, "check":checksum})
        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def modifyFileChecksum(hash, checksum):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("Update skynet_file set checksum = '%(stat)s' where name_hash = '%(hash)s'"
                                   %{"stat":checksum, "hash":hash})
        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def modifyFileSize(hash, size):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("Update skynet_file set size =%(size)s where name_hash = '%(hash)s'"
                                   %{"size":size, "hash":hash})
        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def modifyFileTimes(hash, timestam):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("Update skynet_file set timestam =%(time)s where name_hash = '%(hash)s'"
                                   %{"time":timestam, "hash":hash})
        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def verifyLockFile(hash):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        cur.execute("select status from skynet_file where name_hash = '%(hash)s' "
                                   %{"hash":hash})
        row = cur.fetchone()
        cur.close()
        conn.close()
        print row
        if row[0] == SlaveActionID.FID_UNBLOCK_FILE:
            return True
        else:
            return False

    @staticmethod
    def modifyFileStatus(hash, block):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        cur.execute("Update skynet_file set status = %(stat)s where name_hash = '%(hash)s'"
                                   %{"hash":hash,"stat":block})
        conn.commit()
        cur.close()
        conn.close()

    @staticmethod
    def getInfoFile(hash):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("select _name, name_hash, timestam, checksum, size, status from skynet_file where name_hash ='%(hash)s'"
                                   %{"hash":hash})
        rows = cur.fetchall()
        row = rows[0]
        cur.close()
        conn.close()
        return row

    @staticmethod
    def getFileList():
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        cur.execute("select distinct(r.fk_file), f._name from skynet_slave_file as r, skynet_file as f"
                    "where r.fk_file = f.name_hash and r.checksum = f.checksum")
        rows = cur.fetchall()
        cur.close()
        conn.close()
        return rows


    @staticmethod
    def verifySlave_file(uid, file):
        conn = SkynetDataBase.getConn()
        cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

        cur.execute("Select checksum from skynet_slave_file where fk_slave = '%(hash)s' and fk_file = '%(file)s'"
                    %{"hash":uid, "file":file.get_name_Hash()})
        rows = cur.fetchone()
        cur.close()
        conn.close()
        return rows

