
"""
    ExceptionMaster.py
    Clase de manejo de excepciones usadas en el master Server
"""
import socket
class InvalidServicePortException(Exception):
    pass

class InvalidPacketSizeException(Exception):
    pass

class SockUdpTimeOutException(socket.timeout):
    pass
