__author__ = 'carlos-herrera'


class skynet_file(object):

    def __init__(self, nombre, hash, time, check, size, status):
        self.name = nombre
        self.name_hash = hash
        self.timeStam = time
        self.checksum = check
        self.size = size
        self.status = status


    def getname(self):
        return self.name

    def get_name_Hash(self):
        return self.name_hash

    def getChecksum(self):
        return self.checksum

    def getTimeStam(self):
        return self.timeStam

    def getSize(self):
        return self.size

    def getStatus(self):
        return self.status

    def setname(self, name):
        self.name_hash = name

    def set_name_hash(self,hash):
        self.name_hash = hash

    def setTimeStam(self, time):
        self.timeStam = time

    def setChecksum(self, check):
        self.checksum = check

    def setSize(self,size):
        self.size = size

    def setStatus(self, status):
        self.status = status