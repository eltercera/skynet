from SkyNetPipe import SkyNetQueueListening
from SkyNetMasterQueue import SkyNetQueue
from SkyNetSlaveThreat import slaveThreat
from conf import Conf
import netaddr

"""
    Clase Principal del SocketUDP
"""
class SkyNetMasterUDPServer(object):
    """
        Inicializa las variablescon con las constantes pasadas por parametros
    """
    def __init__(self):
        self.socket = Conf.socket
        self.server_address = (str(netaddr.IPAddress(Conf.UDP_IP)), Conf.UDP_port)


    """
        Inicializa el socket y el hilo para procesamiento de la cola
    """
    def start(self):
        self.socket.bind(self.server_address)
        self.listen = SkyNetQueueListening(self.socket)
        self.listen.start()
        self.checkSlave = slaveThreat()
        self.checkSlave.start()

        SkyNetQueue.initWork()
        SkyNetQueue.threatWork.start()

    """
        Verifica si el socket esta activo
    """
    def isAliveListen(self):
        return self.listen.isAlive()

    """
        Retorna el socket del masterServer
    """
    def getSocket(self):
        return self.socket
