import threading
from skyNetMaster import SkynetDataBase
from skyNetMaster.SkyNetMaster import MasterStateID, SlaveActionID
from skyNetMaster.SkyNetSlaveList import SlaveList
from skyNetMaster.SkynetFile import skynet_file
from skyNetMaster.SkynetMsj import SkynetMsj
from skyNetMaster.UniqueID import UniqueID
from skyNetMaster.skyNetSlave import skyNetSlave
import socket
import time

class AgentRemote(threading.Thread):


    def __init__(self,conn):
        threading.Thread.__init__(self)
        self.isWork = True
        self.connect = conn

    def run(self):
        while self.isWork:
            msj = SkynetMsj.create(self.connect)
            if msj:
                self.protocol(msj)
            else:
                print "Error al recivir mensaje..."
                self.isWork = False
                
        self.connect.shutdown(socket.SHUT_WR)
                


    def protocol(self, msj):
        if (msj.actionIs("registerSlave")):
            print "Registrando Esclavo"
            slave = skyNetSlave(msj.getValue("default","uid"),
            MasterStateID.SID_SLAVE_DISABLE,
            msj.getValue("default","kaliveIP"),
            msj.getValue("default","kalivePort"),
            msj.getValue("default","serviceIP"),
            msj.getValue("default","servicePort"),
            msj.getValue("default","managerIP"),
            msj.getValue("default","managerPort"),
            time.time()
            )
            answer = SkynetMsj("registerSlave")
            try:
                SlaveList.addSlave(slave)
                answer.setVar("default","result","Sucsses")
            except:
                answer.setVar("default","message","Slave doesnt register")
                answer.setVar("default","result","Fail")
            
            if answer.getValue("default","result") == "Sucsses":
                table = SkynetDataBase.SkynetDataBase.getMasterinfo()
                answer.setVar("default","mastersCount", len(table))
                n = 0
                for master in table:
                    answer.setVar("masterServerInfo-%(i)s"%{"i":n},"uid",master[0])
                    answer.setVar("masterServerInfo-%(i)s"%{"i":n},"managerIP",master[1])
                    answer.setVar("masterServerInfo-%(i)s"%{"i":n},"managerPort",master[2])
                    answer.setVar("masterServerInfo-%(i)s"%{"i":n},"kalivePort",master[4])
                    answer.setVar("masterServerInfo-%(i)s"%{"i":n},"kaliveIP",master[3])
                    n = n + 1

            answer.send(self.connect)

        elif (msj.actionIs("unregisterSlave")):
            print "UnRegistrando Esclavo"
            answer = SkynetMsj("unregisterSlave")
            try:
                SlaveList.dropSlave(msj.getValue("default","uid"))
                answer.setVar("default","message","Slave unregister")
                answer.setVar("default","result","Sucsses")
            except:
                answer.setVar("default","message","Slave doesnt unregister")
                answer.setVar("default","result","Fail")
            answer.send(self.connect)

        elif(msj.actionIs("registerFile")):
            print "Registrando archivo"
            answer = SkynetMsj("registerFile")
            file = skynet_file(msj.getValue("default","name"),msj.getValue("default","name_hash"),
                               msj.getValue("default","timeStamp"),msj.getValue("default","checksum"),
                               msj.getValue("default","size"), 0)
            infofile = SkynetDataBase.SkynetDataBase.getCheckFile(file.get_name_Hash())
            if (infofile):
                    if (infofile[0] != file.getChecksum()):
                        if(infofile[1] > file.getTimeStam()):
                            try:
                                SkynetDataBase.SkynetDataBase.updateRalationSlaveFile(file.get_name_Hash(),msj.getValue("default","uid"), file.getChecksum())
                                answer.setVar("default","message","Slave update checksum file on relation slave <-> file")
                                answer.setVar("default","result","Repli")
                            except:
                                answer.setVar("default","message","Slave doesnt update checksum file on relation slave <-> file")
                                answer.setVar("default","result","Fail")
                        else:
                            try:
                                SkynetDataBase.SkynetDataBase.modifyFileChecksum(file.get_name_Hash(), file.getChecksum())
                                answer = self.saveRelation(answer, msj, file)
                            except:
                                answer.setVar("default","message","Slave doesnt update checksum on relation slave <-> file")
                                answer.setVar("default","result","Fail")

                    else:
                        print("two files has a same checksum")
                        answer = self.saveRelation(answer, msj, file)

            else:
                try:
                    SkynetDataBase.SkynetDataBase.addFile(msj.getValue("default","uid"),file)
                    answer.setVar("default","message","Slave register file")
                    answer.setVar("default","result","Sucsses")
                except:
                    answer.setVar("default","message","Slave doesnt register file")
                    answer.setVar("default","result","Fail")
            answer.send(self.connect)

        elif (msj.actionIs("slaveRepliFile")):
            print "Solicitando"
            answer = SkynetMsj("slaveRepliFile")
            info = SkynetDataBase.SkynetDataBase.countSlaveFile(msj.getValue("default","name_hash"))
            table = SkynetDataBase.SkynetDataBase.lookingAllSlave(msj.getValue("default","name_hash"))
            n = 0
            answer.setVar("default","count",info[0])
            answer.setVar("File","timeStamp",info[1])
            answer.setVar("File","checksum",info[2])
            for i in range(info[0]):
                slave = table[n]
                answer.setVar("Slave-%(i)s"%{"i":i},"uid",slave[0])
                answer.setVar("Slave-%(i)s"%{"i":i},"manageip",slave[1])
                answer.setVar("Slave-%(i)s"%{"i":i},"manageport",slave[2])
                n = n + 1
            answer.send(self.connect)
        elif (msj.actionIs("lockFile")):
            print "Loqueando"
            answer = SkynetMsj("lockFile")
            if SkynetDataBase.SkynetDataBase.verifyLockFile(msj.getValue("default","name_hash")):
                try:
                    SkynetDataBase.SkynetDataBase.modifyFileStatus(msj.getValue("default","name_hash"),SlaveActionID.FID_BLOCK)
                    answer.setVar("default","message","Block file")
                    answer.setVar("default","result","Sucsses")
                except:
                    answer.setVar("default","message","doesnt block file")
                    answer.setVar("default","result","Fail")
            else:
                answer.setVar("default","message","File already block")
                answer.setVar("default","result","Fail")
            answer.send(self.connect)
        elif (msj.actionIs("unlockFile")):
            print "Desbloqueand"
            answer = SkynetMsj("unlockFile")
            try:
                SkynetDataBase.SkynetDataBase.modifyFileStatus(msj.getValue("default","name_hash"),SlaveActionID.FID_UNBLOCK_FILE)
                answer.setVar("default","message","UnBlock file")
                answer.setVar("default","result","Sucsses")
            except:
                answer.setVar("default","message","doesnt Unblock file")
                answer.setVar("default","result","Fail")
            answer.send(self.connect)


        elif (msj.actionIs("end")):
                self.isWork = False



    def saveRelation(self, answer, msj, file):
        check = SkynetDataBase.SkynetDataBase.verifySlave_file(msj.getValue("default","uid"), file)
        if (check):
            try:
                SkynetDataBase.SkynetDataBase.updateRalationSlaveFile(file.get_name_Hash(),msj.getValue("default","uid"), file.getChecksum())
                answer.setVar("default","message","Slave register file")
                answer.setVar("default","result","Sucsses")
            except:
                answer.setVar("default","message","Slave doesnt update checksum file on relation slave <-> file")
                answer.setVar("default","result","Fail")
        else:
            try:
                SkynetDataBase.SkynetDataBase.insertRalationSlaveFile(file.get_name_Hash(),msj.getValue("default","uid"), file.getChecksum())
                answer.setVar("default","message","insert Slave relation file")
                answer.setVar("default","result","Sucsses")
            except:
                answer.setVar("default","message","doesnt insert on relation slave <-> file")
                answer.setVar("default","result","Fail")

        return answer

    def stopAgent(self):
        self.isWork = False