from mutex import mutex
import threading, time
from SkyNetSlaveList import SlaveList
from SkyNetMaster import MasterStateID
from skyNetMaster.SkynetDataBase import SkynetDataBase


class slaveThreat(threading.Thread):
    """
        Clase de manejo de la lista
        del masterServer
    """

    def __init__(self):
        """
        Constructor
        Init Thread and set the work in True
        """
        threading.Thread.__init__(self)
        self.isWork = True
        self.lock = mutex()


    """
        Verifica los TimeStamp de los SlaveServer registrados en el masterServer
            Se pasan a down despues de un tiempo estimado
            Se eliminan de la lista si pasa a Down y aun no se recibe paquete de actualizacion 3 segundos despues
    """
    def run(self):

        while self.isWork:
            time.sleep(3)


    """
        Cierra el hilo del proceso
    """
    def closeThreath(self):
        self.isWork = False
