import socket
import threading
import netaddr
from skyNetMaster.SkynetMasterAgentRemote import AgentRemote
from skyNetMaster.conf import Conf

__author__ = 'carlos-herrera'

"""
    ///hilo tcp escucha

"""


class socketTCP(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.socket = Conf.getSocketTcp()
        print (str(netaddr.IPAddress(Conf.Manage_IP)), Conf.Manage_PORT)
        self.server_address = (str(netaddr.IPAddress(Conf.Manage_IP)), Conf.Manage_PORT)
        self.socket.bind(self.server_address)
        self.socket.listen(9999)
        self.socket.settimeout(3)
        self.isWork = True


    def run(self):
        while self.isWork:
            try:
                conn, addr = self.socket.accept()
                print "recivida Conexion"
                agentRemote = AgentRemote(conn)
                agentRemote.start()
            except socket.timeout:
                continue


    def stopSocketTCP(self):
        self.isWork = False
        self.socket.shutdown(socket.SHUT_WR)


