"""
    SkyNetPipe.py

    Clases para la manipulacion de la cola
    del Master Server

    Create on: 19 / 11 / 2015
"""
__author__ = 'carlos-herrera'

import threading, socket, Queue, netaddr
from skyNetSlave import skyNetSlave
from SkyNetSlaveList import SlaveList
from ExceptionMaster import InvalidPacketSizeException
from UniqueID import UniqueID
from SkyNetMasterQueue import SkyNetQueue
from SkyNetPacket import skyNetPacket
from SkyNetMaster import MasterStateID, SlaveActionID
from conf import Conf
from mutex import mutex

class SkyNetQueueListening(threading.Thread):

    def __init__(self, socket):
        """
            Constructor
        :param socket:
        """
        threading.Thread.__init__(self)
        self.work = True
        self.sock = socket
        self.sock.settimeout(3)


    def run(self):
        """
        Thread run method.
        """
        while self.work:

            try:
               data , addr = self.sock.recvfrom(32)
               self.process(data)
            except socket.timeout:
                continue

            except InvalidPacketSizeException:
                print("This packet is not 32 bytes")
                continue

    """
        Process data from the socket
        Packet recivide is a byteArray of 32 bytes
    """
    def process(self, data):
        packet = skyNetPacket()
        packet.skyNetPacket(data)
        SkyNetQueue.putInQueue(packet)
    """
        Set the work in False
        and close the Thread
    """
    def closeThread(self):
        self.sock.close()
        self.work = False
        self.join()


"""
    Clase SkynetQueueWorking
    Implementation of a Thread that get packets from a queue
    with the protocols from packets
"""
class SkyNetQueueWorking(threading.Thread):

    def __init__(self):
        """
        Constructor
        Init Thread and set the work in True
        """
        threading.Thread.__init__(self)
        self.isWork = True
        self.queue = Queue.Queue()
        self.lock = mutex()

    """
        Set the work in False
        and close the Thread
    """
    def closeThreat(self):
        self.isWork = False

    """
        Thread Run

        check is the queue is empty
        if it is not empty it takes a packet and proceeds to get the protocol

    """
    def run(self):
        while self.isWork:
            if (not (self.lock.test())):
                self.lock.testandset()
            try:
                packet = self.queue.get(True,3)
                if (packet is not None):
                    self.protocol(packet)
                    self.queue.task_done()
                    self.lock.unlock()
            except Queue.Empty:
                continue

    """
        Packet's Protocol
    """
    def protocol(self, packet):

        if (packet.getAcctionID() == SlaveActionID.AID_SLAVE_REGISTER):
            self.slaveRegister(packet)
        elif (packet.getAcctionID() == SlaveActionID.AID_SLAVE_REGISTER_SERVICE):
            self.slaveRegisterService(packet)
        elif (packet.getAcctionID() == SlaveActionID.AID_SLAVE_STATUS):
            self.slaveUpdateStatus(packet)
        elif (packet.getAcctionID() == SlaveActionID.AID_SLAVE_UNREGISTER):
            self.slaveUnregister(packet)
        else:
            print("That action is not aviable")

    """
        Imprime el estado del servidor
    """
    def prin(self, wr, uid):
        if (wr == MasterStateID.SID_SLAVE_ENABLE):
            print ("El slave %s paso a estado enable o disponible."%uid)
        elif (wr == MasterStateID.SID_SLAVE_DISABLE):
            print ("El slave %s paso al estado Disable o ocupado."%uid)
        elif (wr == MasterStateID.SID_SLAVE_DOWN):
            print ("El slave %s paso a estado caido."%uid)

    """
        Actualizacion del TimeStamp y Status del slave
        en la lista del masterServer
    """
    def slaveUpdateStatus(self, recpacket):
        uid = str(recpacket.getStringUID())
        
        slave = SlaveList.getSlaveFromList(uid)
        if (slave is not None):
            if (recpacket.getStatusID() != slave.getStatus()):
                SlaveList.statusSlave(slave.getUID(), recpacket.getStatusID());
                print("Cambio de estado exitoso.")
                self.prin(slave.getStatus(),slave.getUID())

            SlaveList.updateStatus(slave.getUID(), recpacket.getTimeStamp())
        else:
            print "El slave server no esta registrado"
            self.notificateSlaveUnregister(recpacket)


    """
        Registro de esclavo en la lista
    """
    def slaveRegister(self, recpacket):

        if(Conf.serverStatus == MasterStateID.SID_MASTER_ON):
            uid = str(recpacket.getStringUID())
            slave = SlaveList.getSlaveFromList(uid)
            if (slave is None):
                print ("Agregando Slave a la lista")
                slave = skyNetSlave(uid,recpacket.getStatusID(),str(netaddr.IPAddress(recpacket.getServiceAddress())),recpacket.getServicePort(), recpacket.getTimeStamp())
                SlaveList.addSlave(slave)
                print ("Slave Agregado satisfactoriamente")
                self.registerAccept(slave,SlaveActionID.AID_REGISTER_ACCEPT)
            elif ((str((slave.getIp())) == str(netaddr.IPAddress(recpacket.getServiceAddress()))) & (slave.getPort() == recpacket.getServicePort())):
                self.registerAccept(slave,SlaveActionID.AID_REGISTER_ACCEPT)
            else:
                self.registerDeny(recpacket, SlaveActionID.AID_REGISTER_DENY)
        else:
            self.registerDeny(recpacket, SlaveActionID.AID_REGISTER_DENY)


    """
        Registra la ip y puerto de servicio del server slave
    """
    def slaveRegisterService(self, recpacket):
        if(Conf.serverStatus == MasterStateID.SID_MASTER_ON):
            uid = str(recpacket.getStringUID())
            slave = SlaveList.getSlaveFromList(uid)
            if (slave is not None):
                if ((slave.serviceIP is None) | (slave.getServicePort() is None)):
                    print ("Agregando datos de servicio del esclavo: %s"%slave.getUID())
                    slave.setService(int(recpacket.getServiceAddress()),recpacket.getServicePort())
                    self.registerAccept(slave,SlaveActionID.AID_REGISTER_SERVICE_ACCEPT)
                elif (str((slave.getServiceIP()) == str(netaddr.IPAddress(recpacket.getServiceAddress()))) & (slave.getServiceIP() == recpacket.getServicePort())):
                    self.registerAccept(slave,SlaveActionID.AID_REGISTER_SERVICE_ACCEPT)
                else:
                    self.registerDeny(recpacket, SlaveActionID.AID_REGISTER_SERVICE_DENY)
            else:
                self.registerDeny(recpacket, SlaveActionID.AID_REGISTER_SERVICE_DENY)
        else:
            self.registerDeny(recpacket, SlaveActionID.AID_REGISTER_SERVICE_DENY)


    """
        Notifica al Slave Server que no esta registrado
    """
    def notificateSlaveUnregister(self, packet):
        pk = self.packetAnswer(SlaveActionID.AID_REGISTER_REJECT)
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server_addr = (str(packet.getServiceAddress()),packet.getServicePort())
        try:
            sock.sendto(pk.getPacket(), server_addr)
        finally:
            sock.close()



    """
        Respuesta al protocolo AID_SLAVE_REGISTER
        Con el identificador AID_REGISTER_DENY
    """
    def registerDeny(self, rec, acctionID):
        packet = self.packetAnswer(acctionID)
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server_addr = (str(rec.getServiceAddress()),rec.getServicePort())
        try:
            sock.sendto(packet.getPacket(), server_addr)
        finally:
            sock.close()

    """
        Respuesta al protocolo AID_SLAVE_REGISTER
        con el identificador AID_REGISTER_ACCEPT
    """
    def registerAccept(self, rec, acctionID):
        packet = self.packetAnswer(acctionID)
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server_addr = (rec.getSocket())
        try:
            sock.sendto(packet.getPacket(), server_addr)
        finally:
            sock.close()


    """
        SlaveUnregister Elimina a un slave server de la lista de servidores
        responde con el identificador AID_UNREGISTER_ACCEPT.
    """
    def slaveUnregister(self, rec):
        print ("Unregister Slave...")
        slave = SlaveList.getSlaveFromList(rec.getStringUID())
        if (slave is not None):
            SlaveList.dropSlave(str(rec.getStringUID()))
            packet = self.packetAnswer(SlaveActionID.AID_UNREGISTER_ACCEPT)
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            print str(slave.getIp()),slave.getPort()
            server_addr = (str(slave.getIp()),slave.getPort())
            try:
                sock.sendto(packet.getPacket(), server_addr)
            finally:
                print("Slave eliminado Exitosamente")
                sock.close()
        else:
            print("El slave server ya estaba eliminado")


    "Constructor de paquete de respuesta"
    def packetAnswer(self, acctionID):
        packet = skyNetPacket()
        packet.setAccionID(acctionID)
        packet.setServicePort(Conf.UDP_port)
        packet.setServiceAddress(Conf.UDP_IP)
        packet.setUID(UniqueID.getUniqueID())
        packet.setTimeStamp()
        packet.setStatusID(Conf.serverStatus)
        return packet



    """
        Pone en la cola un paquete.
    """
    def putInQueue(self,packet):
        self.queue.put(packet)



