__author__ = 'carlos-herrera'
__version__ = '0.1'

"""
    SkyNetMaster.py
    Header principal del servidor maestro
"""
from enum import Enum


"""
    Protocolo del Paquete UDP
"""
class SlaveActionID (Enum):

    PROGRAM_NAME = "SkyNet Master Server"
    PROGRAM_VERSION = "0.1"

    BYTE_NULL = 0x00
    COUNT_SLAVE_MAX = 2
    AID_SLAVE_REGISTER = 1
    AID_SLAVE_STATUS = 2
    AID_SLAVE_UNREGISTER = 5
    AID_SLAVE_REGISTER_SERVICE = 6

    AID_REGISTER_ACCEPT = 64
    AID_REGISTER_SERVICE_ACCEPT = 65
    AID_UNREGISTER_ACCEPT = 66
    AID_REGISTER_SERVICE_DENY = 194
    AID_REGISTER_REJECT = 193
    AID_REGISTER_DENY = 192

    FID_UNBLOCK_FILE = 0
    FID_BLOCK = FILE = 1

"""
    Estados del MasterServer y de los SlaveServer
"""
class MasterStateID (Enum):

    SID_SLAVE_ENABLE = 1
    SID_SLAVE_DISABLE = 2
    SID_SLAVE_DOWN = 3

    SID_MASTER_ON = 4
    SID_MASTER_OFF = 5

