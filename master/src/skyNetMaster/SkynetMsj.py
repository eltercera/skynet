'''
Created on Jan 17, 2016

@author: rrodriguez
'''

class SkynetMsjContextVar():
    
    def __init__(self,name,value = None):
        if name:
            self.__name = name
            self.__value = value
        else:
            pass
            #meter una exepcion.
    
    def nameIs(self,name):
        if self.__name == name:
            return True
        return False
    
    def setVal(self,value):
        if value:
            self.__value = str(value)
            
    def getVal(self):
        return self.__value
    
    def getName(self):
        return self.__name
    
            
class SkynetMsjContext():
    
    def __init__(self,name):
        if name:
            self.__name = name
            self.__vars = list()
        else:
            pass
            #meter una exepcion.
    
    def nameIs(self,name):
        if self.__name == name:
            return True
        return False
    
    def getName(self):
        return self.__name
    
    def addVar(self,var):
        if var:
            self.__vars.append(var)
            
    def getVar(self,varName,create = False):
        for var in self.__vars:
            if var.nameIs(varName):
                return var
        if create:
            var = SkynetMsjContextVar(varName)
            self.addVar(var)
            return var;
        return None
    
    def getVars(self):
        return self.__vars
    
class SkynetMsj():
    
    def __init__(self,action):
        if action:
            self.__action = action
            self.__contexts = list()
        else:
            pass
            #meter una exepcion.
            
    def actionIs(self,action):
        if self.__action == action:
            return True
        return False
    
    def addContext(self,con):
        if con:
            self.__contexts.append(con)
    
    def getContext(self,name,create = False):
        if not name:
            return None
        
        for cont in self.__contexts:
            if cont.nameIs(name):
                return cont
        if create:
            cont = SkynetMsjContext(name)
            self.addContext(cont)
            return cont
        return None
    
    def setVar(self,cName,vName,value):
        if not cName or not vName or not value:
            return None
        
        self.getContext(cName,True).getVar(vName,True).setVal(value)
        
    def getValue(self,cName,vName):
        
        con = self.getContext(cName)
        if con:
            var = con.getVar(vName)
            if var:
                return var.getVal()
            
        return None
    
    def send(self,sock):
        
        sock.send("action:"+self.__action+"\r\n")
        for con in self.__contexts:
            sock.send("context:"+con.getName()+"\r\n")
            for var in con.getVars():
                sock.send(var.getName()+":"+var.getVal()+"\r\n")
        sock.send("\r\n")
        
    @staticmethod
    def create(sock):
        line = ""
        lContext = None

        msj = None
        fileSock = sock.makefile()
        while True:
            line = fileSock.readline().strip("\r\n")
            if line == "":
                break
            
            parts = line.split(":",1)
            if len(parts) != 2:
                pass
                #meter una exepcion de que el formate esta malo y no se leyo el mensaje.
            
            if parts[0] == "action":
                msj = SkynetMsj(parts[1])
            else:
                if not  msj:
                    pass
                    #meter una exepcion de que el formate esta malo y no se leyo el mensaje.
                if parts[0] == "context":
                    lContext = parts[1]
                else:
                    msj.setVar(lContext,parts[0],parts[1])
                
            
        return msj
        
        
            