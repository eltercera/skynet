import netaddr


class skyNetSlave:

#Metodo para setear el UID del esclavo

    def setUID(self,id):
        self.UID=id


#Metodo para consultar el UID del esclavo

    def getUID(self):
        return self.UID


#Metodo para setear el estado del esclavo

    def setStatusSlave(self,Status):
        self.Estado=Status


#Metodo para consultar el estado del esclavo

    def getStatus(self):
        return self.Estado


#Metodo para setear la IP y Puerto del esclavo en una variable socket, formato UDP

    def setSocket(self,IP,Puerto):
        UDP_IP=IP
        UDP_PORT=Puerto
        sock = (IP, Puerto)
        self.Socket=sock

#Metodo para consultar el Socket (ip,puerto)

    def getSocket(self):
        return self.Socket

#Metodo para consultar IP

    def getIp(self):
        return (self.getSocket())[0]


#Metodo para consultar y setear Puerto

    def getPort(self):
        return (self.getSocket())[1]

#Metodo para setear el ultimo tiempo que llego un paquete de actualizacion

    def setTimeStamp(self,timeStamp):
        self.timeStamp = timeStamp

#Metodo para consultar el tiempo que llego el ultimo paquete de actualizacion

    def getTimeStamp(self):
        return self.timeStamp


    def setService(self, ip, port):
        self.serviceIP = ip
        self.servicePort = port

    def getServiceIP(self):
        return str(netaddr.IPAddress(self.serviceIP))

    def getServicePort(self):
        return self.servicePort

    def setManager(self, ip, port):
        self.manageIP = ip
        self.managePort = port

    def getManageIP(self):
        return str(netaddr.IPAddress(self.manageIP))

    def getManagePort(self):
        return self.managePort


#Metodo para llenar inicializar el Esclavo

    def constructor(self, uid,estado,ip,puerto,time):
        self.setUID(uid)
        self.setStatusSlave(estado)
        self.setSocket(ip,puerto)
        self.setTimeStamp(time)
        self.servicePort = None
        self.serviceIP = None
        self.manageIP = None
        self.managePort = None

    def __init__(self, uid,estado,ip,puerto,serviceIP, servicePort,manageIP, managePort,time):
        self.setUID(uid)
        self.setStatusSlave(estado)
        self.setSocket(ip,puerto)
        self.setTimeStamp(time)
        self.servicePort = servicePort
        self.serviceIP = serviceIP
        self.manageIP = manageIP
        self.managePort = managePort

    def constructorEmpty(self):
        self.timeStamp = None
        self.Socket= None
        self.Estado = None
        self.UID = None
        self.serviceIP = None
        self.servicePort = None
        self.manageIP = None
        self.managePort = None