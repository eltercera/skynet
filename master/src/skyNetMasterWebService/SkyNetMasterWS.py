
"""
    Clase WebService.py
    Manejo de servicios del WebService
"""
from skyNetMaster.SkynetDataBase import SkynetDataBase
from skyNetMaster.SkynetFile import skynet_file

__author__ = 'carlos-herrera'
import threading
from BaseHTTPServer import HTTPServer
from pysimplesoap.server import SoapDispatcher, SOAPHandler
from skyNetMaster.SkyNetMaster import MasterStateID
from skyNetMaster.skyNetSlave import skyNetSlave
from skyNetMaster.SkyNetSlaveList import SlaveList
from skyNetMaster.conf import Conf

class WebService(threading.Thread):

    """
        Constructor
    """
    def __init__(self,ip,port):
        threading.Thread.__init__(self)
        self.isWork = True
        name = "skynet.net"
        self.httpd = HTTPServer((ip, port), SOAPHandler)
        dir = "http://%s:%i/"%(name,port)
        print dir
        self.dispatcher = SoapDispatcher(
        'dispatcher',
        location = dir,
        action = dir,
        namespace = dir, prefix="ns0",
        trace = True,
        ns = True)


    """
        Solicitud de listado de archivos del clientes al Maestro.
    """
    def getFileList(self):
        result = "Sucsses"
        infoFile = None
        tmp = {"files":[]}
        try:
            infoFile = SkynetDataBase.getFileList()
        except:
            print "Error en skynetDatabase getFileList"

        if infoFile is not None:
            for info in infoFile:
                tmp["files"].append({"file" :{'nombre' : info[0], 'UID' : info[1] }})
            message = "Archivos encontrados"
        else:
            result = "Faile"
            message = "No se encontraron archivos"

        ret = {"files":tmp["files"], "result":result,"message":message}
        return ret


    """

    """
    def getFileInfo(self,hash_name):

        result = "Sucsses"
        infoFile = SkynetDataBase.getInfoFile(hash_name)
        name = ""
        times = 0
        lock = True
        check = ""
        size = 0
        if infoFile:
            name = infoFile[0]
            times = infoFile[2]
            check = infoFile[3]
            size = infoFile[4]
            if infoFile[5] == 0:
                lock = False
            else:
                lock = True
            message = "Informacion de Archivo"

        else:
            result = "Fail"
            message = "no se encontro el archivo indicado"

        return {
            'FileInfo':
                {'result': result,
                 'name': name, 'name_hash':hash_name,'checksum':check,'size':size, 'lock':lock,'lock_ts':times,
                 'message':message}}

    """
        Solicitud apertura de un archivo.
    """
    def openFile(self,hash_name):
        result = "Sucsses"
        message = ""
        checksum = ""
        uid = None
        serviceIP = None
        Port = -1
        infoSlave = SkynetDataBase.openfile(hash_name)
        print "-------> :",infoSlave
        if infoSlave:
                uid = infoSlave[0]
                serviceIP = infoSlave[1]
                Port = infoSlave[2]
                checksum = infoSlave[3]
        else:
            print "------------------------"
            result = "Fail"
            message = "No hay slave disponibles"

        return {'file':{'result':result,'ServerName':serviceIP,'serverPort':Port,
                        'suid':uid,'checksum':checksum,'message':message}}

    """
        registra las funciones
        que el webService puede realizar
    """
    def registerFunction(self):
        # register the functions
        self.dispatcher.register_function('FileList', self.getFileList,
            returns={'files':[{'file':{'nombre':str,"UID":str}}],'result':str,'message':str},
            args = {})

        self.dispatcher.register_function('FileInfo', self.getFileInfo,
            returns={'FileInfo':{'result':str, 'name':str,'name_hash':str,'checksum':str,'size':int, 'lock':bool,'lock_ts':int}},
            args = {'hash_name':str})

        self.dispatcher.register_function('openFile',self.openFile,
            returns={'file':{'result':str,'ServerName':str,'serverPort':int,'suid':str,'checksum':str,'message':str}},
            args = {'hash_name':str})

    def run(self):
        """
            Thread run
        """
        self.registerFunction()
        self.httpd.dispatcher = self.dispatcher
        while (self.isWork):
            self.httpd.serve_forever()

    """
        Cierra el socket del WebService
    """
    def closeWS(self):
        self.isWork = False
        self.httpd.shutdown()