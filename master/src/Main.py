import getopt
import sys
import skyNetMaster.ServerMain

if __name__ == '__main__':
    try:
        param, o = getopt.getopt(sys.argv[1:],"i:p:I:P:T:C:")
        if(len(param) > 1 ):
            for w in param:
                skyNetMaster.ServerMain.case (w)
            skyNetMaster.ServerMain.init()
    except getopt.GetoptError as err:
        print 'Flag error: ', err